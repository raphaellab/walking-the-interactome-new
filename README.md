# README #

### THINGS TO DO ### 

DK:

* first, look at optimal t for monogenic, polygenic. how do they compare to optimal t for cancer, 0.0015?

* revisit \eps-balls stuff with t from above. will cancer genes be spread out here? would be interesting to see results. i don't think cancer genes will be that spread out still; i just think that the graphs will look v similar to RWR ones.

* look at results' dependence on t. want to somehow derive results concerning (degree of gene set) vs. optimal t to use. how can we figure this out? naively we could use a lot of computing power to do this, but there should be a better way.

--------------------------------------------

RWR:

* check RWR against global pagerank vector (check phone photos, it's just sum of columns of PPR matrix).

* see if restart rate changes if we want optimal polygenic/monogenic results instead. if it does, then can probably do things similar to stuff w/ DK. otherwise, RWR is more stable to change than DK. i think(/hope) RWR is stable enough.

* look up results about PPR eigenvalues and stationary distributions.

--------------------------------------------

OTHER:

* get more diseases from exomewalker? would be hard to make monogenic/polygenic distinction, but like ben said maybe that just doesn't matter. will have to edit the python script and all that fun stuff. 

* figure out some way to systematically/algorithmically determine the clusters of a gene set. perhaps using the \eps-ball approach? my idea is:

--have gene list L. first, maybe do something w/ degree/something else to get parameters. this will let us compute the kernel K. 

--using K, cluster L, say L1, L2, ..., Ln. 

--look at K*1_{L1}, ..., K*1_{Ln}. ______ them to get total scores for each candidate. wait summing them doesn't change anything. uhhh... maybe take the max? product? min? (although i feel like min ~ product). my gut says max is best to use, but let's see.

### OVERVIEW ###

It’s a work in progress, but we can currently run scripts to simulate results in Walking the Interactome by Kohler et al. We plan to build off this implementation in various directions. A current write-up is given in `walking_the_interactome_summary.pdf`.

### HOW TO RUN ###

Run `rwr_and_diffusion_kernel.jl` using [Julia](http://julialang.org/). A typical command will probably look like

    julia rwr_and_diffusion_kernel.jl --dload disease_gene_data/ --rwr 0.30 --dk .00001 -mpc /path/to/index/file /path/to/edge/list

which will run random walk with restart with r=0.30 and diffusion kernel with \beta = 0.00001 on the monogenic, polygenic, and cancer data.

More details on the flags can be found by running

    julia rwr_and_diffusion_kernel --help

in the command line.

### NETWORKS ###

We're using iRef14, IntHint2, and another version of iRef. iRef14 currently gives the best results though.

### DISEASE GENE FILE FORMAT ###

The disease gene files (`monogenic_data.txt`, ...) are formatted like so:

    disease 1
    number of disease genes for disease 1
    disease gene 1
    number of candidate genes for disease gene 1
    candidate gene 1
    candidate gene 2
    ...
    disease gene 2
    ...
    ...
    disease 2
    ...

These candidate genes associated to a disease gene are the 100 (or 200) genes closest to it via genomic distance, on the same chromosome.

### THINGS TO DO (also old) ###

* Color similarity vs. edge density plots according to disease class.

DONE

* Make x-axis logarithmic for similarity vs. edge density plots.

DONE

* For finding maximum partitions, for each part of a partition try looking at minimum similarity instead of average similarity (to get rid of the size dependence?). Or (my idea) try normalizing the columns of K and then finding maximal partitions.

Partitions take way too long to run, so not going to try

* But regardless, should not spend too much time w/ partitions. Instead, work with Ben's idea: start with small \eps-balls around each point in a disease-gene family (either looking at a similarity kernel or the adjacency matrix), and gradually make them larger and larger. The idea is that, if the gene set really is clustered, then the balls should take a while to merge together.

DONE. included plots in plots folder, will add to LaTeX summary

### THINGS TO DO (old) ###

* Part 1: Use violin plot/jitter scatter plot to visualize distribution of rankings for each disease gene in a class (monogenic, polygenic, cancer). Not sure what plotting software to use. Might look at MatPlotLib, Gadfly, or Winston (used Plotly for the summary, but had to do too much out-of-code work). 

DONE, see pdf

* Also, per Max's suggestion, plot disease gene rank versus degree of disease gene. Might be interesting to note that patterns arise.

DONE, see pdf

* Part 2: Anyway, compute densities of clusters of disease genes. Perhaps the monogenic diseases have high densities, while polygenic/cancer diseases do not. Or maybe the polygenic disease genes have multiple clusters; might also be useful to see of there exist some (nontrivial) partitions where the (sum of?) density is high. Can play around with things here, see what patterns I get. Should use their disease data and one of the gene networks we have though.

THINGS DONE (need to better write-up later):

-- Computed average edge densities for monogenic, polygenic, cancer disease gene families. They are all very, very low (monogenic: 0.221, polygenic: 0.079, cancer: 0.124). Indeed, looking at the families many of them have 0 or 1 connection.

Interestingly, if you pick a "random" set of genes (the size s is first picked uniformly (could change this to better match the disease gene families we have, but I don't think it'll change results terribly) between 3 and 20, and then s genes in the network are picked randomly), the edge density is .001 -- .002, which is smaller than even the polygenic disease-gene families.

-- Computed average distance for monogenic, polygenic, cancer disease-gene families. They are pretty low, showing that while disease gene families aren't clustered together, they are very close. Polygenic diseases are further away than monogenic, cancer, which might explain why random walk/diffusion work better on them. (Data: cancer: 1.9757, polygenic: 2.65359, monogenic: 2.15022).

Didn't do "random" set of genes procedure for this since it takes a while to run.

-- Finally, looked at average similarity (using both RWR kernel and diffusion kernel) for monogenic, polygenic, cancer disease-gene families. 

With diffusion kernel (beta = .00001), we get: monogenic: 4.674e-8, polygenic: 1.454e-9, cancer: 1.414e-7. Here, it goes cancer, monogenic, and then polygenic. While these differences are pretty high, we note that getting the diffusion kernel is an exponential process, so it makes sense that they are orders apart.

Repeating the randomized process w/ DK, beta = .00001, we get 2e-11 -- 4e-11, much smaller than any of the similarities we get above.

With the RWR kernel, we get: monogenic: 1.277e-3, polygenic: 2.129e-4, cancer: 4.577e-4. Here, monogenic is the highest by a long shot, and since the kernel isn't created through exponentiation, the difference seems quite high. What causes the difference here? Perhaps the asymmetry of this kernel takes into account the topology of the network more.

Randomized process w/ RWR kernel, alpha = 0.35, we get 2.004e-5, smaller than all of the above.

-- Created plots of edge density vs. similarity for every disease-gene family. The plot looks like sqrt(x) (for edge density x axis, similarity y-axis), although it's much cleaner for the diffusion kernel than for the RWR kernel.

-- Tried doing partition stuff, but as I found out, this takes a very long time. Even when using parallel computing w/ coho and other optimizations (not looking at every partition), it takes 18 hours for just one disease-gene family of length 15. Looked at partitioning disease-gene families through similarity, since many families have very low edge density. We look for the partition that maximizes total similarity density.

Because of time issues, mostly worked with partitioning the disease-gene families into 2 sets. The general trend seems to be that, for reasonably large disease-gene families in any disease-gene class, the partition that maximizes total edge density/similarity has one small part of size 3 or 4 and lumps the rest into a big group. This isn't a very interesting result, since smaller gene sets naturally have larger edge densities.

Changing the denominator in the similarity density calculation from O(n^2) to O(n) lets some partitions parts of size 4 or 5 pass through, but the trend described above still seems to hold.

This seems to be a result intrinsic to our ranking of partitions. It would be cool to find a size-agnostic way to do this.

* For instance, might be interesting to see what happens if I repeat the Walking the Interactome process with random sets of genes (rather than specific disease genes) and see what happens? The sets of genes could also be highly clustered or divided rather than purely random. 

Not sure how useful this will be, since it won't tell us anything about the difference between the disease classes themselves.. Will probably do a bit more with 

* Ask Max about clique-finders. Compare these to cliques found by examining diffusion kernel K, random walk w/ restart PPR.

Will work on later.