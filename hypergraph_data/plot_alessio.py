import numpy as np
import matplotlib.pyplot as plt

N = 3

simple = (9.587240463686248, 11.17274531024531, 6.010513329263329)
#weighted = (9.618850810642186, 12.038281625781627, 5.885841473341475)
unweighted = (9.254924762407324, 11.303403078403079, 5.634943528693527)

#alessio_weighted = (9.586803485565378, 11.323292448292449, 5.96969881969882)
alessio_unweighted = (9.38602026364392, 10.921885521885521, 5.752742165242165)



#whoops these are wrong
#weighted = (8.097587689383818, 11.99806998556999, 10.546845746845747)
#alessio_weighted = (7.708131188018628, 10.037554112554114, 8.917997049247049)
#alt_unweighted = (7.950003965772829, 10.853571428571428, 9.447178978428978)
#alessio_alt_unweighted = (7.81522242905407, 10.129593554593553, 8.785897435897436)

ind = np.arange(N)  # the x locations for the groups
width = 0.15       # the width of the bars

fig, ax = plt.subplots()

rects1 = ax.bar(ind, simple, width, color='r')
rects2 = ax.bar(ind + width, unweighted, width, color='y')
rects3 = ax.bar(ind + (2*width), alessio_unweighted, width, color='b')
#rects4 = ax.bar(ind + (3*width), alessio_weighted, width, color='m')
#rects5 = ax.bar(ind + (4*width), alessio_unweighted, width, color='g')

# add some text for labels, title and axes ticks
ax.set_ylabel('Average Rank')
ax.set_xlabel('Disease Type')
ax.set_xticks(ind + (1.5)*width)
ax.set_xticklabels(('Monogenic', 'Polygenic', 'Cancer'))

ax.set_title('Average Rank w/ PageRank Kernel (r=0.4) on ReactomeFI')

#l = ax.legend((rects1[0], rects2[0], rects3[0], rects4[0], rects5[0]), ('Simple Graph RWR', 'Edge-weighted Hypergraph RWR w/ Max Cliques', 'Unweighted Hypergraph RWR w/ Max Cliques', 'Edge-weighted Hypergraph RWR w/ Alessio', 'Unweighted Hypergraph RWR w/ Alessio'))
l = ax.legend((rects1[0], rects2[0], rects3[0]), ('Simple Graph RWR', 'Unweighted Hypergraph RWR w/ Max Cliques', 'Unweighted Hypergraph RWR w/ Alessio'))

plt.ylim([0, 17])
plt.savefig('reactomeFI_NEW_unweighted_hypergraph_results.png')

plt.show()