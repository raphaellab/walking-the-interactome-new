#!/usr/bin/env bash

scripts=/data/compbio/datasets/InfluenceMatrices/networks/scripts

network=reactome
data=/data/compbio/datasets/InfluenceMatrices/networks/ICGC/Reactome
num_cores=25
num_permutations=100

# Create directories.
mkdir -p $data
mkdir -p $data/permuted

# Extract largest connected component of network.
echo Extracting largest connected component of network...
python $scripts/merge_networks.py \
    -iig $data/processed_data/"$network"_index_gene.tsv \
    -iel $data/processed_data/"$network"_edge_list.tsv \
    -oig $data/"$network"_index_gene.tsv \
    -oel $data/"$network"_edge_list.tsv \
    -c

# Choose beta.
echo Choosing beta...
python $scripts/choose_beta.py \
    -i $data/"$network"_edge_list.tsv \
    -o $data/"$network"_beta.txt

beta=`cat $data/"$network"_beta.txt`

# Create PPR matrix.
echo Creating PPR matrix...
python $scripts/create_ppr_matrix.py \
    -i $data/"$network"_edge_list.tsv \
    -b $beta \
    -o $data/"$network"_hn2_"$beta".h5

# Create permuted networks.
echo Creating permuted networks...
parallel -u -j $num_cores --bar python $scripts/permute_network.py \
    -i $data/"$network"_edge_list.tsv \
    -s {} \
    -q 200 \
    -o $data/permuted/"$network"_edge_list_{}.tsv ::: `seq 1 $num_permutations`

# Create PPR matrices for permuted networks.
echo Creating PPR matrices with permuted networks...
parallel -u -j $num_cores --bar python $scripts/create_ppr_matrix.py \
    -i $data/permuted/"$network"_edge_list_{}.tsv \
    -b $beta \
    -o $data/permuted/"$network"_hn2_"$beta"_{}.h5 ::: `seq 1 $num_permutations`
