#!/usr/bin/env bash

python process_network.py \
    -i   ../raw_data/FIsInGene_031516_with_annotations.txt \
    -oig ../processed_data/reactome_index_gene.tsv \
    -oel ../processed_data/reactome_edge_list.tsv
