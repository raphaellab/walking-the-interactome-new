import numpy as np
import matplotlib.pyplot as plt

N = 3

simple = (9.587240463686248, 11.17274531024531, 6.010513329263329)
weighted = (8.097587689383818, 11.99806998556999, 10.546845746845747)
unweighted = (8.115445197900808, 11.451202501202502, 9.846133496133495)
alt_weighted = (7.975019605033872, 11.79279701779702, 10.469246031746032)
alt_unweighted = (7.950003965772829, 10.853571428571428, 9.447178978428978)

ind = np.arange(N)  # the x locations for the groups
width = 0.15       # the width of the bars

fig, ax = plt.subplots()

rects1 = ax.bar(ind, simple, width, color='r')
rects2 = ax.bar(ind + width, weighted, width, color='y')
rects3 = ax.bar(ind + (2*width), unweighted, width, color='b')
rects4 = ax.bar(ind + (3*width), alt_weighted, width, color='g')
rects5 = ax.bar(ind + (4*width), alt_unweighted, width, color='m')

# add some text for labels, title and axes ticks
ax.set_ylabel('Average Rank')
ax.set_xlabel('Disease Type')
ax.set_xticks(ind + (2.5)*width)
ax.set_xticklabels(('Monogenic', 'Polygenic', 'Cancer'))

ax.set_title('Average Rank w/ PageRank Kernel on ReactomeFI')

l = ax.legend((rects1[0], rects2[0], rects3[0], rects4[0], rects5[0]), ('Simple Graph RWR', 'Weighted Hypergraph RWR', 'Unweighted Hypergraph RWR', 'Weighted Hypergraph RWR with Cliques', 'Unweighted Hypergraph RWR with Cliques'))

plt.ylim([0, 20])
plt.savefig('reactomeFI_hypergraph_results.png')

plt.show()