#!/usr/bin/env bash

scripts=/data/compbio/datasets/InfluenceMatrices/networks/scripts

network=reactome
data=/data/compbio/datasets/InfluenceMatrices/networks/ICGC/Reactome
num_cores=25

beta=`cat $data/"$network"_beta.txt`

## Create permuted networks.
#echo Creating permuted networks...
#parallel -u -j $num_cores --bar python $scripts/permute_network.py \
#    -i $data/"$network"_edge_list.tsv \
#    -s {} \
#    -q 200 \
#    -o $data/permuted/"$network"_edge_list_{}.tsv ::: `seq 101 1000`

# Create PPR matrices for permuted networks.
echo Creating PPR matrices with permuted networks...
parallel -u -j $num_cores --bar python $scripts/create_ppr_matrix.py \
    -i $data/permuted/"$network"_edge_list_{}.tsv \
    -b $beta \
    -o $data/permuted/"$network"_hn2_"$beta"_{}.h5 ::: `seq 1 250`
