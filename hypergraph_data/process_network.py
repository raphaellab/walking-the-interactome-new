#!/usr/bin/python

# Load modules.
import sys, argparse

# Functions
def get_parser():
    description = 'Merge multiple networks'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-i', '--input_file', required=True, type=str,
        help='Input edge list filename')
    parser.add_argument('-oel', '--output_edge_list_file', required=True, type=str,
        help='Output edge list filename')
    parser.add_argument('-oig', '--output_index_gene_file', required=True, type=str,
        help='Output index gene filename')
    return parser

def run(args):
    # Load data.
    nodes = set()
    edges = set()
    with open(args.input_file, 'r') as f:
        for i, l in enumerate(f):
            entries = l.strip().split('\t')
            if i!=0:
                u = entries[0]
                v = entries[1]
                annotation = entries[2]
                direction = entries[3]
                score = float(entries[4])

                if score>=0.75:
                    nodes.add(u)
                    nodes.add(v)
                    edges.add(frozenset([u, v]))

    sorted_nodes = sorted(nodes)
    sorted_edges = sorted(sorted(edge) for edge in edges)

    # Create gene-to-index dictionary and index-valued edge list.
    gene_to_index = dict((gene, i+1) for i, gene in enumerate(sorted_nodes))
    index_gene_list = sorted((i, gene) for gene, i in gene_to_index.iteritems())
    edge_list = sorted(sorted((gene_to_index[u], gene_to_index[v])) for u, v in sorted_edges)

    # Save combined network.
    with open(args.output_index_gene_file, 'w') as f:
        f.write('\n'.join('\t'.join(map(str, (i, gene))) for i, gene in index_gene_list))
    with open(args.output_edge_list_file, 'w') as f:
        f.write('\n'.join('\t'.join(map(str, (i, j))) for i, j in edge_list))

if __name__ == "__main__":
    run(get_parser().parse_args(sys.argv[1:]))
