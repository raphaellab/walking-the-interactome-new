\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{\scshape Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{\scshape Hypergraphs}{8}{0}{2}
\beamer@sectionintoc {3}{\scshape Random Walks}{15}{0}{3}
\beamer@sectionintoc {4}{\scshape Equivalent Random Walks Between Graphs and Hypergraphs}{22}{0}{4}
\beamer@sectionintoc {5}{\scshape Applications to Disease-Gene Prioritization}{30}{0}{5}
\beamer@sectionintoc {6}{\scshape Conclusion}{45}{0}{6}
