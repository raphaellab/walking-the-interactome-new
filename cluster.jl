using JLD
using Iterators
using ArgParse
include("rwr_and_diffusion_kernel.jl")

################################################################################################################
# EDITING DISEASE DICTS
# Functions that edit the disease dicts
################################################################################################################

# remove gene lists that are of sze 1 or 0
@everywhere function remove_small_lists(dict)
    new_dict = Dict()
    for k in keys(dict)
        if length(dict[k]) > 1
            new_dict[k] = dict[k]
        end
    end
    return new_dict
end

# take normal disease dict and remove candidates
# thus gene_dict will map disease --> (list of disease genes in network)
# after this, use remove_small_lists to filter
function remove_candidates(x_dict, gene_to_num)
    new_dict = Dict()
    for disease in keys(x_dict)
        genes = collect(keys(x_dict[disease]))
        new_genes = String[]
        for i in range(1, size(genes,1))
            if haskey(gene_to_num, genes[i])
                push!(new_genes, genes[i])
            end
        end
        new_dict[disease] = new_genes
    end
    return new_dict
end

################################################################################################################
# CLUSTER DENSITY/COUNT
# Functions that compute cluster counts/densities
################################################################################################################

# given a gene_list, computes the cluster coefficient of this set
# cc = (number of connections in gene_list)/(nC2), where n = length(gene_list)
# aka, how close is it to being a full cluster
@everywhere function cluster_density(gene_list, A, gene_to_num)

    if length(gene_list) < 2
        return -1
    end

    connection_count = 0
    for i in gene_list
        for j in gene_list
            if haskey(gene_to_num, i) & haskey(gene_to_num, j)
                i_num = gene_to_num[i]
                j_num = gene_to_num[j]
                connection_count = connection_count + ((A[i_num,j_num]==1) & (i_num > j_num))
            end
        end
    end
    n = length(gene_list)
    # println("connection count: " * string(connection_count))
    # println("n: " * string(n))
    return connection_count/(n*(n-1)/2)
end

# above function, but just returns number of connections in gene_list
@everywhere function cluster_density_count(gene_list, A, gene_to_num)
    if length(gene_list) < 2
        return -1
    end
    connection_count = 0
    for i in gene_list
        for j in gene_list
            i_num = gene_to_num[i]
            j_num = gene_to_num[j]
            connection_count = connection_count + ((A[i_num,j_num]==1) & (i_num > j_num))
        end
    end
    return connection_count
end

# gets cluster coefficient average over a disease dict
@everywhere function cluster_density_dict(dis_dict, A, gene_to_number)
    new_dict = Dict()
    for dis in keys(dis_dict)
        c = cluster_density(dis_dict[dis], A, gene_to_number)
        new_dict[dis] = c
    end
    return new_dict
end

################################################################################################################
# CLUSTER PARTITIONS
# Functions that compute maximal cluster partitions
################################################################################################################

#given a partition, compute total number of connections in each part
@everywhere function cluster_density_partition_list(partition, A, gene_to_num)
    s=0
    for part in partition
        #println(part)
        #println(s)
        #s += cluster_density_count(part, A, gene_to_num) # i shouldn't look at raw count
        s += cluster_density(part, A, gene_to_num)
    end
    return s
end

#checks if partition is "valid" -- every part has size > 2
#since otherwise, it's not a partition we care about
@everywhere function is_valid_partition(p)
    if p==nothing
        return false
    end
    to_return = true
    for part in p
        to_return = to_return & (length(part) > 2)
    end
    return to_return
end

#do i need this?
#anyway, get the max of tuples for some position
@everywhere function tuple_max(t1, t2, pos=2)
    if t1[pos] > t2[pos]
        return t1
    else
        return t2
    end
end

#helper for parallelized function before
@everywhere function max_cluster_partition_par_h(part, A, gene_to_num)
    if is_valid_partition(part)
        return (part, cluster_density_partition_list(part, A, gene_to_num))
    else
        return (part, 0)
    end
end

#computes the max cluster partition of a gene_list (the partition w/ largest cluster coef)
#parallelized
@everywhere function max_cluster_partition_par(gene_list, A, gene_to_num)
    if (length(gene_list) < 6)
        to_return = Array[]
        push!(to_return, gene_list)
        return (to_return, cluster_density(gene_list, A, gene_to_num))
    else
        return reduce(tuple_max, pmap((x) -> max_cluster_partition_par_h(x, A, gene_to_num), partitions(gene_list)))
    end
end

#computes max cluster partition serially
@everywhere function max_cluster_partition(gene_list, A, gene_to_num)
    to_return = Array[]
    if length(gene_list) < 6
        push!(to_return, gene_list)
        return (to_return, cluster_density(gene_list, A, gene_to_num))
    else
        max_part = Array[]
        max = 0

        p = Progress(length(partitions(1:length(gene_list))),1)
        for part=partitions(gene_list)
            if is_valid_partition(part)
                c=cluster_density_partition_list(part, A, gene_to_num)
                if c > max
                    max = c
                    max_part = part
                end
            end
            next!(p)
        end
        n = length(gene_list)
        return (max_part, max)
    end
end

################################################################################################################
# AVERAGE SIMILARITY
# Functions that compute similarity/average similarity among gene sets, given some kernel K
################################################################################################################

@everywhere function avg_simil(gene_list, K, gene_to_num)

    tot=0
    n = length(gene_list)

    if n < 2
        return -1
    end

    for i=1:n
        for j=i+1:n
            gene1 = gene_list[i]
            gene2 = gene_list[j]
            score = (K[gene_to_num[gene1],gene_to_num[gene2]] + K[gene_to_num[gene1], gene_to_num[gene2]])/2
            if score < 0
                score = -1*score
            end
            tot += score
        end
    end
    denominator = n*(n-1)/2 #maybe change to n?
    #denominator = n
    return tot/denominator
end

# computes gmean instead of sum/nC2.
function avg_simil_new(gene_list, K, gene_to_num)
    tot=0
    n=length(gene_list)
    simil_list = FloatingPoint[]

    if n < 2
        return -1
    end

    for i=1:n
        for j=i+1:n
            gene1 = gene_list[i]
            gene2 = gene_list[j]
            if haskey(gene_to_num, gene1) & haskey(gene_to_num, gene2)
                score=(K[gene_to_num[gene1],gene_to_num[gene2]] + K[gene_to_num[gene1], gene_to_num[gene2]])/2
                if score<0
                    score = -1 * score
                end
                push!(simil_list,score)
            end
        end
    end
    return gmean(simil_list)
end

#does avg_simil for each of the values in the dict
function avg_simil_dict(x_dict, K, gene_to_num)
    new_dict = Dict()
    for i in keys(x_dict)
        new_dict[i] = avg_simil(x_dict[i], K, gene_to_num)
    end
    return new_dict
end

#does avg_simil_new for each of the values in the dict
function avg_simil_dict_new(x_dict, K, gene_to_num)
    new_dict = Dict()
    for i in keys(x_dict)
        new_dict[i] = avg_simil_new(x_dict[i], K, gene_to_num)
    end
    return new_dict
end

################################################################################################################
# AVERAGE SIMILARITY CLUSTERING
# Functions that find clusters with maximal similarity
################################################################################################################

@everywhere function simil_max(t1, t2)
    if !(is_valid_partition(t1[1])) && !(is_valid_partition(t2[1]))
        return (nothing, 0)
    elseif !(is_valid_partition(t1[1]))
        return t2
    elseif !(is_valid_partition(t2[1]))
        return t1
    else
        if t1[2] > t2[2]
            return t1
        else
            return t2
        end
    end
end

# see here: http://stackoverflow.com/questions/31442027/julia-parallel-for-loop-over-partitions-iterator#31469268
function my_take(iter,state,n)
    i = n
    arr = Array[]
    while !done(iter,state) && (i>0)
        a,state = next(iter,state)
        push!(arr,a)
        i = i-1
    end
    return arr, state
end
    

@everywhere function simil_partition(part, K, gene_to_num)
    t = 0
    for p in part
        t = t + avg_simil(p, K, gene_to_num)
    end
    return (part, t)
end

function max_simil_partition(gene_list, K, gene_to_num, num_parts)
    l = length(gene_list)
    up_to = int(round(l/3))
    if num_parts != 0
        up_to = num_parts
    end
    c = Any[]
    for i=1:up_to
        push!(c, partitions(gene_list, i))
    end
    c = chain(c...)
    glob_max = (nothing, 0)
    for part in c
        p = simil_partition(part, K, gene_to_num)
        if p[2] > glob_max[2]
            glob_max = simil_max(glob_max, p)
        end
    end
    return glob_max
end

# the REPL was complaining about the parallel for loops for some reason

#set num_parts=0 if u want all partitions
# @everywhere function max_simil_partition_par_1(gene_list, K, gene_to_num, num_parts)
#     p=[]
#     if num_parts == 0
#         p = partitions(gene_list)
#     else
#         p = partitions(gene_list, num_parts)
#     end
 
#     s = start(p)
#     glob_max = (nothing, 0)
#     t=0
#     while !done(p,s)
#         arr, s = my_take(p, s, 10000)
#         cur_max = @parallel (simil_max) for a in arr
#             simil_partition(a, K, gene_to_num)
#         end
#         t += 10000
#         println("progress: " * string(t))
#         if cur_max[2] > glob_max[2]
#             glob_max = cur_max
#         end
#     end
#     return glob_max
# end

# @everywhere function max_simil_partition_par_2(gene_list, K, gene_to_num, num_cores, num_parts=0)
#     l = length(gene_list)
#     up_to = int(round(l/3))
#     if num_parts != 0
#         up_to = num_parts
#     end
#     c = Any[]
#     for i=1:up_to
#         push!(c, partitions(gene_list, i))
#     end
#     c = chain(c...)
#     to_return = @parallel (simil_max) for i=1:num_cores
#         reduce(simil_max, map((x) -> simil_partition(x, K, gene_to_num), takenth(chain(1:29,drop(c, i-1)), 30)))
#     end
#     return to_return
# end



################################################################################################################
# AVERAGE DISTANCE
# Functions that compute distance/average distance among gene sets
################################################################################################################

# it's better to filter BEFORE adding to the queue rather than after
# because otherwise the queue can become huge and a lot of the things in it are repeats
@everywhere function distance(A, start_gene, end_gene)
    to_iterate = Queue(Any)
    in_queue = Set()
    add_list_to_queue(to_iterate, [start_gene], -1, in_queue)
    #visited = Set()
    while !isempty(to_iterate)
        cur_node, cur_dist = front(to_iterate)
        # println(visited)
        # println(to_iterate)
        #println(front(to_iterate))
        if cur_node == end_gene
            return cur_dist
        end
        # if cur_node in visited
        #     continue
        # end
        #println(to_iterate)
        add_list_to_queue(to_iterate, children(A, cur_node), cur_dist, in_queue)
        #push!(visited, cur_node)
        dequeue!(to_iterate)
    end
    return -1
end

@everywhere function children(A, gene)
    return find(A[gene,:])
end

@everywhere function add_list_to_queue(queue, list, dist, in_queue)
    # dist is distance to parent of l 
    for item in list
        if !(item in in_queue)
            enqueue!(queue, (item, dist+1))
            push!(in_queue, item)
        end
    end
end

@everywhere function avg_dist(gene_list, A, gene_to_num)
    tot_dist = 0
    num_pairs = 0
    disconnected = String[]

    if length(gene_list) < 2
        return [0]
    end

    for i=1:length(gene_list)
        for j=i+1:length(gene_list)
            gene1 = gene_list[i]
            gene2 = gene_list[j]
            if (gene1 in disconnected) | (gene2 in disconnected)
                println("hey! don't go here.")
                continue
            end
            d = distance(A, gene_to_num[gene1], gene_to_num[gene2])
            if d == -1
                disconnected = [disconnected gene1]
            else
                tot_dist += d
                num_pairs += 1
            end
        end
    end

    avg = tot_dist / num_pairs

    if !isempty(disconnected)
        return [avg avg_dist(disconnected, A, gene_to_num)]
    else
        return [avg]
    end
end


@everywhere function avg_dist_par_helper(gene_list, A, gene_to_num, comb)
    gene1 = gene_list[comb[1]]
    gene2 = gene_list[comb[2]]
    return distance(A, gene_to_num[gene1], gene_to_num[gene2])
end

#assumes connectedness
@everywhere function avg_dist_par(gene_list, A, gene_to_num)
    if length(gene_list) < 2
        return [0]
    end

    n = length(gene_list)
    two_combs = n*(n-1)/2 

    return sum(pmap((x) -> avg_dist_par_helper(gene_list, A, gene_to_num, x), combinations(1:length(gene_list), 2)))/two_combs
end

################################################################################################################
# SIMILARITY \eps-BALLS
# Stuff w/ \eps-balls
################################################################################################################

#get the \eps-ball surrounding a gene in kernel K
function get_eps_ball(gene, K, eps)
    to_return = Int[]
    for i=1:size(K,1)
        if (K[gene, i] > eps)
            push!(to_return, i)
        end
    end
    return to_return
end

function get_largest_value(d)
    v = collect(values(d))
    if length(v) == 0
        return 1
    else
        return maximum(v) + 1
    end
end

function get_clusters(gene_ball_list,eps)

    gene_to_cluster = Dict()
    for i=1:length(gene_ball_list)
        in_cluster = false

        for j=(i-1):-1:1
            # if (length(gene_ball_list[1]) > 5)
            #     println(length(gene_ball_list[1]))
            # end
            if length(intersect(Set(gene_ball_list[i]), Set(gene_ball_list[j]))) > 0
                # add it to j's cluster and be done

                if !in_cluster
                    gene_to_cluster[i] = gene_to_cluster[j]
                    in_cluster = true

                # need to do some messing around
                else
                    current_cluster = gene_to_cluster[i]
                    cluster_j = gene_to_cluster[j]

                    # get everything else in i's cluster or j's cluster
                    in_cluster_i_or_j = Int[]
                    for k=1:length(gene_ball_list)
                        if haskey(gene_to_cluster, k)
                            if (gene_to_cluster[k] == current_cluster) | (gene_to_cluster[k] == cluster_j)
                                push!(in_cluster_i_or_j, k)
                            end
                        end
                    end
                    push!(in_cluster_i_or_j, i)
                    push!(in_cluster_i_or_j, j)


                    # add everything in i's cluster to j's cluster
                    for k=1:length(in_cluster_i_or_j)
                        gene_to_cluster[in_cluster_i_or_j[k]] = min(gene_to_cluster[j], gene_to_cluster[i])
                    end

                end
            end
        end

        if !in_cluster
            gene_to_cluster[i] = get_largest_value(gene_to_cluster)
        end
    end

    #println(gene_to_cluster)
    #return renormalize_g2c(gene_to_cluster)
    v=visualize_g2c(gene_to_cluster)
    return v
end


#this is a terrible idea btw

#tfw 2 weeks later you realize theres a bug here and this really was a terrible idea
function renormalize_g2c(gene_to_cluster)
    s = sort(collect(zip(values(gene_to_cluster), keys(gene_to_cluster))))
    g2c = Dict()

    #essentially, s = [(1,1), (1,2), (3,3), ...]
    #aka list of (value, key) sorted by value
    #so we can iterate through s

    real_index = 1
    cur_index = s[1][1]
    for i=1:length(s)
        if s[i][1] == cur_index
            cur_key = s[i][2]
            g2c[cur_key] = real_index
        else
            real_index = real_index + 1
            cur_index = s[i][1]
            cur_key = s[i][2]
            g2c[cur_key] = cur_index
        end
    end
    return g2c

end

function visualize_g2c(gene_to_cluster)
    s = sort(collect(zip(values(gene_to_cluster), keys(gene_to_cluster))))
    cur = 0
    real_ind = 0
    cluster_list = Any[]

    for i=1:length(s)
        if s[i][1] == cur
            push!(cluster_list[real_ind], s[i][2])
        else
            push!(cluster_list, [s[i][2]])
            cur = s[i][1]
            real_ind = real_ind + 1
        end
    end

    return cluster_list
end



function grow(gene_list, K, eps_list)
    #eps_list = logspace(-5, -20, 10000) #lol
    num_cluster_list = ones(length(eps_list))
    num_clusters = length(gene_list)
    num_non_disease = zeros(length(eps_list))
    c = 1
    gene_to_cluster = Dict()
    prev_clusters = Any[]
    clust_size_avg = zeros(length(eps_list))
    while (num_clusters > 1) & (c < (length(eps_list) + 1))

        # things to do in each iteration
        # 1. increase epsilon
        # 2. get \eps-balls around each gene in gene_list
        # 3. determine clusters by looking at pairwise intersections. update num_clusters
        # 4. determine number of non disease genes in the \epsilon balls. update num_non_disease

        # don't need to be clever about 3, 4 since we'll have to look at all pairwise intersections anyway

        eps = eps_list[c]

        # get the \eps-balls for each gene
        gene_ball_list = Array[]
        for i=1:length(gene_list)
            push!(gene_ball_list, get_eps_ball(gene_list[i], K, eps))
        end

        # get clusters, update num_clusters
        #cluster_list = visualize_g2c(gene_to_cluster)
        cluster_list = get_clusters(gene_ball_list, eps)
        #println(cluster_list)
        if cluster_list != prev_clusters
            #println(cluster_list)
            prev_clusters = cluster_list
        end
        num_cluster_list[c] = length(cluster_list)

        # get number of non-disease genes in clusters
        n = length(union(gene_ball_list...))
        num_non_disease[c] = n

        #get avg per cluster
        avg_per_clust = 0
        for part in cluster_list
            tot_in_clust = Set()
            for i in part
                tot_in_clust = union(tot_in_clust, gene_ball_list[i])
            end
            avg_per_clust += length(tot_in_clust)
        end
        avg_per_clust = avg_per_clust / length(cluster_list)
        if length(cluster_list) > 1
            clust_size_avg[c]= avg_per_clust
        end

        num_clusters = num_cluster_list[c]
        c += 1

    end
    #println(c-1)
    #println(eps_list[c-1])
    #println(num_clusters)
    #return eps_list[c-1], eps_list, num_cluster_list, num_non_disease, clust_size_avg
    return eps_list[c-1], num_non_disease[c-1]
end

function grow_adjusted(gene_list, K, eps_list=logspace(-2, -20, 1000))
    eps,n = grow(gene_list, K, eps_list)
    l = logspace(log(10,eps*1.06), log(10,eps), 1000)
    #l = logspace(-5, log(10, eps), 1000)
    return grow(gene_list, K, l)
end

function compare_clust_rank_eps_plot_lists(cand_dict, gene_to_num, K, eps_list)
    cum_clust = FloatingPoint[]
    cum_rank = FloatingPoint[]
    cum_eps = FloatingPoint[]
    dis_gene_dict=remove_candidates(cand_dict, gene_to_num)

    ranks_dict = dict_results(cand_dict, gene_to_num, Array[K])

    for dis in keys(ranks_dict)
        #println(dis)
        ranks = ranks_dict[dis]
        ranks = filter((x) -> (x!= 100), ranks)
        if length(ranks) > 1
            avg_rank = sum(ranks)/length(ranks)
            gene_list = dis_gene_dict[dis]
            gene_list = map((x) -> gene_to_num[x], gene_list)
            eps,non_dis = grow_adjusted(gene_list, K, eps_list)
            push!(cum_clust, non_dis)
            push!(cum_rank, avg_rank)
            push!(cum_eps, eps)
            #println(eps)
        end
    end
    return cum_clust, cum_rank, cum_eps
end

###

function grow_cliques(gene_list, K, eps_list)
    num_non_disease = zeros(length(eps_list))
    c = 1
    is_connected = false
    while (!is_connected) & (c < (length(eps_list) + 1))

        # things to do in each iteration
        # 1. increase epsilon
        # 2. get \eps-balls around each gene in gene_list
        # 3. determine clusters by looking at pairwise intersections. update num_clusters
        # 4. determine number of non disease genes in the \epsilon balls. update num_non_disease

        # don't need to be clever about 3, 4 since we'll have to look at all pairwise intersections anyway

        eps = eps_list[c]

        # get the \eps-balls for each gene
        gene_ball_list = Array[]
        for i=1:length(gene_list)
            push!(gene_ball_list, get_eps_ball(gene_list[i], K, eps))
        end

        new_is_connected = true
        for comb in combinations(1:length(gene_list), 2)
            g1 = comb[1]
            g2 = comb[2]
            if length(intersect(Set(gene_ball_list[g1]), Set(gene_ball_list[g2]))) == 0
                new_is_connected = false
                break
            end
        end

        # get number of non-disease genes in clusters
        n = length(union(gene_ball_list...))
        num_non_disease[c] = n

        c += 1

        if new_is_connected == true
            is_connected = true
        end

    end
    #println(c-1)
    println(eps_list[c-1])
    #println(num_clusters)
    #return eps_list[c-1], eps_list, num_cluster_list, num_non_disease, clust_size_avg
    return eps_list[c-1], num_non_disease[c-1]
end

function grow_adjusted_cliques(gene_list, K, eps_list=logspace(-5, -20, 1000))
    eps,n = grow_cliques(gene_list, K, eps_list)
    l = logspace(log(10,eps*1.06), log(10,eps), 1000)
    #l = logspace(-5, log(10, eps), 1000)
    return grow_cliques(gene_list, K, l)
end

function compare_clust_rank_eps_plot_lists_cliques(cand_dict, gene_to_num, K, eps_list)
    cum_clust = FloatingPoint[]
    cum_rank = FloatingPoint[]
    cum_eps = FloatingPoint[]
    dis_gene_dict=remove_candidates(cand_dict, gene_to_num)

    ranks_dict = dict_results(cand_dict, gene_to_num, Array[K])

    for dis in keys(ranks_dict)
        ranks = ranks_dict[dis]
        ranks = filter((x) -> (x!= 100), ranks)
        if length(ranks) > 1
            avg_rank = sum(ranks)/length(ranks)
            gene_list = dis_gene_dict[dis]
            gene_list = map((x) -> gene_to_num[x], gene_list)
            eps,non_dis = grow_adjusted_cliques(gene_list, K, eps_list)
            push!(cum_clust, non_dis)
            push!(cum_rank, avg_rank)
            push!(cum_eps, eps)
        end
    end
    return cum_clust, cum_rank, cum_eps
end

################################################################################################################
# BEST t/r VALUE
# Functions analyzing data on which t/r values are best
################################################################################################################

#eps balls, together with data on which diffusion kernel parameter is the best

function compare_clust_rank_eps_dk_plot_lists(cand_dict, gene_to_num, K, eps_list, dk_dict, dk_t_list=logspace(-1.4, -0.3, 50))
    cum_clust = FloatingPoint[]
    cum_rank = FloatingPoint[]
    cum_eps = FloatingPoint[]
    cum_t = FloatingPoint[]
    cum_t_2 = FloatingPoint[]
    dis_gene_dict=remove_candidates(cand_dict, gene_to_num)

    ranks_dict = dict_results(cand_dict, gene_to_num, Array[K])

    for dis in keys(ranks_dict)
        #println(dis)
        ranks = ranks_dict[dis]
        ranks = filter((x) -> (x!= 100), ranks)
        if length(ranks) > 1
            avg_rank = sum(ranks)/length(ranks)
            gene_list = dis_gene_dict[dis]
            gene_list = map((x) -> gene_to_num[x], gene_list)
            eps,non_dis = grow_adjusted(gene_list, K, eps_list)
            push!(cum_clust, non_dis)
            push!(cum_rank, avg_rank)
            push!(cum_eps, eps)

            # dk stuff

            dk_data = dk_dict[dis]
            #TODO: change below. bad metric.
            dk_data = sum(dk_data, 2) #sum of rows, so each entry is "avg rank". we want smallest... 
            min_ind = findfirst(x->x==minimum(dk_data), dk_data) #index of lowest elt
            t = dk_t_list[min_ind]
            #println("one")
            #println(t)
            push!(cum_t, t)

            # more dk stuff

            dk_data = dk_dict[dis]
            dk_data = prod(dk_data, 2) #prof of rows, so each entry is geometric "avg rank". we want smallest... 
            min_ind = findfirst(x->x==minimum(dk_data), dk_data) #index of lowest elt
            t = dk_t_list[min_ind]
            #println("two")
            #println(t)
            push!(cum_t_2, t)


            #println(eps)
        end
    end
    return cum_clust, cum_rank, cum_eps, cum_t, cum_t_2
end

# compare best t value against degree

function compare_deg_dk_t(dis_dict, gene_to_num, A, dk_dict, dk_t_list=logspace(-1.4, -0.3, 50))
    glob_degs = sum(A, 2) #all degrees
    gmean_degs_to_return = FloatingPoint[] #gmean of degrees for each disease
    best_t = FloatingPoint[] #best t for each disease
    for dis in keys(dk_dict)
        genes = keys(dis_dict[dis])
        gm_list = FloatingPoint[]
        for i in genes
            if haskey(gene_to_num, i)
                push!(gm_list, glob_degs[gene_to_num[i]])
            end
        end
        gm = gmean(gm_list)
        push!(gmean_degs_to_return, gm)


        dk_data = dk_dict[dis]
        dk_data = prod(dk_data, 2) #prof of rows, so each entry is geometric "avg rank". we want smallest... 
        min_ind = findfirst(x->x==minimum(dk_data), dk_data) #index of lowest elt
        t = dk_t_list[min_ind]
        #println("two")
        #println(t)
        push!(best_t, t)
    end
    return gmean_degs_to_return, best_t
end

# compare best t value against cluster density

function compare_clust_dens_dk_t(dis_dict, gene_to_num, A, dk_dict, dk_t_list=logspace(-1.4, -0.3, 50))
    clust_dens_to_return = FloatingPoint[] #gmean of degrees for each disease
    best_t = FloatingPoint[] #best t for each disease
    for dis in keys(dk_dict)
        genes = keys(dis_dict[dis])
        clust_score = cluster_density(genes, A, gene_to_num)
        push!(clust_dens_to_return, clust_score)


        dk_data = dk_dict[dis]
        dk_data = prod(dk_data, 2) #prof of rows, so each entry is geometric "avg rank". we want smallest... 
        min_ind = findfirst(x->x==minimum(dk_data), dk_data) #index of lowest elt
        t = dk_t_list[min_ind]
        #println("two")
        #println(t)
        push!(best_t, t)
    end
    return clust_dens_to_return, best_t
end

# compare best t value against avg similarity (according to... any kernel, I guess)

function compare_avg_simil_dk_t(dis_dict, gene_to_num, K, dk_dict, dk_t_list=logspace(-1.4, -0.3, 50))
    avg_simil_to_return = FloatingPoint[] #gmean of degrees for each disease
    best_t = FloatingPoint[] #best t for each disease
    for dis in keys(dk_dict)
        genes = [g for g in keys(dis_dict[dis])]
        simil_score = avg_simil_new(genes, K, gene_to_num)
        push!(avg_simil_to_return, simil_score)


        dk_data = dk_dict[dis]
        dk_data = prod(dk_data, 2) #prof of rows, so each entry is geometric "avg rank". we want smallest... 
        min_ind = findfirst(x->x==minimum(dk_data), dk_data) #index of lowest elt
        t = dk_t_list[min_ind]
        #println("two")
        #println(t)
        push!(best_t, t)
    end
    return avg_simil_to_return, best_t
end

# compare best t value against "influence": given genes g_i, influence is sum of columns g_i in kernel K

function compare_avg_infl_dk_t(dis_dict, gene_to_num, K, dk_dict, dk_t_list=logspace(-1.4, -0.3, 50))
    avg_infl_to_return = FloatingPoint[] #gmean of degrees for each disease
    best_t = FloatingPoint[] #best t for each disease
    for dis in keys(dk_dict)
        genes = [g for g in keys(dis_dict[dis])]
        infl_score = avg_influence(genes, K, gene_to_num)
        push!(avg_infl_to_return, infl_score)


        dk_data = dk_dict[dis]
        dk_data = prod(dk_data, 2) #prof of rows, so each entry is geometric "avg rank". we want smallest... 
        min_ind = findfirst(x->x==minimum(dk_data), dk_data) #index of lowest elt
        t = dk_t_list[min_ind]
        #println("two")
        #println(t)
        push!(best_t, t)
    end
    return avg_infl_to_return, best_t
end

#for each t (or r), averages rank for each disease and averages those ranks
#res shoudl be of the form(mono_res, poly_res, canc_res)
function combine_results(res, param_list)
    to_return = zeros(length(param_list), 1)
    num_dis = 0
    for i=1:length(x)
        cur_res = res[i]
        num_dis = num_dis + length(keys(cur_res))

        for mat in values(cur_res)
            to_return = to_return + mean(mat, 2)
        end


    end
    return to_return ./ num_dis
end

#plots for monogenic, polygenic, cancer of t vs. avg rank
#does not average, unlike above
function separate_results(res, param_list)
    to_return = zeros(length(param_list), length(res))
    for i=1:length(res)
        num_dis = 0
        cur_res = res[i]
        num_dis = num_dis + length(keys(cur_res))

        for mat in values(cur_res)
            to_return[:,i] = to_return[:,i] + mean(mat, 2)
        end

        to_return[:,i] = to_return[:,i] ./ num_dis
    end
    return to_return
end

################################################################################################################
# MIN LINKAGE
# idea: have complete graph with edges weighted by kernel values
# add edges one at a time. what's the largest edge value u can remove and still
# have clique/connectedness
################################################################################################################

#however, this is a bit harder for undirected graphs. but let's do cliques first since it's easy.
#to maintain a clique, every arrow must be in the graph. so we can just find the min similarity.

function min_clique_linkage(gene_list, K)
    n = length(gene_list)
    min = Inf
    for i=1:n
        for j=(i+1):n
            n1 = gene_list[i]
            n2 = gene_list[j]
            s1 = K[n1,n2]
            s2 = K[n2,n1]
            m = minimum([s1,s2])
            if m < min
                min = m
            end
            # if s1 < min
            #     min = s1
            # end
            # if s2 < min
            #     min = s2
            # end
        end
    end
    return min
end

#now lets do it for linkages

function min_linkage(gene_list, K)
    edge_list = Any[]
    n=length(gene_list)
    for i=1:n
        for j=(i+1):n
            n1 = gene_list[i]
            n2 = gene_list[j]
            push!(edge_list, (K[n1,n2], j, i)) #edge from j to i
            push!(edge_list, (K[n2,n1], i, j)) #edge from i to j
        end
    end

    #this sorts it by first elt in tuple: so the length of the edge
    #with rev=true, the largest simil value (large simil=close together)
    sort(edge_list, rev=true)

    #maps i -> (nodes that can get to i)
    in_dict = Dict()

    #maps i -> (nodes you can get to from i)
    out_dict = Dict()

    for i=1:n
        in_dict[i] = Set([i])
        out_dict[i] = Set([i])
    end

    ind = 1
    cur_edge_length = 0

    while true
        cur_edge = edge_list[ind]
        ind = ind + 1
        cur_edge_length = cur_edge[1]

        #our edge is directed n1 --> n2
        n1 = cur_edge[2]
        n2 = cur_edge[3]

        get_to_from_n1 = out_dict[n1]
        get_to_n1 = in_dict[n1]

        get_to_from_n2 = out_dict[n2]
        get_to_n2 = in_dict[n2]

        #update in_dict
        #for i \in (nodes you can get to from n2)
        #every (node that can get to n1) can get to i
        #so update in_dict[i] by adding nodes that can get to n1

        for node in get_to_from_n2
            in_dict[node] = union(in_dict[node], get_to_n1)
        end


        #update out_dict
        #for i \in (nodes that can get to n1)
        #i can get to (nodes you can get to from n2)
        #so update out_dict[i] by adding nodes you can get to from n2

        for node in get_to_n1
            out_dict[node] = union(out_dict[node], get_to_from_n2)
        end

        #check if we can exit
        new_is_connected = true
        for i=1:n
            if length(out_dict[i]) < n
                new_is_connected = false
                break
            end
        end

        if new_is_connected
            break
        end
    end
    return cur_edge_length
end

function compare_rank_linkage_plot_lists(cand_dict, gene_to_num, K)
    cum_rank = FloatingPoint[]
    cum_eps = FloatingPoint[]
    cum_eps_clique = FloatingPoint[]
    dis_gene_dict=remove_candidates(cand_dict, gene_to_num)

    ranks_dict = dict_results(cand_dict, gene_to_num, Array[K])

    for dis in keys(ranks_dict)
        ranks = ranks_dict[dis]
        ranks = filter((x) -> (x!= 100), ranks)
        if length(ranks) > 1
            avg_rank = sum(ranks)/length(ranks)
            gene_list = dis_gene_dict[dis]
            gene_list = map((x) -> gene_to_num[x], gene_list)
            eps = min_linkage(gene_list, K)
            c_eps = min_clique_linkage(gene_list, K)
            push!(cum_rank, avg_rank)
            push!(cum_eps, eps)
            push!(cum_eps_clique, c_eps)
        end
    end
    return cum_rank, cum_eps, cum_eps_clique
end



################################################################################################################
# MISCELLANEOUS
# Miscellaneous functions
################################################################################################################

#for lists of nonnegative numbers!
# "new" methods add 1 to everything, gmean, and then subtract 1
function gmean(l)
    cur = 0
    len = length(l)
    for i=1:len
        elt = l[i]
        if elt == 0
            return 0
        else
            cur = cur + log(float(elt))
        end
    end
    return exp(cur/len)
end

function gmean_filter_0(l)
    m = filter(x -> x > 0, l)
    return gmean(m)
end

function gmean_new(l)
    cur = 0
    len = length(l)
    for i=1:len
        elt = l[i]
        cur = cur + log(float(elt + 1))
    end
    return exp(cur/len) - 1
end

function gmean_dict(d)
    return gmean(collect(values(d)))
end

function gmean_dict_filter_0(d)
    println(d)
    c=collect(values(d))
    e = Int[]
    for i=1:length(c)
        append!(e, vec(c[i]))
    end
    println(e)
    return gmean(filter(x -> x > 0, e))
end

function gmean_dict_new(d)
    c = collect(values(d))
    e = Int[]
    for i=1:length(c)
        append!(e, vec(c[i]))
    end
    len = length(e)

    #add 1 to everything
    for i=1:len
        e[i] += 1
    end

    #take gmean
    g=gmean(e)

    #subtract 1
    return g-1
end

function print_dict(d)
    for dis in d
        println(dis)
    end
end

#for some reason the below is not working???????
#jk it works but matplotlib looks better

function plot_simil_cluster(dict_list, K, A, gene_to_num)
    #using Plotly
    simil = FloatingPoint[]
    cluster = FloatingPoint[]
    for i=1:length(dict_list)
        dict = dict_list[i]
        for g_list in values(dict)
            simil_score = avg_simil(g_list, K, gene_to_num)
            clust_score = cluster_density(g_list, A, gene_to_num)
            push!(simil, simil_score)
            push!(cluster, clust_score)
        end
    end
    trace = ["x" => cluster, "y" => simil, "type" => "scatter", "mode" => "markers"]
    layout = ["title" => "Edge Density vs. Diffusion Kernel Similarity", "xaxis" => ["title" => "Edge Density"], "yaxis" => ["title" => "Similarity", "type" => "log"]]
    response = Plotly.plot([trace], ["layout" => layout, "filename" => "iref14_dk_similarity_cluster", "fileopt" => "overwrite"])
    plot_url = response["url"]
    println(plot_url)
end

#using PyPlot

function plot_simil_cluster_rand_new(K, A, gene_to_num, num_to_gene,dist)
    gene_range = size(K,1)
    colors = ["red", "yellow", "blue"]

    #commented out stuff was me trying to see if size had any impact on the graph
    #but eh
    #for c=1:3
    for c=3:3
        cluster = FloatingPoint[]
        simil = FloatingPoint[]
        for i=1:10000
            number_of_genes = sample_from_dist(dist)
            #number_of_genes = rand(2:100)
            #number_of_genes = rand((0:3 + 4*c - 2))
            genes = map(x -> num_to_gene[rand(1:gene_range)], 1:number_of_genes)
            simil_score = avg_simil(genes, K, gene_to_num)
            clust_score = cluster_density(genes, A, gene_to_num)
            if clust_score !=0
                push!(simil, simil_score)
                push!(cluster, clust_score)
            end
        end
        plot(cluster, simil, linestyle="None", marker="o", c=colors[c], markersize=4)
    end
    xscale("log")
    yscale("log")
    show()
end

function plot_simil_cluster_mpl(dict_list, K, A, gene_to_num)

    #simil = FloatingPoint[]
    #cluster = FloatingPoint[]
    #colors = FloatingPoint[]
    colors = ["red", "yellow", "blue"]
    labels = ["Monogenic", "Polygenic", "Cancer"]
    c=1
    min = 2
    for i=1:length(dict_list)
        simil = FloatingPoint[]
        cluster = FloatingPoint[]
        dict = dict_list[i]
        for g_list in values(dict)
            simil_score = avg_simil(g_list, K, gene_to_num)
            clust_score = cluster_density(g_list, A, gene_to_num)
            if clust_score==0
                clust_score = 0.005
            end
            push!(simil, simil_score)
            push!(cluster, clust_score)
        end
        plot(cluster, simil, linestyle="None", marker="o", c = colors[c], label=labels[c])
        c += 1
    end
    #scatter(cluster, simil, c=colors) #first is x, second is y

    # classes = ["monogenic","polygenic","cancer"]
    # class_colours = ["r","y","b"]
    # recs = []
    # for i in range(1,length(class_colours))
    #     push!(recs, Rectangle((0,0),1,1,fc=class_colours[i]))
    # end
    # legend(recs,classes,loc=4)

    #scaling
    yscale("log")
    ylim([10.0^-5, 10.0^-1])

    xscale("log")
    xlim([10.0^-2, 10.0^0.2])

    #titling
    suptitle("Similarity versus Edge Density for Various Disease-Gene Families w/ DK, beta=0.0015", fontsize=12)
    xlabel("Edge Density")
    ylabel("Average Similarity")

    legend(loc=4, prop=["size" => 8])

    show()
    #savefig("iref14_rwr_similarity_edgedensity_loglog_with0.pdf")
    #print("Hit <enter> to continue") #so the plot will stay
    #readline()
end

function plot_simil_cluster_mpl_new(dict_list, K, A, gene_to_num)

    #simil = FloatingPoint[]
    #cluster = FloatingPoint[]
    #colors = FloatingPoint[]
    colors = ["red", "yellow", "blue"]
    labels = ["Monogenic", "Polygenic", "Cancer"]
    c=1
    min = 2
    for i=1:length(dict_list)
        simil = FloatingPoint[]
        cluster = FloatingPoint[]
        dict = dict_list[i]
        for g_list in values(dict)
            simil_score = avg_simil_new(g_list, K, gene_to_num)
            clust_score = cluster_density(g_list, A, gene_to_num)
            if clust_score==0
                clust_score = 0.005
            end
            push!(simil, simil_score)
            push!(cluster, clust_score)
        end
        plot(cluster, simil, linestyle="None", marker="o", c = colors[c], label=labels[c])
        c += 1
    end
    #scatter(cluster, simil, c=colors) #first is x, second is y

    # classes = ["monogenic","polygenic","cancer"]
    # class_colours = ["r","y","b"]
    # recs = []
    # for i in range(1,length(class_colours))
    #     push!(recs, Rectangle((0,0),1,1,fc=class_colours[i]))
    # end
    # legend(recs,classes,loc=4)

    #scaling
    yscale("log")
    #ylim([10.0^-5, 10.0^-1])

    xscale("log")
    #xlim([10.0^-2, 10.0^0.2])

    #titling
    #suptitle("Similarity versus Edge Density for Various Disease-Gene Families w/ DK, beta=0.0015", fontsize=12)
    xlabel("Edge Density")
    ylabel("Average Similarity")

    legend(loc=4, prop=["size" => 8])

    show()
    #savefig("iref14_rwr_similarity_edgedensity_loglog_with0.pdf")
    #print("Hit <enter> to continue") #so the plot will stay
    #readline()
end

#given dict mapping disease --> [list of genes in disease]
# get avg of all degrees of all genes in dict
# (genes in >1 list do not get counted more than once)
function avg_deg(degs, x_dict, gene_to_num)
    deg_list = Int[]
    genes_seen = Set()
    for l in values(x_dict)
        # to_add = map(x -> degs[gene_to_num[x]], l)
        # deg_list = [deg_list, to_add]
        for g in l
            if !in(g, genes_seen)
                #println(g)
                if haskey(gene_to_num,g)
                    deg = degs[gene_to_num[g]]
                    push!(deg_list, deg)
                    push!(genes_seen, g)
                end
            end
        end
    end
    #filter(x -> x < 500, deg_list)
    #return mean(deg_list)
    #println(deg_list)
    return gmean(deg_list)
    #return deg_list
end

#uh i need to define influence better...
function avg_influence(genes, K, gene_to_num)
    tot = 0
    tot_num = 0
    for gene in genes
        if haskey(gene_to_num, gene)
            gene_num = gene_to_num[gene]
            
            #attempt 1: sum of column in DK. this was dumb though since columns sum to 1

            #attempt 2: product of column??? no they all just end up being 0.
            
            #attempt 3: sum of degree of nodes connected to initial node


            tot_num = tot_num + 1
        end    
    end
    return tot/tot_num
end

################################################################################################################
# RANDOMIZED STATISTICS
# looking at statistics on "random" sets
################################################################################################################

function create_size_dist(m,p,c)
    size_map = Dict()
    for dis in values(merge(m,p,c))
        s = length(dis)
        if haskey(size_map,s)
            size_map[s] += 1
        else
            size_map[s] = 1
        end
    end

    return size_map
end

#ofc this only works for the dist above lol or anything with min value 2
#but i tested this and it seems to work so yolo
function sample_from_dist(dist)
    #tot = sum(values(dist))

    tot = 106 #i'll hardcode this in to save time
    r = rand(1:tot)

    at = 2
    cur = dist[at]

    while(r > cur)
        at = at + 1
        if haskey(dist, at)
            cur += dist[at]
        end
    end

    return at
end

#turns a dist that looks like (value --> count) to (value --> probability)
function make_prob_dist(dist)
    tot = sum(values(dist))
    new_dist = Dict()
    for i in keys(dist)
        new_dist[i] = dist[i] / tot
    end
    return new_dist
end

function random_cluster(n, A, gene_to_num, num_to_gene, dist)
    t = FloatingPoint[]
    gene_range = size(A,1)
    pc=0
    cc=0
    mc=0
    for i=1:n
        number_of_genes = sample_from_dist(dist)
        genes = rand(1:gene_range, number_of_genes)
        genes = [num_to_gene[genes[i]] for i=1:length(genes)]
        c=cluster_density(genes, A, gene_to_num)
        push!(t, c)
    end
    #return (mean(t), gmean(filter(x -> x > 0, t)))
    #return mean(t)
    return gmean_new(t)
end

function random_simil(n, K, gene_to_num, num_to_gene, dist)
    t = FloatingPoint[]
    gene_range = size(K,1)
    for i=1:n
        number_of_genes = sample_from_dist(dist)
        genes = rand(1:gene_range, number_of_genes)
        genes = [num_to_gene[genes[i]] for i=1:length(genes)]
        push!(t, avg_simil(genes, K, gene_to_num))
    end
    return gmean(t)
    #return gmean_new(t)
end

function random_simil_new(n, K, gene_to_num, num_to_gene, dist)
    t = FloatingPoint[]
    gene_range = size(K,1)
    count=0
    for i=1:n
        number_of_genes = sample_from_dist(dist)
        genes = rand(1:gene_range, number_of_genes)
        genes = [num_to_gene[genes[i]] for i=1:length(genes)]
        a = avg_simil_new(genes, K, gene_to_num)
        push!(t, a)
    end
    return gmean(t)
    #return gmean_new(t)
end

function plot_random_cluster(A, gene_to_num, num_to_gene, dist)
    t = FloatingPoint[]
    gene_range = size(A,1)
    for i=1:10000
        number_of_genes = sample_from_dist(dist)
        genes = rand(1:gene_range, number_of_genes)
        genes = [num_to_gene[genes[i]] for i=1:length(genes)]
        push!(t, cluster_density(genes, A, gene_to_num))
    end
    plot(t, zeros(length(t)) + 0.05*(rand(length(t)) - 0.5), linestyle="None", marker="o")
    #xscale("log")
end

function plot_random_simil(K, gene_to_num, num_to_gene, dist)
    t = FloatingPoint[]
    gene_range = size(K,1)
    for i=1:1000
        number_of_genes = sample_from_dist(dist)
        genes = rand(1:gene_range, number_of_genes)
        genes = [num_to_gene[genes[i]] for i=1:length(genes)]
        push!(t, avg_simil(genes, K, gene_to_num))
    end
    plot(t, zeros(length(t)) + 0.05*(rand(length(t)) - 0.5), linestyle="None", marker="o")
end

function plot_random_simil_new(K, gene_to_num, num_to_gene, dist)
    t = FloatingPoint[]
    gene_range = size(K,1)
    for i=1:1000
        number_of_genes = sample_from_dist(dist)
        genes = rand(1:gene_range, number_of_genes)
        genes = [num_to_gene[genes[i]] for i=1:length(genes)]
        push!(t, avg_simil_new(genes, K, gene_to_num))
    end
    plot(t, zeros(length(t)) + 0.05*(rand(length(t)) - 0.5), linestyle="None", marker="o")
end

################################################################################################################
# CONNECTED COMPONENTS
################################################################################################################
# using LightGraphs
# using GraphLayout

# using TikzGraphs
# using TikzPictures

function get_components(genez, A, gene_to_number)
    g = Graph(length(genez))
    for i=1:length(genez)
        for j=(i+1):length(genez)
            gene_i = gene_to_number[genez[i]]
            gene_j = gene_to_number[genez[j]]
            if A[gene_i, gene_j] == 1
                add_edge!(g, i, j)
            end
        end
    end

    #println(connected_components(g))
    #println(longest_components(connected_components(g)))
    return long_components(connected_components(g))

end

function long_components(comps)
    long_comps = Array[]
    # longest_length = -Inf

    # for i=1:length(comps)
    #     ci = comps[i]
    #     if length(ci) > longest_length
    #         longest_comps = Array[ci]
    #         longest_length = length(ci)
    #     elseif length(ci) == longest_length
    #         push!(longest_comps, ci)
    #     end
    # end
    # return longest_comps

    for i=1:length(comps)
        ci = comps[i]
        if length(ci) > 3
            push!(long_comps, ci)
        end
    end
    return long_comps
end

# to_return : dis -> (LCC of dis, as numbers)
# eg Breast Cancer -> [1, 3, 4, ...]

#also horribly unexpendable.
function get_all_conn_comps(dis_dict, A, gene_to_number)
    to_return = Dict()
    for dis in keys(dis_dict)
        c = get_components(dis_dict[dis], A, gene_to_number)
        if length(c) == 1
            to_return[dis] = c[1]
        elseif length(c) ==2
            if length(c[1]) > length(c[2])
                i=1
            else
                i=2
            end
            to_return[dis] = c[i]
        end
    end
    return to_return
end

#for each disease in dis_dict, plot the largest connected component
function plot_all_conn_comps(dis_dict, A, gene_to_number, dis_class)
    for dis in keys(dis_dict)
        genez = dis_dict[dis]

        long_comps = get_components(genez, A, gene_to_number) #list of longest components

        #length of longest component
        if length(long_comps) > 0
            s = maximum(map((x) -> length(x),long_comps))
        else
            s = 0
        end
        #s = length(long_comps[1])


        #graph long components
        if (s > 3)
            #if only one component
            if length(long_comps) == 1
                comp = long_comps[1]
                plot_comp(comp, genez, A, gene_to_number, dis, dis_class)
            #otherwise plot several components
            else
                for i=1:length(long_comps)
                    comp = long_comps[i]
                    plot_comp(comp, genez, A, gene_to_number, dis*"_"*string(i), dis_class)
                end
            end

        end
    end
end

#helper to plot a connected component
function plot_comp(comp, genez, A, gene_to_number, disease_name, disease_class)
    comp_genes = map((x) -> genez[x], comp) #list of genes, say [TK1, BCA2, BCA1]

    #make graph for comp
    g=Graph(length(comp))
    for i=1:length(comp)
        for j=(i+1):length(comp)
            gene_i = gene_to_number[genez[comp[i]]]
            gene_j = gene_to_number[genez[comp[j]]]
            if A[gene_i, gene_j] == 1
                add_edge!(g, i, j)
            end
        end
    end

    #plot graph, w/ node labels = comp_genes

    #using tikzgraphs
    #t=TikzGraphs.plot(g, comp_genes)
    #save(PDF("component_plots/" * disease_name * ".pdf"), t)

    #using GraphLayout
    am = full(adjacency_matrix(g))
    loc_x, loc_y = layout_spring_adj(am)
    draw_layout_adj(am, loc_x, loc_y, labels=comp_genes, filename="component_plots/" * disease_class * "/" * disease_name * ".svg")
end

#test hypothesis that genes in largest connected component have good rankings
#looks at each "large connected component" and sees how ranks are
function all_rank_conn_comps(dis_dict, A, gene_to_number, res)
    tot = 0
    num=0
    for dis in keys(dis_dict)
        genez = dis_dict[dis]

        long_comps = get_components(genez, A, gene_to_number) #list of longest components
        #length of longest component
        if length(long_comps) > 0
            s = maximum(map((x) -> length(x),long_comps))
        else
            s = 0
        end

        #get ranks for large components

        if (s > 3)
            for i=1:length(long_comps)
                comp = long_comps[i]
                println(map((x) -> genez[x], comp))
                tot=tot+rank_comp(comp, res, dis)
                num = num+1
            end

        end
    end
    println(num)
    println(tot/num)
end

#helper for above. comp=connected component
function rank_comp(comp, res, dis)
    disease_ranks = res[dis]
    comp_ranks = map((x) -> disease_ranks[x], comp)
    println(dis)
    println(comp_ranks)
    return mean(comp_ranks)
end

# want to compare normal results using DK/RWR
# to results where the only initial genes are
# those in the "large connected component"

function lcc_init(dis_dict, A, gene_to_number, cand_dict, kernel)

    conn_comps = get_all_conn_comps(dis_dict, A, gene_to_number)

    conn_cand_dict = cand_dict_conn_comps(dis_dict, A, gene_to_number, cand_dict)
    #println(conn_cand_dict)
    res_dict = dict_results(cand_dict, gene_to_number, Array[kernel])
    conn_res_dict = dict_results(conn_cand_dict, gene_to_number, Array[kernel])
    #println(conn_res_dict)
    for dis in keys(conn_res_dict)
        println(dis)
        println(conn_res_dict[dis])
        println(res_dict[dis][conn_comps[dis]])
    end

end

#not expendable at all since i pretty much assume there's only connected component. 
#an easy hack if there are more: make another dict. future me can deal with this
#really the error is in how the LCCs are gotten

#anyway returns cand_dict : dis -> (gene -> candidate genes)
function cand_dict_conn_comps(dis_dict, A, gene_to_number, cand_dict)
    to_return = Dict() #dis -> (gene -> candidates)

    for dis in keys(dis_dict)
        to_return_dis = Dict() # gene -> candidates

        genez = dis_dict[dis]
        #println(dis)
        long_comps = get_components(genez, A, gene_to_number) #list of longest components
        #length of longest component
        if length(long_comps) > 0
            s = maximum(map((x) -> length(x),long_comps))
        else
            s = 0
        end

        #get all components in to_return
        if (s > 3)
            comp = long_comps[1]
            to_return_genes = map((x) -> dis_dict[dis][x], comp)
            for i=1:length(to_return_genes)
                g_i = to_return_genes[i]
                to_return_dis[g_i] = cand_dict[dis][g_i]
                #println(to_return_dis)
            end

            to_return[dis] = to_return_dis
        end

    end
    return to_return
end

#for each disease gene list [g1, g2, ...], look at each connected component C
#and enlarge C by looking at the neighbors of each g \in C

#n lets you look at neighbors of neighbors ... of neighbors
function get_near_neighbors(dis_dict, A, gene_to_number, n)

    #make graph for A
    n = size(A,1)
    global_graph=Graph(n)
    for i=1:n
        for j=(i+1):n
            if A[i, j] == 1
                add_edge!(global_graph, i, j)
            end
        end
    end

    for dis in keys(dis_dict)
        genez = dis_dict[dis]
        genez_num = map((x) -> gene_to_number[x], genez) #gene numbers in A

        #make graph of disease genes
        dis_g=Graph(length(genez))
        for i=1:length(genez)
            for j=(i+1):length(genez)
                gene_i = gene_to_number[genez[i]]
                gene_j = gene_to_number[genez[j]]
                if A[gene_i, gene_j] == 1
                    add_edge!(dis_g, i, j)
                end
            end
        end


        #get neighbors of each connected component
        #for each connected component C, get the neighbors of every v \in C
        cc = connected_components(dis_g) #connected components
        cc_new = Array[]
        for i=1:length(cc)
            cur_comp = cc[i]
            new_comp = Int[]
            for j=1:length(cur_comp)
                v = cur_comp[j]
                #need to use genez_num below so we can get the numbers in A. 
                #v_neigh = intersect(neighbors(global_graph, genez_num[v]), genez_num) #which neighbors are in the graph?
                v_neigh = get_n_fold_neighbors(genez_num[v], cur_comp, global_graph, n) #get vertices dist <=n away
                new_comp = union(new_comp, map((x) -> findfirst(genez_num,x),v_neigh)) #findfirst so we are mapped back to [1,...,n] 
                new_comp = union(new_comp, [v])
            end
            push!(cc_new, new_comp)
        end

        #check if neighbors overlap... make another graph to do this LOL
        g = Graph(length(cc_new))
        for i=1:length(cc_new)
            for j=(i+1):length(cc_new)
                if length(intersect(cc_new[i], cc_new[j])) > 0
                    add_edge!(g, i, j)
                end
            end
        end

        overlaps = connected_components(g) #for every entry [i,j,k,...] they are one component
        #println(overlaps)
        to_return = Array[] #the output components
        for i=1:length(overlaps)
            shared = overlaps[i]
            combined_comp = mapreduce((x) -> cc_new[x], union, shared)
            #combined_comp = union(map((x) -> cc_new[x], shared))
            push!(to_return, sort(combined_comp))
        end

        #if cc != to_return
        println(dis)
        println("old: " * string(cc))
        println("new: " * string(to_return))
        #end
    end
end

#given vertex v, component comp, n
#find all vertices w that are dist <=n away from v in graph
#and are in comp

#super inefficient atm... will deal with LATER.
function get_n_fold_neighbors(v, comp, global_graph, n)
    vert = Int[]
    push!(vert, v)
    for i=1:n
        for j=1:length(vert)
            vert = union(vert, intersect(neighbors(global_graph, vert[j]), comp))
        end
    end
    return intersect(vert, comp)
end

#given results array, remove 100 entries
function remove_100s(res)
    c = Any[]
    to_return = copy(res)
    cols = size(res,2)
    rows = size(res,1)
    for j=1:cols
        if sum(res[:,j])==100*rows
            push!(c,j)
        end
    end
    wanted_cols = filter(x -> !(x in c), 1:cols)
    return res[:, wanted_cols]
end

################################################################################################################
# HYPERGRAPHS
################################################################################################################

function parse_gene_families(gene_to_num, fam_file, A)
    f = open(fam_file)
    to_return = zeros(length(keys(gene_to_num)), 0)
    while !eof(f)
        num = int(readline(f))
        row = zeros(length(keys(gene_to_num)), 1)
        for i=1:num
            genes = split(readline(f), ",")
            for j=1:length(genes)
                g = genes[j]
                if g in keys(gene_to_num)
                    g_num = gene_to_num[g]
                    row[g_num] = 1
                    break
                end
            end
        end
        #println(sum(row))
        if sum(row) > 2
            to_return = [to_return row]
        end
        if sum(row) == 2
            nonzero = find(row)
            g1 = nonzero[1]
            g2 = nonzero[2]
            if A[g1, g2] == 0
                to_return = [to_return row]
            end
        end
    end
    return to_return
end

# rows are genes, columns are hyperedges AND edges in A
function construct_hyper_edge_mat(g_fam, A)
    number_normal_edges = int(sum(A) / 2)
    number_genes, number_hyper_edges = size(g_fam)
    to_return = zeros(number_genes, number_hyper_edges + number_normal_edges)

    #add edges in A to to_return
    cur_col = 1
    for i=1:number_genes
        for j=(i+1):number_genes
            if A[i,j] == 1
                to_return[i, cur_col] = 1
                to_return[j, cur_col] = 1
                cur_col = cur_col + 1
            end
        end
    end

    #add g_fam to to_return
    to_return[:, cur_col:end] = g_fam
    return to_return
end

#constructs a matrix akin to -beta L
# i think i might need to transpose the result though...
# i should also probably make a function that just makes the transition matrix
function construct_hyper_log_dk(g_fam, A, beta)
    num_genes = size(A,1)
    to_return = zeros(num_genes, num_genes)
    normalized_g_fam = g_fam ./ sum(g_fam,1)
    for i=1:num_genes
        d = sum(A[i,:]) + sum(g_fam[i,:]) #total degree of gene i

        relevant_edges = normalized_g_fam[:, find(g_fam[i, :])]
        fam_weights = sum(relevant_edges, 2) / d

        edge_weights = A[:, i] / d

        tot_weights = (beta) * (fam_weights + edge_weights)
        tot_weights[i] = -1 * beta * d
        to_return[i, :] = tot_weights
    end
    return to_return
end

# (i,j) is prob of going from j to i
function construct_hyper_transition(g_fam, A)
    num_genes = size(A,1)
    to_return = zeros(num_genes, num_genes)
    for i=1:num_genes
        d = sum(A[i,:]) + sum(g_fam[i,:]) #total degree of gene i

        relevant_edges = g_fam[:, find(g_fam[i, :])]
        relevant_edges[i, :] = 0 #make sure i cant go to itself
        relevant_edges = relevant_edges ./ sum(relevant_edges, 1) #normalize edge weight
        fam_weights = sum(relevant_edges, 2) / d

        edge_weights = A[:, i] / d

        tot_weights = fam_weights + edge_weights
        to_return[i, :] = tot_weights
    end
    return to_return
end

function construct_hyper_rwr_mat(g_fam, A, r)
    W = construct_hyper_transition(g_fam, A)
    W = transpose(W)
    n=size(A, 1)

    PPR = r*inv(A_add_diag(-(1.0-r),W,1.0,ones(n)))
    return PPR
end

# returns a list of all hyperedge degrees for all genes in dis_dict
function get_hyper_degrees(g_fam, dis_dict, gene_to_number)
    to_return = Dict()
    for dis in keys(dis_dict)
        dis_degs = []
        for gene in keys(dis_dict[dis])
            if gene in keys(gene_to_number)
                g_num = gene_to_number[gene]
                d = sum(g_fam[g_num, :])
                push!(dis_degs, d)
            end
        end
        to_return[dis] = dis_degs
    end
    return to_return
end

# find avg degree among hyper edges for all genes in dis_dict
function get_avg_hyper_degrees(g_fam, dis_dict, gene_to_number)
    to_return = Dict()
    tot = 0
    c = 0
    for dis in keys(dis_dict)
        for gene in keys(dis_dict[dis])
            if gene in keys(gene_to_number)
                g_num = gene_to_number[gene]
                d = sum(g_fam[g_num, :])
                tot = tot + d
                c = c + 1
            end
        end
    end
    return tot/c
end

#returns geom mean degree among hyperedges for all genes in dis_dict
function get_avg_gmean_hyper_degrees(g_fam, dis_dict, gene_to_number)
    to_return = Dict()
    degs = []
    for dis in keys(dis_dict)
        for gene in keys(dis_dict[dis])
            if gene in keys(gene_to_number)
                g_num = gene_to_number[gene]
                d = sum(g_fam[g_num, :])
                push!(degs, d)
            end
        end
    end
    return gmean(degs)
end

#given gene g (represented as a number), returns the number of nodes
#that g was not connected to in A that it is now connected to
function find_all_new_edges(g, g_fam, A)
    old_connections = A[g, :]

    #new_edges is a matrix where rows are genes
    #columns are edges that contain g
    new_edges = g_fam[:, find(vec(g_fam[g, :]))]

    #column matrix where entry is 1 if gene i is in edge w g
    new_connections = sum(new_edges, 2)
    new_connections = new_connections .> 0

    #figure out which entries are in new_connections and not old_connections
    return sum(vec(new_connections) .> vec(old_connections))
end

# process reactomeFI network
function process_FI_cliques(edge_file, gene_to_num, A)
    #create 
    B=zeros(A)
    open(edge_file) do f
        for i in enumerate(eachline(f))
            if i[1]>1
                details = split(i[2], '\t')
                is_complex = contains(details[3], "complex")
                if is_complex
                    gene1=details[1]
                    gene2=details[2]
                    score = strip(details[5])
                    if parse(Float64, score) >= 0.75
                        B[gene_to_num[gene1], gene_to_num[gene2]] = 1
                        B[gene_to_num[gene2], gene_to_num[gene1]] = 1
                    end
                end
            end
        end
    end
    return B
end

#given adjacency matrix A, hyperedges H
function create_weighted_hyper_transition_mat(A, H)
    num_genes = size(A,1)
    to_return = zeros(num_genes, num_genes)
    for i=1:num_genes
        #println(i)
        relevant_edges = H[:, find(H[i, :])]
        relevant_edges[i, :] = 0

        probs = sum(relevant_edges,2) + A[:,i]
        probs = probs ./ sum(probs)
        #to_return[i, :] = probs'
        to_return[:, i] = probs #since walk matrix in make_rwr_mat is constructed this way
    end
    return to_return
end

function create_unweighted_hyper_transition_mat(A, H)
    num_genes = size(A,1)
    to_return = zeros(num_genes, num_genes)
    for i=1:num_genes
        d = sum(A[i,:]) + sum(H[i,:]) #total degree of gene i

        relevant_edges = H[:, find(H[i, :])]
        relevant_edges[i, :] = 0 #make sure i cant go to itself
        relevant_edges = relevant_edges ./ sum(relevant_edges, 1) #normalize edge weight
        fam_weights = sum(relevant_edges, 2) / d

        edge_weights = A[:, i] / d

        tot_weights = fam_weights + edge_weights
        #to_return[i, :] = tot_weights'
        to_return[:, i] = tot_weights #since walk matrix in make_rwr_mat is constructed this way
    end
    return to_return
end

function number_in_cliques(H, dis_dict, gene_to_num)
    to_return = Dict()
    for dis in keys(dis_dict)
        dis_num_list = zeros(1, length(keys(dis_dict[dis])))
        i=1
        for gene in keys(dis_dict[dis])
            if haskey(gene_to_num, gene)
                gene_num = gene_to_num[gene]
                num_cliques = sum(H[gene_num,:])
                dis_num_list[i] = num_cliques
                i=i+1
            end
        end
        to_return[dis] = dis_num_list[1:(i-1)]
    end
    return to_return
end

function size_of_cliques(H, dis_dict, gene_to_num)
    cliques = Set()
    for dis in keys(dis_dict)
        for gene in keys(dis_dict[dis])
            if haskey(gene_to_num, gene)
                gene_num = gene_to_num[gene]
                c = find(H[gene_num,:])
                cliques=union(cliques,c)
            end
        end
    end
    to_return=map(x -> sum(H[:,x]), cliques)
    println(to_return)
    return to_return
end

#get change in rank when using K1 vs K2. takes out all 100s (aka genes not in database)
function rank_change(res1, res2)
    to_return = Dict()
    for dis in keys(res1)
        dis_res1 = filter(x->x!=100, res1[dis])
        dis_res2 = filter(x->x!=100, res2[dis])

        to_return[dis] = dis_res2 - dis_res1
    end
    return to_return
end

function rank_change_vs_num_cliques(H, dis_dict, gene_to_num, K1, K2)
    res1 = dict_results(dis_dict, gene_to_num, Array[K1])
    res2 = dict_results(dis_dict, gene_to_num, Array[K2])
    r = rank_change(res1, res2)

    n = number_in_cliques(H, dis_dict, gene_to_num)

    rank_list = Int[]
    clique_list = Int[]

    for dis in keys(r)
        append!(rank_list, r[dis])
        append!(clique_list, vec(n[dis]))
    end

    return (rank_list, clique_list)
end

################################################################################################################
# IMPLEMENTING ECC-rc FROM "Clique Covering of Large Real-World Networks"
# FOR FINDING HYPEREDGES
################################################################################################################

# A is adjacency matrix
# implementation of Algorithm 1 from "Clique Covering of Large Real-World Networks"
# limit is to try and see if smaller hyperedges are better

#OOPS, turns out that, for say lim=5, all cliques have size 6 (or less)...
function ecc(A; lim=Inf)
    clique_list = Any[]
    B = copy(A) # B is uncovered entries
    while sum(sum(B)) > 0

        #println(sum(sum(B)))

        #println("")
        #println("new cycle")
        #println(B)

        # find non-zero entries of B, aka uncovered entries
        inds = findn(B)
        r = rand(1:length(inds[1]))
        #r=1 #to debug

        # pick random uncovered edge
        u = inds[1][r]
        v = inds[2][r]

        #println("edge: " * string(u) * ", " * string(v))
        #println(u)
        #println(v)

        #find clique containing u,v
        R=find_clique_of(u,v,B,A,lim)

        #println("clique: " * string(R))
        #println(R)

        #add R to clique_list
        push!(clique_list,R)

        #cover edges in R
        n=length(R)
        for i=1:n
            for j=i+1:n
                x=R[i]
                y=R[j]
                B[x,y]=0
                B[y,x]=0
            end
        end
    end
    return clique_list
end

# helper function to select clique containing u,v
function find_clique_of(u,v,B,A,lim)
    R=[u,v] #clique to return, as a list of nodes, e.g. [1,5,3,19,...]
    Nu=find(A[u,:])
    Nv=find(A[v,:])
    P=intersect(Nu,Nv) #nodes to choose from in building clique R
    #println("neighbors P: " * string(P))
    #println(P)

    #if no nodes to add, return edge u - v
    if length(P) == 0
        return R
    end

    #otherwise, continue on with algorithm
    z = extract_node(P,R,B)

    #println("extracted node: " * string(z))

    #make sure we can add to clique
    while (z > 0) & (length(R) <= lim)
        R=union(R,[z])
        P=intersect(P,find(A[z,:]))
        z=extract_node(P,R,B)
    end

    return R
end

function extract_node(P,R,B)
    if length(P) == 0
        return -1
    end
    P1 = map((z) -> length(intersect(R, find(B[z,:]))), P)
    #println("P1: " * string(P1))
    i = indmax(P1)
    #println("ind max: " * string(i))
    if P1[i] > 0
        return P[i]
    else
        return -1
    end
end

################################################################################################################
# MAIN
################################################################################################################

#using PyPlot
using LightGraphs

function main()

    #index_file = "networks/iref14_index_genes"
    #edge_file = "networks/iref14_edge_list"

    # index_file = "networks/inthint2_index_genes"
    # edge_file = "networks/inthint2_edge_list"

    index_file = "hypergraph_data/reactomeFI/reactome_index_gene.tsv"
    edge_file = "hypergraph_data/reactomeFI/reactome_edge_list.tsv"

    A = load_edge_file(edge_file)
    gene_to_number = Dict()
    number_to_gene = Dict()
    process_index_file_new(index_file, gene_to_number, number_to_gene)

    B=process_FI_cliques("hypergraph_data/reactomeFI/FIsInGene_031516_with_annotations.txt", gene_to_number, A)

    #println("pls")

    # open("protein_complexes.txt", "w") do f
    #     for i=1:size(B,1)
    #         for j=i+1:size(B,1)
    #             if B[i,j] > 0
    #                 write(f,"$i $j\n")
    #             end
    #         end
    #     end
    # end

    # return -1

    println("starting")

    g=Graph(B)
    #lims=[Inf]
    #lims=[1,2,3,4,5,6,7,8,9,10,15,20,Inf]
    #lims=[3,4,5,6,7,8,9,10,15,20]
    #lims=[2]
    #lims=[1] # should be same as simple graph ...
    # for k=1:length(lims)

    # println()
    # println("STARTING TRIAL " * string(k))
    # println("finding ECC")
    #mc = maximal_cliques(g)
    #mc = ecc(B)

    # if lims[k]==1
    #     #not waiting for this case
    #     num_edges = trunc(Int,sum(B)/2)
    #     C=zeros(size(A,1),num_edges)
    #     counter=1
    #     for i=1:size(B,1)
    #         for j=i+1:size(B,1)
    #             if B[i,j] > 0
    #                 C[i,counter]=1
    #                 C[j,counter]=1
    #                 counter=counter+1
    #             end
    #         end
    #     end
    # else
    #     mc = ecc(B;lim=lims[k])
    #     #println("done ECC")
    #     mc = filter(x -> length(x) > 1, mc)

    #     C=zeros(size(A,1),length(mc))
    #     for i=1:length(mc)
    #         C[mc[i],i]=1
    #     end
    # end

    # mc = filter(x -> length(x) > 1, mc)

    # C=zeros(size(A,1),length(mc))
    # for i=1:length(mc)
    #     C[mc[i],i]=1
    # end

    #C2 = JLD.load("ECC_alessio9600_hyper.jld", "C")

    # D = create_weighted_hyper_transition_mat(A-B, C)

    #PPR1 = make_rwr_mat(A, 0.4)
    # PPR1=JLD.load("reactome_edge_list_rwr_mat_0.4.jld", "M")

    # println("done PPR1")

    #PPR2 = make_rwr_mat_pmf(D, 0.4)
    #PPR2=JLD.load("reactome_edge_list_hyper_weighted_rwr_mat_0.4.jld", "M")

    #println("done PPR2")

    #PPR3 = JLD.load("reactome_edge_list_ECC_alessio9600_hyper_weighted_rwr_mat_0.4.jld", "M")

    #println("done PPR3")

    m_dict = read_candidates("disease_gene_data/monogenic_data.txt", gene_to_number)
    p_dict = read_candidates("disease_gene_data/polygenic_data.txt", gene_to_number)
    c_dict = read_candidates("disease_gene_data/cancer_data.txt", gene_to_number)

    # m_res = dict_results(m_dict, gene_to_number, Array[PPR1, PPR3])

    # m = remove_candidates(m_dict, gene_to_number)

    # for dis in keys(m)
    #     genes = m[dis]
    #     hypergraph_degs = map((x) -> sum(C[gene_to_number[x],:]), genes)
    #     hypergraph_degs = trunc(Int,hypergraph_degs)
    #     hypergraph_degs = hypergraph_degs'
        
    #     simple_degs = map((x) -> sum(A[gene_to_number[x],:]), genes)
    #     simple_degs = trunc(Int,simple_degs)'

    #     E=A-B
    #     simple_degs_without_hyperedges = map((x) -> sum(E[gene_to_number[x],:]), genes)
    #     simple_degs_without_hyperedges = trunc(Int,simple_degs_without_hyperedges)'

    #     true_hyp_deg = hypergraph_degs + simple_degs_without_hyperedges

    #     m_res[dis] = [remove_100s(m_res[dis]); simple_degs; true_hyp_deg]
    # end

    # println(m_res)

    # return -1

    #JLD.save("ECC_alessio_lim_" * string(lims[k]) * "_hyper.jld", "C", C)
    #JLD.save("reactome_edge_list_max_cliques.jld", "C", C)
    #JLD.save("reactome_edge_list_ECC_alessio.jld", "C", C)

    C=JLD.load("ECC_alessio9600_hyper.jld", "C")
    JLD.save("reactome_edge_list_ECC_alessio.jld", "C", C)

    println("done c")

    # clique_nums = sum(C,2)
    # top_genes=sortperm(vec(clique_nums),rev=true)
    # for i=1:40
    #     g = number_to_gene[top_genes[i]]
    #     num = clique_nums[top_genes[i]]
    #     println(g)
    #     println(num)
    # end

    # return -1

    ## testing number of genes in different max cliques
    ## IMPORTANT: numbers from thesis filter out zeros from list

    #max cliques

    # println("monogenic")
    # println(gmean_dict_filter_0(number_in_cliques(C, m_dict, gene_to_number)))
    # println("polygenic")
    # println(gmean_dict_filter_0(number_in_cliques(C, p_dict, gene_to_number)))
    # println("cancer")
    # println(gmean_dict_filter_0(number_in_cliques(C, c_dict, gene_to_number)))

    # avg number of hyperedges 
    # monogenic: 13.794
    # polygenic: 5.922
    # cancer: 18.454

    #alessio

    # println("monogenic")
    # println(gmean_dict_filter_0(number_in_cliques(C2, m_dict, gene_to_number)))
    # println("polygenic")
    # println(gmean_dict_filter_0(number_in_cliques(C2, p_dict, gene_to_number)))
    # println("cancer")
    # println(gmean_dict_filter_0(number_in_cliques(C2, c_dict, gene_to_number)))

    # avg number of hyperedges 
    # monogenic: 3.663
    # polygenic: 3.605
    # cancer: 7.830

    ## size of cliques

    # max cliques

    # println("monogenic")
    # println(gmean_filter_0(size_of_cliques(C, m_dict, gene_to_number)))
    # println("polygenic")
    # println(gmean_filter_0(size_of_cliques(C, p_dict, gene_to_number)))
    # println("cancer")
    # println(gmean_filter_0(size_of_cliques(C, c_dict, gene_to_number)))

    # monogenic
    # 16.290463746450975
    # polygenic
    # 8.03129601688194
    # cancer
    # 9.16935539783473

    #alessio

    # println("monogenic")
    # println(gmean_filter_0(size_of_cliques(C2, m_dict, gene_to_number)))
    # println("polygenic")
    # println(gmean_filter_0(size_of_cliques(C2, p_dict, gene_to_number)))
    # println("cancer")
    # println(gmean_filter_0(size_of_cliques(C2, c_dict, gene_to_number)))

    # monogenic
    # 4.2463086861978265
    # polygenic
    # 3.680332856653923
    # cancer
    # 4.122078329037847

    # doesnt say anything


    ## creating different hypergraph rwr matrices

    println("right before")
    D1=create_weighted_hyper_transition_mat(A-B, C)
    #JLD.save("reactome_edge_list_ECC_alessio_lim" * string(lims[k]) * "_hyper_weighted_trans_mat.jld", "P", D1)
    #JLD.save("reactome_edge_list_max_cliques_hyper_weighted_trans_mat.jld", "P", D1)
    JLD.save("reactome_edge_list_ECC_alessio_hyper_weighted_trans_mat.jld", "P", D1)
    println("done D1")

    D2=create_unweighted_hyper_transition_mat(A-B, C)
    #JLD.save("reactome_edge_list_ECC_alessio_lim" * string(lims[k]) * "_hyper_unweighted_trans_mat.jld", "P", D2)
    # JLD.save("reactome_edge_list_max_cliques_hyper_unweighted_trans_mat.jld", "P", D2)
    JLD.save("reactome_edge_list_ECC_alessio_hyper_unweighted_trans_mat.jld", "P", D2)
    println("done D2")

    # D3=create_weighted_hyper_transition_mat(A, C)
    # #JLD.save("reactome_edge_list_ECC_alessio_lim" * string(lims[k]) * "_alt_hyper_weighted_trans_mat.jld", "P", D3)
    # JLD.save("reactome_edge_list_max_cliques_alt_hyper_weighted_trans_mat.jld", "P", D3)
    # println("done D3")

    # D4=create_unweighted_hyper_transition_mat(A, C)
    # #JLD.save("reactome_edge_list_ECC_alessio_lim" * string(lims[k]) * "_alt_hyper_unweighted_trans_mat.jld", "P", D4)
    # JLD.save("reactome_edge_list_max_cliques_alt_hyper_unweighted_trans_mat.jld", "P", D4)
    # println("done D4")

    # return -1

    #D1=JLD.load("reactome_edge_list_ECC_alessio_hyper_weighted_trans_mat.jld", "P")
    #D2=JLD.load("reactome_edge_list_ECC_alessio_hyper_unweighted_trans_mat.jld", "P")
    #D3=JLD.load("reactome_edge_list_ECC_alessio_alt_hyper_weighted_trans_mat.jld", "P")
    #D4=JLD.load("reactome_edge_list_ECC_alessio_alt_hyper_unweighted_trans_mat.jld", "P")

    #r_list = [0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.6]
    #r_list = [0.25, 0.3, 0.35, 0.4, 0.45, 0.5]
    r_list = [0.4, 0.45, 0.5, 0.55, 0.6]
    #r_list = [0.4]
    println("starting rwr")
    for r in r_list

        m_dict = read_candidates("disease_gene_data/monogenic_data.txt", gene_to_number)
        p_dict = read_candidates("disease_gene_data/polygenic_data.txt", gene_to_number)
        c_dict = read_candidates("disease_gene_data/cancer_data.txt", gene_to_number)

        PPR1 = make_rwr_mat_pmf(D1, r)
        #JLD.save("reactome_edge_list_ECC_alessio_lim" * string(lims[k]) * "_hyper_weighted_rwr_mat_" * string(r) * ".jld", "M", PPR1)
        JLD.save("reactome_edge_list_ECC_alessio_hyper_weighted_rwr_mat_" * string(r) * ".jld", "M", PPR1)

        PPR2 = make_rwr_mat_pmf(D2, r)
        #JLD.save("reactome_edge_list_ECC_alessio_lim" * string(lims[k]) * "_hyper_unweighted_rwr_mat_" * string(r) * ".jld", "M", PPR2)
        #JLD.save("reactome_edge_list_max_cliques_hyper_unweighted_rwr_mat_" * string(r) * ".jld", "M", PPR2)
        JLD.save("reactome_edge_list_ECC_alessio_hyper_unweighted_rwr_mat_" * string(r) * ".jld", "M", PPR2)

        #PPR3 = make_rwr_mat_pmf(D3, r)
        #JLD.save("reactome_edge_list_ECC_alessio_lim" * string(lims[k]) * "_alt_hyper_weighted_rwr_mat_" * string(r) * ".jld", "M", PPR3)
        #JLD.save("reactome_edge_list_max_cliques_alt_hyper_weighted_rwr_mat_" * string(r) * ".jld", "M", PPR3)

        #PPR4 = make_rwr_mat_pmf(D4, r)
        #JLD.save("reactome_edge_list_ECC_alessio_lim" * string(lims[k]) * "_alt_hyper_unweighted_rwr_mat_" * string(r) * ".jld", "M", PPR4)
        #JLD.save("reactome_edge_list_max_cliques_alt_hyper_unweighted_rwr_mat_" * string(r) * ".jld", "M", PPR4)

        println("NEW BATCH")
        println(r)
        # m_res = dict_results(m_dict, gene_to_number, Array[PPR1, PPR2, PPR3, PPR4])
        # p_res = dict_results(p_dict, gene_to_number, Array[PPR1, PPR2, PPR3, PPR4])
        # c_res = dict_results(c_dict, gene_to_number, Array[PPR1, PPR2, PPR3, PPR4])

        m_res = dict_results(m_dict, gene_to_number, Array[PPR1, PPR2])
        p_res = dict_results(p_dict, gene_to_number, Array[PPR1, PPR2])
        c_res = dict_results(c_dict, gene_to_number, Array[PPR1, PPR2])

        # println("MONOGENIC")
        # println(m_res)
        # println("POLYGENIC")
        # println(p_res)
        # println("CANCER")
        # println(c_res)

        println("MONOGENIC")
        println(get_average(m_res, false, false))
        println("POLYGENIC")
        println(get_average(p_res, false, false))
        println("CANCER")
        println(get_average(c_res, false, false))
    end

    #end
    return -1

    # println("bouta find rwr mat")
    # PPR = make_rwr_mat_pmf(D, 0.4)
    # JLD.save("reactome_edge_list_hyper_unweighted_rwr_mat_0.4.jld", "M", PPR)


    #PPR = make_rwr_mat(A, 0.4)
    #JLD.save("reactome_edge_list_rwr_mat_0.4.jld", "M", PPR)
    #PPR = JLD.load("reactome_edge_list_rwr_mat_0.4.jld", "M")
    #newPPR1 = JLD.load("reactome_edge_list_hyper_weighted_rwr_mat_0.4.jld", "M")
    #newPPR2 = JLD.load("reactome_edge_list_hyper_unweighted_rwr_mat_0.4.jld", "M")
    m_dict = read_candidates("disease_gene_data/monogenic_data.txt", gene_to_number)
    p_dict = read_candidates("disease_gene_data/polygenic_data.txt", gene_to_number)
    c_dict = read_candidates("disease_gene_data/cancer_data.txt", gene_to_number)

    m = remove_candidates(m_dict, gene_to_number)
    p = remove_candidates(p_dict, gene_to_number)
    c = remove_candidates(c_dict, gene_to_number)

    degs = sum(A,1)

    println("monogenic: " * string(avg_deg(degs, m, gene_to_number)))
    println("polygenic: " * string(avg_deg(degs, p, gene_to_number)))
    println("cancer: " * string(avg_deg(degs, c, gene_to_number)))

    #println(rank_change_vs_num_cliques(C, m_dict, gene_to_number, PPR, newPPR1))
    return -1

    # TO DO: make lists of change in rank vs number of max cliques for m_dict, p_dict, c_dict
    # then plot

    #PPR = make_rwr_mat(A, 0.4)
    #JLD.save("reactome_edge_list_rwr_mat_0.4.jld", "M", PPR)
    #PPR=JLD.load("reactome_edge_list_rwr_mat_0.4.jld", "M")

    # rows are genes, columns are hyperedges
    # g_fam = 5
    # try
    #     g_fam = JLD.load("gene_families_mat.jld", "H")
    # catch
    #     println("pls")
    #     g_fam = parse_gene_families(gene_to_number, "gene_families.txt", A)
    #     JLD.save("gene_families_mat.jld", "H", g_fam)
    # end

    # K=5
    # try 
    #     K = JLD.load("iref14_edge_list_dk_mat_1.0e-5.jld", "K")
    # catch
    #     K = make_dk_mat(A, .00001)
    #     JLD.save("iref14_edge_list_dk_mat_1.0e-5.jld", "K", K)
    # end

    #PPR = JLD.load("iref14_edge_list_rwr_mat_0.30.jld", "M")
    #K = JLD.load("iref14_edge_list_dk_mat_0.0015.jld", "K")


    # K=5
    # try 
    #     K = JLD.load("iref14_edge_list_dk_mat_0.1.jld", "K")
    # catch
    #     println("no")
    #     K = make_dk_mat(A, .1)
    #     JLD.save("iref14_edge_list_dk_mat_0.1.jld", "K", K)
    # end

    # PPR = 5
    # try
    #     PPR = JLD.load("iref14_edge_list_rwr_mat_0.35.jld", "M")
    # catch
    #     println("aww")
    #     PPR = make_rwr_mat(A, 0.35)
    #     JLD.save("iref14_edge_list_rwr_mat_0.35.jld", "M", PPR)
    #     println("done!")
    # end

    # get distance matrix for A
    # g = Graph(A)
    # f = floyd_warshall_shortest_paths(g)
    # d = f.dists
    # JLD.save("dist_mat.jld", "D", d)

    # for now, i dont want to JLD.load these, so will ignore
    #PPR = JLD.load("iref14_edge_list_rwr_mat_0.35.jld", "M")
    #newPPR = JLD.load("iref14_edge_list_hyper_rwr_mat_0.4.jld", "M")

    # columns are edges, rows are genes
    # gen_g_fam = construct_hyper_edge_mat(g_fam, A) # has all edges
    # JLD.save("iref14_edge_list_hyper_edges.jld", "gen_g_fam", gen_g_fam)
    # dont JLD.load this shit, its super HUGE
    # DONT DO IT
    # DONT
    # DONTTTTTTT
    # gen_g_fam = JLD.load("iref14_edge_list_hyper_edges.jld", "gen_g_fam")

    m_dict = read_candidates("disease_gene_data/monogenic_data.txt", gene_to_number)
    p_dict = read_candidates("disease_gene_data/polygenic_data.txt", gene_to_number)
    c_dict = read_candidates("disease_gene_data/cancer_data.txt", gene_to_number)

    # m_res = dict_results(m_dict, gene_to_number, Array[PPR])
    # p_res = dict_results(p_dict, gene_to_number, Array[PPR])
    # c_res = dict_results(c_dict, gene_to_number, Array[PPR])

    # new_m_res = dict_results(m_dict, gene_to_number, Array[newPPR])
    # new_p_res = dict_results(p_dict, gene_to_number, Array[newPPR])
    # new_c_res = dict_results(c_dict, gene_to_number, Array[newPPR])

    m_res = dict_results(m_dict, gene_to_number, Array[PPR, newPPR1, newPPR2])
    p_res = dict_results(p_dict, gene_to_number, Array[PPR, newPPR1, newPPR2])
    c_res = dict_results(c_dict, gene_to_number, Array[PPR, newPPR1, newPPR2])

    println("MONOGENIC")
    println(m_res)
    #println(new_m_res)
    println("POLYGENIC")
    println(p_res)
    #println(new_p_res)
    println("CANCER")
    println(c_res)
    #println(new_c_res)

    println("MONOGENIC")
    println(get_average(m_res, false, false))
    #println(get_average(new_m_res, false, false))
    println("POLYGENIC")
    println(get_average(p_res, false, false))
    #println(get_average(new_p_res, false, false))
    println("CANCER")
    println(get_average(c_res, false, false))
    #println(get_average(new_c_res, false, false))
    return -1

    # tot_dict = merge(remove_candidates(m_dict, gene_to_number), remove_candidates(p_dict, gene_to_number))
    # tot_dict = merge(tot_dict, remove_candidates(c_dict, gene_to_number))
    # to_return = Int[]
    # println(tot_dict)
    # for dis in keys(tot_dict)
    #     push!(to_return, length(tot_dict[dis]))
    # end
    # println(to_return)
    # return -1


    # println(get_avg_gmean_hyper_degrees(gen_g_fam, m_dict, gene_to_number))
    # println(get_avg_gmean_hyper_degrees(gen_g_fam, p_dict, gene_to_number))
    # println(get_avg_gmean_hyper_degrees(gen_g_fam, c_dict, gene_to_number))

    # m_res = dict_results(m_dict, gene_to_number, Array[PPR])
    # m_new_res = dict_results(m_dict, gene_to_number, Array[newPPR])
    # p_res = dict_results(p_dict, gene_to_number, Array[PPR])
    # p_new_res = dict_results(p_dict, gene_to_number, Array[newPPR])
    # c_res = dict_results(c_dict, gene_to_number, Array[PPR])
    # c_new_res = dict_results(c_dict, gene_to_number, Array[newPPR])
    # res = Dict()
    # res["m"] = m_res
    # res["p"] = p_res
    # res["c"] = c_res
    # new_res = Dict()
    # new_res["m"] = m_new_res
    # new_res["p"] = p_new_res
    # new_res["c"] = c_new_res
    # JLD.save("iref14_edge_list_rwr_hyper_compare_0.35_0.4_results.jld", "res", res, "new_res", new_res)

    d = "p"
    cand_dict = p_dict
    full_res = JLD.load("iref14_edge_list_rwr_hyper_compare_0.35_0.4_results.jld", "res")
    full_new_res = JLD.load("iref14_edge_list_rwr_hyper_compare_0.35_0.4_results.jld", "new_res")

    cand_dicts = [m_dict, p_dict, c_dict]
    r = ["m", "p", "c"]
    l = ["monogenic", "polygenic", "cancer"]

    for j=1:3
        dis_gene_dict = remove_candidates(cand_dicts[j], gene_to_number)
        res = full_res[r[j]]
        new_res = full_new_res[r[j]]

        # up_deg_change = Any[]
        # down_deg_change = Any[]
        # other_deg_change = Any[]
        # u_tot = 0 #how many went up in rank
        # d_tot = 0
        # o_tot = 0
        deg_change_arr = Any[]
        rank_change_arr = Any[] #old_rank - new_rank, so if positive, rank went up
        for dis in keys(res)
            new = new_res[dis]
            old = res[dis]

            genez = dis_gene_dict[dis]
            for i=1:length(genez)
                g = genez[i]
                if g in keys(gene_to_number)
                    g_num = gene_to_number[g]
                    deg_change = sum(g_fam[g_num, :])
                    push!(deg_change_arr, deg_change)
                    push!(rank_change_arr, old[i]-new[i])

                    # if old[i] > new[i]
                    #     push!(up_deg_change, deg_change)
                    # elseif old[i] < new[i]
                    #     push!(down_deg_change, deg_change)
                    # else
                    #     push!(other_deg_change, deg_change)
                    # end
                end
            end
        end
    println("# "*l[j])
    println(deg_change_arr)
    println(rank_change_arr)
    println("")
    # println("# mean(up_deg_change)=" * string(mean(up_deg_change)))
    # println("# mean(down_deg_change)=" * string(mean(down_deg_change)))
    # println("# mean(other_deg_change)=" * string(mean(other_deg_change)))
    # tot = u_tot + d_tot + o_tot
    # println("# up_tot/tot=" * string(u_tot/tot))
    # println("# down_tot/tot=" * string(d_tot/tot))
    # println("")
    end
    
    # monogenic
    # mean(up_deg_change)=0.955
    # mean(down_deg_change)=0.531
    # mean(other_deg_change)=0.983
    # up_tot/tot=0.172
    # down_tot/tot=0.157

    # polygenic
    # mean(up_deg_change)=1.2
    # mean(down_deg_change)=0.76
    # mean(other_deg_change)=0.936
    # up_tot/tot=0.258
    # down_tot/tot=0.258

    # cancer
    # mean(up_deg_change)=1.6
    # mean(down_deg_change)=0.556
    # mean(other_deg_change)=1.068
    # up_tot/tot=0.099
    # down_tot/tot=0.178

    # so same trend in all three 
    # if rank went up, then degree went up higher than avg
    # if rank went down, then degree went up lower than avg

    # in monogenic, more went up in rank than went down in rank
    # in polygenic, same went up in rank as went down in rank
    # in cancer, much more went down in rank then went up in rank

    # also, plotting deg change vs rank change did shit

    #lets measure how avg number of new edges

    cand_dicts = [m_dict, p_dict, c_dict]
    r = ["m", "p", "c"]
    l = ["monogenic", "polygenic", "cancer"]

    for j=reverse(1:3)
        dis_gene_dict = remove_candidates(cand_dicts[j], gene_to_number)
        res = full_res[r[j]]
        new_res = full_new_res[r[j]]

        up_conn_change = Any[]
        down_conn_change = Any[]
        other_conn_change = Any[]
        for dis in keys(res)
            new = new_res[dis]
            old = res[dis]

            genez = dis_gene_dict[dis]
            for i=1:length(genez)
                g = genez[i]
                if g in keys(gene_to_number)
                    g_num = gene_to_number[g]
                    conn_change = find_all_new_edges(g_num, g_fam, A)
                    #rank_increase = old[i] - new[i]
                    if old[i] > new[i]
                        push!(up_conn_change, conn_change)
                    elseif old[i] < new[i]
                        push!(down_conn_change, conn_change)
                    else
                        push!(other_conn_change, conn_change)
                    end
                end
            end
        end
        println(l[j])
        println(up_conn_change)
        println(down_conn_change)
        println(other_conn_change)
        println("")
    end

# want to look at some of the subgraphs of kernel graph (complete weighted graph)
#PPR = JLD.load("iref14_edge_list_rwr_mat_0.30.jld", "M")
dis_gene_dict = remove_candidates(c_dict, gene_to_number)
x=0
for dis in keys(c_dict)
    if x>0
        break
    end
    println(dis)
    genes = dis_gene_dict[dis]
    genes = map(x -> gene_to_number[x], genes)

    println(PPR[genes,genes])
    x=x+1
end

# up_conn_change, down_conn_change, other_conn_change
# e.g. up_conn_change is list of, for each gene whose rank went up, 
# how many new genes they became connected to

#...also these are all wrong lol
#cancer
# a={369,98,183,548,202,173,122,42,249,14}
# b={544,0,181,301,0,249,22,543,0,0,0,0,0,0,0,0,545,0}
# c={353,30,0,14,355,0,0,0,4,5,235,60,172,22,471,19,0,1,18,0,1,30,36,0,14,64,2,0,0,9,202,1,14,202,0,29,23,22,78,190,6,15,391,14,125,34,0,0,14,15,0,41,0,38,29,0,1,22,0,311,47,3,0,174,0,0,1,19,1,0,5,2,2}

# #polygenic
# x={399,35,136,3,213,0,0,412,0,7,0,5,176,0,168,9,0,0,126,156,11,52,19,28,11}
# y={6,0,2,0,2,0,11,545,0,12,0,15,174,97,249,16,443,0,171,2,16,4,0,545,0}
# z={10,0,158,0,201,0,0,0,2,360,249,249,364,0,0,47,2,41,0,0,249,15,0,170,0,0,412,412,213,380,0,10,172,7,0,443,56,0,55,56,28,4,249,0,97,41,15}

# # monogenic
# l={0,29,11,22,22,61,204,15,4,8,167,0,5,0,25,8,12,2,34,0,30,5,26,11,0,0,0,14,166,41,11,248,25,10,62,6,0,15,0,5,2,0,18,13,11,0,0,0,29,95,173,6,0,239,0,0,7,145,14,13,12,29,13,13,11,13,0,4,4,10,17,0,5,28,0,544,0,12,77,13,33,0,13,13,7,22,11,9,8}
# m={0,8,20,548,0,545,33,0,0,171,208,30,546,0,0,0,0,3,0,0,0,0,0,18,0,236,0,6,29,9,0,0,139,0,0,8,0,0,0,4,0,0,132,32,0,22,17,0,0,0,0,0,230,0,0,0,42,7,20,172,166,0,0,4,8,0,0,0,0,20,0,5,0,0,44,7,0,11,0,30,0}
# n={16,0,15,0,0,5,0,167,0,5,0,47,173,173,0,20,0,0,132,18,0,27,114,176,0,0,15,249,0,0,94,8,29,173,0,3,0,13,4,206,293,2,5,167,0,0,45,0,0,543,542,7,5,246,6,40,8,38,0,17,29,32,27,29,239,5,0,3,0,3,9,158,15,14,13,16,14,11,9,13,22,12,90,0,0,0,0,37,3,0,0,0,0,0,2,0,0,0,2,0,25,26,9,5,378,22,461,6,9,8,350,144,9,4,7,33,176,19,0,0,181,7,0,12,11,248,122,170,0,0,293,0,0,0,0,0,4,39,18,10,0,26,12,13,12,10,0,110,20,6,11,0,9,13,0,10,10,249,18,18,0,61,0,22,20,22,4,10,0,0,0,63,181,12,5,14,0,176,41,5,16,0,0,4,26,132,0,12,0,13,41,0,609,168,4,4,0,0,230,96,0,0,181,359,180,14,14,33,7,32,468,12,0,11,69,0,0,4,7,6,6,235,7,47,173,525,10,359,8,2,40,8,138,465,0,0,0,11,0,184,31,2,13,5,3,237,6,50,0,1,2,1,0,0,0,0,6,50,100,236,0,0,12,29,357,0,15,4,5,4,0,12,13,12,13,29,6,5,5,0,7,170,0,5,206,293,0,44,3,0,47,5,6,237,0,100,50,50,239,0,0,0,236,8,15,28,0,181,0,33,0,12,13,0,2,29,4,26,5,0,181,357,15,478,72,131,187,196,6,0,6,22,78,18,19,0,0,25,0,0,16,0,0,21,21,0,0}

# so in all three, genes that went up had a much higher change in connectivity

######

# corresponding lists, for each of the lists above, of old_rank - new_rank

# cancer
# {14,4,1,6,15,1,5,2,2,1}
# {-2,-5,-3,-4,-2,-1,-1,-3,-1,-2,-1,-5,-5,-2,-9,-3,-2,-2}
# {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}

# polygenic
# {6,6,3,20,1,2,1,2,1,26,3,3,6,16,2,6,1,2,50,20,21,1,1,2,2}
# {-2,-1,-1,-2,-6,-5,-3,-3,-5,-3,-3,-4,-1,-6,-3,-6,-1,-5,-1,-1,-4,-2,-7,-1,-1}
# {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}

# monogenic
# {1,15,28,2,11,8,37,3,4,2,32,9,4,1,1,16,79,1,1,1,2,14,1,1,2,1,1,2,24,13,1,1,24,9,3,2,11,1,1,2,2,34,21,1,1,2,2,1,1,10,45,1,1,1,1,47,30,2,2,23,3,48,18,5,5,1,1,1,1,36,92,2,12,3,3,1,1,1,17,27,3,43,22,6,2,5,1,4,7}
# {-4,-6,-1,-2,-1,-2,-1,-1,-2,-3,-1,-1,-1,-9,-3,-3,-1,-2,-2,-1,-4,-1,-2,-2,-3,-2,-2,-3,-1,-1,-1,-3,-1,-2,-1,-2,-5,-1,-4,-1,-2,-4,-1,-2,-2,-3,-1,-2,-3,-1,-2,-3,-1,-6,-1,-1,-2,-3,-1,-1,-1,-2,-8,-1,-3,-2,-3,-2,-1,-2,-6,-5,-1,-5,-4,-1,-2,-1,-1,-8,-5}
# {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}


    return -1

    # println("MONOGENIC")
    # println(get_average(m_res, false, false))
    # println("POLYGENIC")
    # println(get_average(p_res, false, false))
    # println("CANCER")
    # println(get_average(c_res, false, false))

    # b = [0.0001, 0.001, 0.01, 0.05, 0.1, 0.5, 1]
    # for beta in b
    #     println(beta)
    #     # newL = construct_hyper_log_dk(g_fam, A, beta)
    #     # newK = expm(newL)
    #     newK = JLD.load("iref14_edge_list_hyper_dk_" * string(beta) * ".jld", "K")

    #     m_res = dict_results(m_dict, gene_to_number, Array[newK])
    #     p_res = dict_results(p_dict, gene_to_number, Array[newK])
    #     c_res = dict_results(c_dict, gene_to_number, Array[newK])
    #     # println("MONOGENIC")
    #     # println(m_res)
    #     # println("POLYGENIC")
    #     # println(p_res)
    #     # println("CANCER")
    #     # println(c_res)

    #     println("MONOGENIC")
    #     println(get_average(m_res, false, false))
    #     println("POLYGENIC")
    #     println(get_average(p_res, false, false))
    #     println("CANCER")
    #     println(get_average(c_res, false, false))
    # end


    # rs = [0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50]
    # for r in rs
    #    println(r)
    #    # PPR = construct_hyper_rwr_mat(g_fam, A, r)
    #    # JLD.save("iref14_edge_list_rwr_mat_" * string(r) * ".jld", "M", PPR)
    #    PPR = JLD.load("iref14_edge_list_hyper_rwr_mat_" * string(r) * ".jld", "M")

    #     m_res = dict_results(m_dict, gene_to_number, Array[PPR])
    #     p_res = dict_results(p_dict, gene_to_number, Array[PPR])
    #     c_res = dict_results(c_dict, gene_to_number, Array[PPR])
    #     # println("MONOGENIC")
    #     # println(m_res)
    #     # println("POLYGENIC")
    #     # println(p_res)
    #     # println("CANCER")
    #     # println(c_res)

    #     println("MONOGENIC")
    #     println(get_average(m_res, false, false))
    #     println("POLYGENIC")
    #     println(get_average(p_res, false, false))
    #     println("CANCER")
    #     println(get_average(c_res, false, false))

    # end

    # so it somehow does better on monogenic... but a lot worse on polygenic/cancer? the t threshold is lower too
    # probably since heat has more places to diffuse in the graph

    # TODO: 
    # investigate the following.
    # 1. what went right in monogenic? which genes did better? do they appear a lot in gene families?
    # 2. do the same for polygenic, cancer
    # 3. make matrices for RWR

    return -1
    # newK = JLD.load("iref14_edge_list_hyper_dk_0.3.jld", "K")

    # m_dict = read_candidates("disease_gene_data/monogenic_data.txt", gene_to_number)
    # p_dict = read_candidates("disease_gene_data/polygenic_data.txt", gene_to_number)
    # c_dict = read_candidates("disease_gene_data/cancer_data.txt", gene_to_number)

    # c=remove_small_lists(remove_candidates(c_dict, gene_to_number))
    # print(c)

    # m_res = dict_results(m_dict, gene_to_number, Array[K])
    # p_res = dict_results(p_dict, gene_to_number, Array[K])
    # c_res = dict_results(c_dict, gene_to_number, Array[K])
    # println("MONOGENIC")
    # println(m_res)
    # println("POLYGENIC")
    # println(p_res)
    # println("CANCER")
    # println(c_res)

    return -1


    # m=remove_small_lists(remove_candidates(m_dict, gene_to_number))
    # p=remove_small_lists(remove_candidates(p_dict, gene_to_number))
    # c=remove_small_lists(remove_candidates(c_dict, gene_to_number))

    # dict = c
    # res_dict = m_dict

    #lcc_init(dict, A, gene_to_number, res_dict, PPR)

    get_near_neighbors(dict, A, gene_to_number, 2)

    #weirdly enough, the below disease has a gene ranked 100 by DK and RWR
    #thats IS in gene_to_number 

    # dis = "Limb-Girdle Muscle Dystrophy"
    # genez = m[dis]

    # for i=1:length(genez)
    #     if ~haskey(gene_to_number, genez[i])
    #         println(genez[i])
    #     end
    # end

    #for plotting
    #plot_all_conn_comps(dict, A, gene_to_number, "monogenic")

    #test hypothesis that connected components have good rank
    #res=dict_results(res_dict, gene_to_number, Array[K])
    #all_rank_conn_comps(dict, A, gene_to_number, res)


    #diseases that look potentially clustered
    # dis_list = ["Xeroderma pigmentosum", "Chondrodysplasia punctata", "Limb-Girdle Muscle Dystrophy"]

    #dis = "Epidermolysis bullosa"
    #println(res[dis])
    #println(m[dis])

    # for i=1:3
    #     dis = dis_list[i]
    #     println(dis)
    #     println(res[dis])
    #     println(m[dis])
    #     println()
    # end

    # for dis in keys(dict)
    #     genez = dict[dis]
    #     println(dis)
    #     get_components(genez, A, gene_to_number)
    # end



    return -1

    dk_res = JLD.load("res.jld", "x") #2 elt tuple, each is dict mapping disease --> matrix of results for various t
    canc_res = JLD.load("canc_res.jld", "x")

    new_dk_res = JLD.load("new_dk_res.jld", "x")
    rwr_res = JLD.load("rwr_res.jld", "x")

    adj_dk_res = JLD.load("adjusted_dk_res.jld", "x") #using linspace(0.01, .99, 99)

    #println(combine_results(new_dk_res, logspace(-6, 0, 61)))
    #println(separate_results(rwr_res, linspace(0.01, .99, 99)))
    println(separate_results(adj_dk_res, linspace(0.01, .99, 99)))
    return -1

    #printing eps vs best t

    # println("monogenic")
    # println(compare_clust_rank_eps_dk_plot_lists(m_dict, gene_to_number, K, logspace(-0.3, -11, 1000), new_dk_res[1], logspace(-6, 0, 61)))

    # println("polygenic")
    # println(compare_clust_rank_eps_dk_plot_lists(p_dict, gene_to_number, K, logspace(-0.3, -11, 1000), new_dk_res[2], logspace(-6, 0, 61)))

    # println("cancer")
    # println(compare_clust_rank_eps_dk_plot_lists(c_dict, gene_to_number, K, logspace(-0.3, -11, 1000), new_dk_res[3], logspace(-6, 0, 61)))

    #rwr

    println("monogenic")
    println(compare_clust_rank_eps_dk_plot_lists(m_dict, gene_to_number, PPR, logspace(-0.3, -11, 1000), new_dk_res[1], logspace(-6, 0, 61)))

    println("polygenic")
    println(compare_clust_rank_eps_dk_plot_lists(p_dict, gene_to_number, PPR, logspace(-0.3, -11, 1000), new_dk_res[2], logspace(-6, 0, 61)))

    println("cancer")
    println(compare_clust_rank_eps_dk_plot_lists(c_dict, gene_to_number, PPR, logspace(-0.3, -11, 1000), new_dk_res[3], logspace(-6, 0, 61)))

    ######

    #printing deg vs best t

    # println("monogenic")
    # println(compare_deg_dk_t(m_dict, gene_to_number, A, new_dk_res[1], logspace(-6, 0, 61)))

    # println("polygenic")
    # println(compare_deg_dk_t(p_dict, gene_to_number, A, new_dk_res[2], logspace(-6, 0, 61)))

    # println("cancer")
    # println(compare_deg_dk_t(c_dict, gene_to_number, A, new_dk_res[3], logspace(-6, 0, 61)))

    #rwr

    # println("monogenic")
    # println(compare_deg_dk_t(m_dict, gene_to_number, A, rwr_res[1], linspace(0.01, .99, 99)))

    # println("polygenic")
    # println(compare_deg_dk_t(p_dict, gene_to_number, A, rwr_res[2], linspace(0.01, .99, 99)))

    # println("cancer")
    # println(compare_deg_dk_t(c_dict, gene_to_number, A, rwr_res[3], linspace(0.01, .99, 99)))

    ######

    #printing clust dens vs best t

    # println("monogenic")
    # println(compare_clust_dens_dk_t(m_dict, gene_to_number, A, new_dk_res[1], logspace(-6, 0, 61)))

    # println("polygenic")
    # println(compare_clust_dens_dk_t(p_dict, gene_to_number, A, new_dk_res[2], logspace(-6, 0, 61)))

    # println("cancer")
    # println(compare_clust_dens_dk_t(c_dict, gene_to_number, A, new_dk_res[3], logspace(-6, 0, 61)))

    ######

    #printing avg simil vs best t

    # println("monogenic")
    # println(compare_avg_simil_dk_t(m_dict, gene_to_number, PPR, new_dk_res[1], logspace(-6, 0, 61)))

    # println("polygenic")
    # println(compare_avg_simil_dk_t(p_dict, gene_to_number, PPR, new_dk_res[2], logspace(-6, 0, 61)))

    # println("cancer")
    # println(compare_avg_simil_dk_t(c_dict, gene_to_number, PPR, new_dk_res[3], logspace(-6, 0, 61)))

    ######

    #printing avg influence vs best t

    # println("monogenic")
    # println(compare_avg_infl_dk_t(m_dict, gene_to_number, K, dk_res[1]))

    # println("polygenic")
    # println(compare_avg_infl_dk_t(p_dict, gene_to_number, K, dk_res[2]))

    # println("cancer")
    # println(compare_avg_infl_dk_t(c_dict, gene_to_number, K, canc_res, logspace(-3, -0.3, 50)))

    return -1

    println("monogenic")
    println(compare_clust_rank_eps_plot_lists(m_dict, gene_to_number, K, logspace(-0.3, -11, 1000)))
    println()
    println("polygenic")
    println(compare_clust_rank_eps_plot_lists(p_dict, gene_to_number, K, logspace(-0.3, -11, 1000)))
    println("cancer")
    println(compare_clust_rank_eps_plot_lists(c_dict, gene_to_number, K, logspace(-0.3, -11, 1000)))
    println()
    return -1


    # for i in p
    #     if "KCNJ11" in i[2]
    #         println(i[1])
    #     end
    # end

    #println(p["Non-Insulin-Dependent Diabetes Mellitus"])

    #return -1

    ########################################################################

    #quick degree calculations

    #high degree genes that appear in multiple lists
    # TP53, 865 (appears in 5 lists, all cancer)
    # EGFR, 863 (appears in 2 lists, both cancer)
    # APP, 2009 (appears in 2 lists, 1 poly and 1 mono)

    # degs = sum(A,1)



    # println("monogenic: " * string(avg_deg(degs, m, gene_to_number)))
    # println("polygenic: " * string(avg_deg(degs, p, gene_to_number)))
    # println("cancer: " * string(avg_deg(degs, c, gene_to_number)))
    # return -1

    ########################################################################

    # calculating cluster statistics using GMEAN new

    # println(gmean_dict_new(cluster_density_dict(m, A, gene_to_number)))
    # println(mean(values(cluster_density_dict(m,A,gene_to_number))))
    # return -1

    #the dist we look at
    dist=create_size_dist(m,p,c)
    #println(dist)

    # getting random similarity and plotting the dist
    #r = random_simil_new(100000, PPR, gene_to_number, number_to_gene, dist)
    # println(r)
    # plot_random_simil_new(PPR, gene_to_number, number_to_gene, dist)


    # getting random edge density and plotting the dist
    # altho plotting is hard cause of 0s
    # r=random_cluster(100000, A, gene_to_number, number_to_gene, dist)
    # println(r)
    # plot_random_cluster(A, gene_to_number, number_to_gene, dist)

    # #testing my sample function
    # #weee they're close

    # dist_probs = make_prob_dist(dist)
    # sample_dist = Dict()
    # for i=1:10000
    #     samp = sample_from_dist(dist)
    #     if haskey(sample_dist, samp)
    #         sample_dist[samp] += 1
    #     else
    #         sample_dist[samp] = 1
    #     end
    # end

    # sample_dist_probs = make_prob_dist(sample_dist)

    # println(dist_probs)
    # println(sample_dist_probs)


    #using beta=0.0015 to plot
    dict_list = Dict[]
    push!(dict_list, m)
    push!(dict_list, p)
    push!(dict_list, c)

    plot_simil_cluster_mpl_new(dict_list, K, A, gene_to_number)

    #plot_simil_cluster_rand_new(K, A, gene_to_number, number_to_gene, dist)
    return -1

    # println(gmean_dict(avg_simil_dict_new(m, K, gene_to_number)))
    # println(gmean_dict(avg_simil_dict_new(p, K, gene_to_number)))
    # println(gmean_dict(avg_simil_dict_new(c, K, gene_to_number)))

    # println(gmean_dict(avg_simil_dict_new(m, PPR, gene_to_number)))
    # println(gmean_dict(avg_simil_dict_new(p, PPR, gene_to_number)))
    # println(gmean_dict(avg_simil_dict_new(c, PPR, gene_to_number)))


    #return -1

    ########################################################################

    #plotting avg_clust_num, tot_clust_num vs epsilon for dif diseases

    # println("monogenic")
    # println(compare_clust_rank_eps_plot_lists_cliques(m_dict, gene_to_number, PPR, logspace(-0.3, -11, 1000)))
    # println()
    # println("polygenic")
    # println(compare_clust_rank_eps_plot_lists_cliques(p_dict, gene_to_number, PPR, logspace(-0.3, -11, 1000)))
    # println("cancer")
    # println(compare_clust_rank_eps_plot_lists_cliques(c_dict, gene_to_number, PPR, logspace(-0.3, -11, 1000)))
    # println()

    # return -1

    #doing linkage stuff

    # println("monogenic")
    # println(compare_rank_linkage_plot_lists(m_dict, gene_to_number, K))
    # println()
    # println("polygenic")
    # println(compare_rank_linkage_plot_lists(p_dict, gene_to_number, K))
    # println("cancer")
    # println(compare_rank_linkage_plot_lists(c_dict, gene_to_number, K))
    # println()

    # return -1

    dic = p
    p_keys = collect(keys(dic))
    c=6

    for c=1:length(p_keys)
        println(p_keys[c])
        println(min_clique_linkage(map(x -> gene_to_number[x], dic[p_keys[c]]), PPR))
        println(min_linkage(map(x -> gene_to_number[x], dic[p_keys[c]]), PPR))
    end
    return -1

    # println(p_keys[c])
    # eps, eps_list, num_cluster_list, num_non_disease, avg_per_clust = grow_adjusted(map((x) -> gene_to_number[x], dic[p_keys[c]]), K)
    # #println(eps_list)
    # #println(num_cluster_list)
    # #println(num_non_disease)
    # println(num_cluster_list[1])
    # println(eps)
    # plot(eps_list, num_cluster_list)
    # #println(avg_per_clust)
    # #plot(eps_list, num_cluster_list)
    # xscale("log")
    # show()
    # return 5

    ########################################################################

    key = c
    dict = p

    dis = collect(keys(dict))
    dis = dis[key]

    genes = dict[dis]
    genes = map(x -> gene_to_number[x], genes)
    genes = sort(genes)

    new_K = K[genes,genes]



    # s = svdfact(new_K)
    # U = s[:U]
    # S = s[:S]
    # V = s[:V]

    # new_S = zeros(S)
    # for i=1:3
    #     new_S[i] = S[i]
    # end

    # new_K_2 = U * diagm(new_S) * transpose(V)



    v1_axis = zeros(length(genes))
    v2_axis = ones(length(genes))

    for i=1:length(genes)
        new_K[i,i] = 0
    end



    for i=1:length(genes)
        v1_axis[i] = sum(new_K * new_K[:,i])
    end 


    # v1 = new_K_2[:,1]
    # v2 = new_K_2[:,2]

    # println(v1/sum(v1))
    # println(v2/sum(v2))
    # return -1

    # v1_axis = FloatingPoint[]
    # v2_axis = FloatingPoint[]

    # for i=1:length(genes)
    #     push!(v1_axis, abs(dot(v1, new_K[:,i])/norm(v1)))
    #     push!(v2_axis, abs(dot(v2, new_K[:,i])/norm(v2)))
    # end

    plot(v1_axis, v2_axis, linestyle="None", marker="o")
    xscale("log")
    #yscale("log")
    #println(new_K_2)
    println(v1_axis)
    println(v2_axis)
    show()

    return -1

    ########################################################################

    #cc = compare_eps_rank(c_dict, gene_to_number, K, logspace(-5, -20, 1000))

    println("monogenic")
    println(compare_clust_rank_eps_plot_lists(m_dict, gene_to_number, K, logspace(-2, -20, 1000)))
    println()
    println("polygenic")
    println(compare_clust_rank_eps_plot_lists(p_dict, gene_to_number, K, logspace(-2, -20, 1000)))
    println("cancer")
    println(compare_clust_rank_eps_plot_lists(c_dict, gene_to_number, K, logspace(-2, -20, 1000)))
    println()

    # println("monogenic")
    # println(compare_clust_rank_eps_plot_lists(m_dict, gene_to_number, PPR, logspace(-0.3, -11, 1000)))
    # println()
    # println("polygenic")
    # println(compare_clust_rank_eps_plot_lists(p_dict, gene_to_number, PPR, logspace(-0.3, -11, 1000)))
    # println("cancer")
    # println(compare_clust_rank_eps_plot_lists(c_dict, gene_to_number, PPR, logspace(-0.3, -11, 1000)))
    # println()

    #plot_eps_clust_rank_PPR_new()

    #plot_eps_clust_rank_K_6()

    #plot_eps_balls_K()

    #plot_eps_ranking_K()

    # plot_eps_clust_rank_K_inthint2()

    #plot_eps_ranking_PPR()

    #plot_clust_ranking_K()

    ########################################################################

    # getting average similarities

    #println(c)
    #println(cluster_density_dict(c, A, gene_to_number))
    #print_dict(m)
    # println(gmean_dict(avg_simil_dict(m, PPR, gene_to_number)))
    # println(gmean_dict(avg_simil_dict(p, PPR, gene_to_number)))
    # println(gmean_dict(avg_simil_dict(c, PPR, gene_to_number)))

    ########################################################################

    # plotting similarity vs. clustering

    # dict_list = Dict[]
    # push!(dict_list, m)
    # push!(dict_list, p)
    # push!(dict_list, c)

    # plot_simil_cluster_mpl(dict_list, PPR, A, gene_to_number)

    ########################################################################

    # growing eps balls

    hncc = String["MSH2","MSH6","MLH1","MLH3","TGFBR2","PMS1","PMS2"]
    diabetes_mellitus = String["CAPN10","HNF4A","KCNJ11","ENPP1","RETN","PTPN1","TCF7L2","SLC2A4","PPP1R3A","LIPC","MAPK8IP1","GPD2","IRS1","NEUROD1","EPHX2","AKT2","SLC2A2"]
    essential_hypertension = String["AGT","SELE","GNB3","ECE1","ATP1B1","ADD1","PTGIS","RGS5","CYP3A5","NOS3","AGTR1","TNFRSF1B"]
    #g = grow(map((x) -> gene_to_number[x], essential_hypertension), K)
    #println(g)

    # for dis in p
    #     g = grow_adjusted(map((x) -> gene_to_number[x], dis[2]), PPR, logspace(-0.3, -10, 1000))
    #     println(dis[1] * string(g))
    #     println()
    # end


    # e_list = Any[]
    # n_list = Any[]
    # count = 0
    # for dis in c
    #     g = grow_adjusted(map((x) -> gene_to_number[x], dis[2]), PPR, logspace(-0.3, -10, 1000))
    #     println(dis[1] * string(g))
    #     println()
    #     push!(e_list, g[1])
    #     push!(n_list, g[2])
    #     if g[1] > .001
    #         count += 1
    #     end
    # end
    # println(e_list)
    # println(count/length(collect(c)))
    # println(n_list)

    #plot_eps_balls_PPR()


    ########################################################################


    # MEAN cluster coefficient data: 
    # monogenic: mean: 0.22173774531964982, std: 0.2630380071155948
    # polygenic: 0.07976084373143195, std: 0.23794231749896852
    # cancer: 0.12402828652828653, std: 0.15559822780404556

    #GMEAN cluster coefficient data (scale up by 1, gmean, then scale down by 1):
    # monogenic: 0.19795490858937836
    # polygenic: 0.06251427051904246
    # cancer: 0.11499882887378243

    # also computed a randomized (expected?) cluster coefficient by just picking random genes
    # randomized (using gmean): .001-.012 or so

    ###

    #avg dist data
    # cancer: mean 1.9757, std 0.2406799748
    # polygenic: mean 2.65359, std 0.5741563099
    # monogenic: mean 2.15022, std 0.6473228702

    #randomized: ??? i think it'd take a while to compute

    ###

    # DK similarity data (geometric mean, using iRef14 and beta = .00001 for DK kernel)
    # monogenic: 4.6747192796680025e-8
    # polygenic: 1.4542070974211536e-9
    # cancer: 1.4143268327919078e-7

    # NEW DK jk using beta = .0015
    # monogenic: 2.8767632462275813e-5
    # polygenic: 3.0258339520400395e-6
    # cancer: 5.284746848167864e-5

    #randomized: around 8.84e-8



    # NEW NEW DK, beta=.0015, geometric mean of geometric mean
    # monogenic: 2.7018353833102823e-7
    # polygenic: 1.4887848179590465e-8
    # cancer: 2.2391132970959677e-6

    #NEW randomized (using geom of geom): around 4.83e-9

    ###

    #similarity data (geometric mean, using iRef14 and RWR kernel, alpha = 0.35)
    # monogenic: 0.001277104244196532 (1.277e-3)
    # polygenic: 0.0002128996124145762 (2.129e-4)
    # cancer: 0.0004577627234738338 (4.577e-4)

    #randomized: around 1.5e-5

    # NEW similarity data, geom mean of geom mean, r=0.35
    # monogenic: 0.0001050004393754985 (1.05e-4)
    # polygenic: 2.5830538753445727e-5 (2.58e-5)
    # cancer: 0.0001319399622758312 (1.32e-4)

    #NEW randomized: around 6.35e-6


end
@time main()