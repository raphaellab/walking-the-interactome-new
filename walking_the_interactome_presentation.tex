%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This Beamer template was created by Cameron Bracken.
%% Anyone can freely use or modify it for any purpose
%% without attribution.
%%
%% Last Modified: January 9, 2009
%%
\documentclass[xcolor=x11names,compress,notheorems]{beamer}
\usepackage[upresentation=true]{uchitra_1}
\usepackage{subcaption}
\usepackage{float}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{\scshape Introduction}
\begin{frame}
\title{Summer Research Presentation}
\author{
Uthsav Chitra
}
\titlepage
\end{frame}

\begin{frame}{Overview}
\begin{itemize}
\item
Introduction
\item
Walking the Interaction Implementation
\item
Results
\item
Further Analysis
\end{itemize}
\end{frame}

\section{\scshape Implementation}
\begin{frame}{Implementation}
\begin{itemize}
\item
Suppose we have a PPI network $G$, and a kernel $K$ represented as a matrix such that $K[i,j]$ is a similarity measure between nodes $i$ and $j$ in $G$.
\item
For a disease $d$, we have a gene-set associated with the disease, say $g_1, ..., g_n$.
\item
Perform leave-one-out validation on this gene-set: that is, given $\{g_1, ..., g_n\} \ g_i$, use the structure of $G$ to deduce $g_i$ from some set $S = \{g_i, l_1, ..., l_m\}$.
\end{itemize}

How can we do this? We follow the method in \cite{walk}.
\end{frame}

\begin{frame}{Implementation}
\begin{itemize}
\item
Take $S$ to be the $100$ closest neighbors to $g_i$ on its chromosome (measured by genomic distance). 
\begin{equation}
score(j) = \text{j-th entry of } (K\cdot e),
\end{equation}
where $e_k=1$ if $k = g_1, ..., g_n$, and $0$ otherwise.
\item
If $K$ is the diffusion kernel, then the score above represents the heat given to node $j$ by each of the disease genes.
\item
If $K$ is the personalized Pagerank matrix, then the score above is the probability of getting to node $j$ in the steady-state of the random walk with restart method.
\end{itemize}
\end{frame}

\begin{frame}{Other Details}
\begin{itemize}
\item
With this scoring system, we can rank the genes in $S$ by score. Ideally gene $g_i$ would be ranked first.
\item
\cite{walk} looks at $50$-fold enrichment, which they define as 
\begin{equation*}
\frac{50}{\text{ranking}}.
\end{equation*}
\item
Look at diseases in three different classes: monogenic, polygenic, and cancer. For each disease $d$, perform the leave-one-out validation process for each gene to obtain $n$ different rankings; average them together to obtain a mean ranking for $d$. Finally, average together the mean ranking for each disease $d$ in a class, and do this for each disease class.
\end{itemize}
\end{frame}

\section{\scshape Results}
\begin{frame}{WTI Results}
\begin{figure}
\includegraphics[scale=0.4]{wti_results}
\caption{Results from \cite{walk}}
\end{figure}
\end{frame}

\begin{frame}{WTI Results}
\begin{itemize}
\item
The y-axis in their plot is $50$-fold enrichment. 
\item
Note the huge difference in cancer genes between the diffusion and RWR kernels. We wanted to try and recreate the gaps they had.
\end{itemize}
\end{frame}

\begin{frame}{My Results}
\begin{figure}
\includegraphics[scale=0.36]{plots/original_summary/iref14_dontinclude100_plots.png}
\includegraphics[scale=0.36]{plots/original_summary/iref14_ranking_dontinclude100_plots.png}
\caption{Enrichment, ranking results from my implementation}
\end{figure}
\end{frame}

\begin{frame}{My Results}
\begin{itemize}
\item
Performed sensitivity analysis to choose optimal parameters for RWR, diffusion kernel. Worked with iRef14 rather than IntHint2 because it is a bigger network and gave better results.
\item
My enrichment plots mostly resemble theirs, although their enrichments are overall higher because their network was bigger.
\item
More interestingly, the cancer gap is not in my ranking plots though-- which makes sense when one thinks about how enrichment is defined. However, there is a clear polygenic gap between the RWR and diffusion kernels. We want to look at where that gap comes from.
\end{itemize}
\end{frame}

\section{\scshape Analysis}
\begin{frame}{Initial Attempts}
Started out by looking at rank distributions instead of mean rankings, although these don't give too much information.

\begin{figure}
\includegraphics[scale=0.27]{plots/distribution_plots/jitter_scatter_ranking_matplotlib.pdf} 
\includegraphics[scale=0.27]{plots/distribution_plots/violin_ranking_matplotlib.pdf}

\caption{Rank Distribution Plots}
\label{iref14_dist}
\end{figure}
\end{frame}

\begin{frame}{Initial Attempts}
Another idea was to see how much degree affects rank: do highly ranked nodes tend to have higher degree?
\begin{figure}
\includegraphics[scale=0.29]{plots/degree_plots/degree_rank_rwr_all.pdf} 
\includegraphics[scale=0.29]{plots/degree_plots/degree_rank_dk_all.pdf} 
\end{figure}
The only thing this rules out though is having a high ranking and a high degree.
\end{frame}

\begin{frame}{Quick Statistics}
\begin{center}
$\begin{array}{|c|c|c|c|c|}
\hline
 & \text{Avg Edge} & \text{Avg Dist} & \text{Avg Simil,} & \text{Avg Simil,} \\ 
  & \text{Density} &  & \text{RWR} & \text{DK} \\
\hline
\text{Monogenic} & 0.221 & 2.150 & 1.277\cdot 10^{-3} & 4.675\cdot 10^{-8} \\ 
\hline
\text{Polygenic} & 0.080 & 2.654 & 2.129\cdot 10^{-4} & 1.454\cdot 10^{-9} \\ 
\hline
\text{Cancer} & 0.124 & 1.976 & 4.578\cdot 10^{-4} & 1.414\cdot 10^{-7} \\ 
\hline
\end{array}$
\end{center}
So on the whole, the polygenic disease genes are much less clustered than monogenic/cancer disease genes.
\end{frame}


\begin{frame}{Similarity vs. Density}
\begin{figure}
\includegraphics[scale=0.28]{plots/cluster_similarity_plots/iref14_dk_similarity_edgedensity_loglog.pdf} 
\includegraphics[scale=0.28]{plots/cluster_similarity_plots/iref14_rwr_similarity_edgedensity_loglog.pdf} 
\end{figure}
In these plots, we omit points with edge density $0$.
\end{frame}

\begin{frame}{$\epsilon$-balls}
\begin{itemize}
\item
Since most disease-gene families aren't that connected, we want to look at graph similarity rather than edge density.
\item
Spent a lot of time looking at maximal partitions of disease-gene families--with the hypothesis that polygenic diseases are more spread out/clustered than monogenic, cancer ones--but ran into lots of problems. 

\end{itemize}
\end{frame}

\begin{frame}{$\epsilon$-balls}
\begin{itemize}
\item
Ben suggested an idea related to the SANTA paper. Given disease $d$ with disease genes $g_1, ..., g_n$, start with balls of radius $\epsilon$ around each $g_i$, such that no two balls intersect. Gradually raise $\epsilon$ until the balls start to combine, and see how long it takes for the $\epsilon$-balls to form one contiguous cluster.
\item
The idea is that, for spread out genes, by the time the $\epsilon$-balls coalesce, they should have a large number of other genes inside them.
\item
Can try this with the PPR, diffusion kernel matrices defining a ``distance" between two nodes. Since higher similarity corresponds to smaller distance, would technically need to lower $\epsilon$ rather than raise it.
\end{itemize}
\end{frame}

\begin{frame}{$\epsilon$-ball DK Results}
\begin{figure}
\includegraphics[scale=0.49]{plots/eps_balls_plots/iRef14/iref14_dk_eps_ranking.pdf}
\end{figure}
\end{frame}

\begin{frame}{$\epsilon$-ball DK Results}
\begin{figure}
\includegraphics[scale=0.49]{plots/eps_balls_plots/iRef14/iref14_dk_eps_ballsize.pdf}
\end{figure}
\end{frame}

\begin{frame}{$\epsilon$-ball DK Results}
\begin{figure}
\includegraphics[scale=0.49]{plots/eps_balls_plots/iRef14/iref14_dk_ballsize_ranking.pdf} 
\end{figure}
\end{frame}

\begin{frame}{$\epsilon$-ball RWR Results}
\begin{figure}
\includegraphics[scale=0.49]{plots/eps_balls_plots/iRef14/iref14_rwr_eps_ranking.pdf}
\end{figure}
\end{frame}

\begin{frame}{$\epsilon$-ball RWR Results}
\begin{figure}
\includegraphics[scale=0.49]{plots/eps_balls_plots/iRef14/iref14_rwr_eps_ballsize.pdf}
\end{figure}
\end{frame}

\begin{frame}{$\epsilon$-ball RWR Results}
\begin{figure}
\includegraphics[scale=0.49]{plots/eps_balls_plots/iRef14/iref14_rwr_ballsize_ranking.pdf} 
\end{figure}
\end{frame}

\begin{frame}{$\epsilon$-ball Results}
\begin{itemize}
\item
There are two distinct $\epsilon$ clusters for the diffusion kernel, and almost all of the polygenic diseases are in the lower one. Also, the only diseases with mean rank below $10$ are in the higher cluster.
\item
In the RWR plots, the polygenic diseases also have lower $\epsilon$ values. Moreover, there is a positive association between higher $\epsilon$ (closer together), and lower average rank.
\item
In the RWR plots, there is also a positive association between having more genes in the $\epsilon$-balls (when they first become one contiguous cluster), and having a higher average rank.
\end{itemize}
\end{frame}

\begin{frame}{Future Goals}
\begin{itemize}
\item
Using the previous method, we can get clusterings for some diseases. Would be interesting to see how these clusters compare to those obtained through some embedding method. I started looking at this, but I discovered I need to do some debugging first.
\item
Could also look at other statistics and ways polygenic disease-gene families can be differentiated from monogenic and cancer disease-gene families.
\end{itemize}
\end{frame}

\begin{frame}{References}
\begin{thebibliography}{1}
\setbeamertemplate{bibliography item}[text]
\bibitem{walk}
Sebastian Kohler, Sebastian Bauer, Denise Horn, and Peter N. Robinson. \textit{Walking the Interactome for Prioritization of Candidate Disease Genes.} The American Journal of Human Genetics \textit{82}, 949-958.
\end{thebibliography}
\end{frame}

\end{document}