import networkx as nx
import matplotlib.pyplot as plt

# part 1

G = nx.Graph()

G.add_nodes_from([1,2,3,4,5])
edges = [(1, 2), (2, 3), (3, 4), (2, 4), (4, 5)]
G.add_edges_from(edges)

pos = nx.spring_layout(G)

nx.draw_networkx_nodes(G, pos, node_color='b', node_size=1200)
nx.draw_networkx_edges(G, pos, edgelist=edges)
plt.axis('off')
plt.savefig('slide1.png', bbox_inches='tight')

plt.clf()

#part 2

G = nx.DiGraph()
G.add_nodes_from([1,2,3,4,5])
red = [(2,1), (2,3), (2,4)]
black=[(3,4),(4,5)]

nx.draw_networkx_nodes(G, pos, nodelist=[1,3,4,5], node_color='b', node_size=1200)
nx.draw_networkx_nodes(G, pos, nodelist=[2], node_color='r', node_size=1200)

nx.draw_networkx_edges(G, pos, edgelist=black, arrows=False)
nx.draw_networkx_edges(G, pos, edgelist=red, edge_color='r', arrows=True)
plt.axis('off')
plt.savefig('slide2.png', bbox_inches='tight')

plt.clf()

#part 3

G = nx.Graph()

G.add_nodes_from([1,2,3,4,5])
edges = [(1, 2), (2, 3), (3, 4), (2, 4), (4, 5)]
G.add_edges_from(edges)

nx.draw(G,pos,node_color=[3,4,2,2,1],node_size=1200,cmap=plt.cm.Oranges)
plt.axis('off')
plt.savefig('slide3.png', bbox_inches='tight')

plt.clf()