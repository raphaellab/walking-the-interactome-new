using JLD
using HDF5
using ArgParse

##############################################################################################################################
# Computing ranks
##############################################################################################################################

# WHAT IT DOES:
# uses each kernel in kernel_list to determine the rank of disease_gene relative to candidate_genes

# GIVEN:
# kernel_list, a list of kernels to be tested
# disease_members -- list of genes associated with disease
# candidate_genes -- list of genes to be ranked
# disease_gene -- the candidate gene whose ranking we're interested in.
# return_raw_score -- if true, return the raw score

# RETURNS:
# list of (ranking of disease_gene against all genes in candidate_genes) for each kernel in kernel_list
function kernel_score(kernel_list :: Array, disease_members :: Array{Int, 1}, 
    candidate_genes :: Array{Int, 1}, disease_gene :: Int, return_raw_score=false)

    # not sure why this would happen but better to be safe
    if !(disease_gene in candidate_genes)
        candidate_genes = push!(candidate_genes, disease_gene)
    end
    
    #how many genes are there?
    number_of_genes = size(kernel_list[1],1)
    
    #make p_0 and initialize it with the disease_members
    p_init = zeros(number_of_genes,1)

    d= length(disease_members)
    
    for i in disease_members
        if i != disease_gene
            p_init[i] = 1/(d-1)         
        end
    end

    #get the ranks of the disease gene for each kernel
    ranks = Int[]

    if return_raw_score
        raw_scores = zeros(length(kernel_list), length(candidate_genes))
        k=1
    end

    for kernel in kernel_list

        #p_inf = kernel*p_init #steady-state

        # candidate_scores=zeros(size(candidate_genes,1))

        # for i in range(1,size(candidate_genes,1))
        #     candidate_scores[i] = p_inf[candidate_genes[i]]
        # end

        #so lets compute candidate_scores directly

        #well i probably shouldnt do a straight up matrix multiplication
        #in fact, we dont even need to compute p_inf, we just need candidate_scores

        #tl;dr: doing above code but w/ for loops instead of big matrix mult

        candidate_scores = zeros(size(candidate_genes, 1))

        for i=1:size(candidate_genes,1)

            cand_gene_i = candidate_genes[i]
            i_score = 0

            #need to do cand_gene_i-th row of kernel dotted with p_init
            #but we just explicitly write that out

            for j=1:size(disease_members,1)
                dis_memb_j = disease_members[j]
                if dis_memb_j != disease_gene
                    i_score = i_score + kernel[cand_gene_i, dis_memb_j]
                end
            end

            i_score = i_score / (d-1)

            candidate_scores[i] = i_score
        end
        rank = find_ranking(candidate_genes, candidate_scores, disease_gene)
        push!(ranks, rank)

        if return_raw_score
            raw_scores[k,:] = candidate_scores'
            k=k+1
        end
    end
    if return_raw_score
        return raw_scores
    end
    return ranks
end

#pick out candidate degree w highest degree
function kernel_score_deg(degs, candidate_genes, disease_gene)
    # not sure why this would happen but better to be safe
    if !(disease_gene in candidate_genes)
        candidate_genes = push!(candidate_genes, disease_gene)
    end

    cand_scores = Int[]

    for i=1:length(candidate_genes)
        push!(cand_scores, degs[candidate_genes[i]])
    end

    return find_ranking(candidate_genes, cand_scores, disease_gene)
end

# WHAT IT DOES: 
# helper function for finding ranking

# GIVEN: 
# candidate_genes -- list of candidate_genes to be ranked
# candidate_scores -- the scores corresponding to each of the candidate_genes above
# disease_gene -- gene whose ranking we're interested in

# RETURNS:
# ranking of disease_gene, using worst-case to break ties.
function find_ranking{S <: Real}(candidate_genes :: Array{Int, 1}, candidate_scores :: Array{S, 1}, disease_gene :: Int)
    #where disease_gene is located in candidate_genes
    disease_gene_index = findin(candidate_genes, disease_gene)

    #disease_gene's score
    disease_gene_score = candidate_scores[disease_gene_index]

    #candidate_scores sorted with smallest in front
    candidate_scores_sorted = sort(candidate_scores)

    #this gives TOTAL - actual rank (using worst case methodology)
    disease_inverse_ranking = findfirst(candidate_scores_sorted, disease_gene_score[1])
    disease_actual_rank = size(candidate_genes,1) - disease_inverse_ranking + 1
    return disease_actual_rank
end

##############################################################################################################################
# Clustering-related functions
##############################################################################################################################

function cluster_gene_list(genes, kernel, threshold)
    new_gene_digraph = kernel[genes, genes] .> threshold;
    g = Digraph(new_gene_digraph)
    scc = strongly_connected_components(g)
    return map(x -> map(y -> genes[y]), scc)
end

function cluster_kernel_score(kernel_list :: Array, disease_members :: Array{Int, 1}, 
    candidate_genes :: Array{Int, 1}, disease_gene :: Int, threshold, p=1)

    raw_scores = zeros(length(kernel_list),length(candidate_genes))

    clusters = cluster_gene_list(disease_members, kernel, threshold)

    for i=1:length(clusters)
        cluster_i = clusters[i]
        scores_cluster_i = kernel_score(kernel_list, disease_members, 
            candidate_genes, disease_gene, return_raw_score=true)
        raw_scores = raw_scores + (scores_cluster_i ./ length(cluster_i)).^p
    end

    ranks = Int[]

    for i=1:length(kernel_list)
        r = find_ranking(candidate_genes, squeeze(raw_scores[i, :]), disease_gene)
        push!(ranks, r)
    end
    return ranks
end

##############################################################################################################################
# Constructing PPR, K (Shamelessly stolen from Matt)
##############################################################################################################################

function A_add_diag{S<:Real, T<:Real, U<:Real, V<:Real}(alpha::S, A::Matrix{T}, beta::U, b::Vector{V})

    # We are computing B = alpha*A + beta*diagm(b) for A and b with compatible dimensions.

    m, n = size(A)
    p = length(b)
    min(m,n)==p || error("Dimensions of matrix incompatible with length of vector")

    B = alpha*A
    for i=1:p
        @inbounds B[i,i] += beta*b[i]
    end

    return B

end

function make_dk_mat{S<:Real, T<:Real}(A::Matrix{S}, t::T)

    # We are computing Li = expm(-L*t), where L = D-A.

    d = sum(A,1)[:]
    L = A_add_diag(t,A,-t,d)
    B = eigfact(Hermitian(L))
    Li = B[:vectors] * (exp(B[:values]) .* B[:vectors]')

    return Li

end

function make_rwr_mat{S<:Real, T<:Real}(A::Matrix{S}, beta::T)

    # We are computing PPR = beta*inv(I-(1-beta)*W), where W = A*inv(D).

    m, n = size(A)
    d = sum(A,1)
    W = A ./ d

    PPR = beta*inv(A_add_diag(-(1.0-beta),W,1.0,ones(n)))

    return PPR

end

function make_rwr_mat_pmf(P, r)
    #compute PPR = r * inv(I - (1-r)*P)

    m, n = size(P)

    PPR = r * inv(A_add_diag(-(1.0-r), P, 1.0, ones(n)))
    return PPR
end

##############################################################################################################################
# Loading edge/index files and candidate .txt files
##############################################################################################################################

# WHAT IT DOES:
# constructs adjacency matrix

# GIVEN: 
# edge_file -- .txt file w/ each line containing an edge

# RETURNS: 
# Adjacency matrix for graph given by edge_file
function load_edge_file(edge_file :: String)
    edges = readdlm(edge_file, Int64)
    n = maximum(edges)
    A = zeros(Int, (n, n))
    for i=1:size(edges, 1)
        A[edges[i, 1], edges[i, 2]] = 1
        A[edges[i, 2], edges[i, 1]] = 1
    end
    return A
end

# WHAT IT DOES:
# turns empty dictionaries into ones mapping gene --> number and number --> gene,
# according to the index file

# GIVEN:
# index_file -- .txt file where each line is a number followed by a gene symbol
# g2n, n2g -- empty dictionaries

# RETURNS:
# nothing, but it mutates the g2n and n2g objects.
function process_index_file_new(index_file :: String, g2n :: Dict, n2g :: Dict)
    f = open(index_file)
    for line in eachline(f)
        x=split(line)
        num = parse(Int, x[1])
        gene_name = strip(x[2])
        if length(x) > 2
            gene_name = join(x[2:end], " ")
        end
        g2n[gene_name] = num
        n2g[num]=gene_name
    end
end

# HOW c_file SHOULD BE FORMATTED:
# disease name
# number of disease genes
# disease gene (commas to separate potential alternate names)
# number of candidate genes
# list of candidate genes
# and repeat.

# WHAT IT DOES:
# creates dictionary out of candidate file. checks for disease_gene alternate names

# GIVEN:
# c_file -- .txt file of diseases and candidates formatted as described above
# gene_to_num -- dictionary mapping gene --> number

# RETURNS:
# dictionary mapping diseases --> (dictionary mapping gene --> list of candidate genes)
function read_candidates(c_file :: String, gene_to_num :: Dict)
    f = open(c_file)
    disease_dict = Dict{String, Dict{String, Array{Int, 1}}}()

    while !eof(f)
        gene_dict = Dict{String, Array{Int, 1}}()
        disease_name = strip(readline(f))
        number_of_dis_genes = parse(Int, strip(readline(f)))

        for i in range(1, number_of_dis_genes)
            dis_gene_name = strip(readline(f))
            cand_list = Int[]
            if contains(dis_gene_name, ",")
                gene_names = split(dis_gene_name, ",")
            else
                gene_names = [dis_gene_name]
            end
            have = false
            real_name = ""
            for z in range(1, length(gene_names))
                if haskey(gene_to_num, gene_names[z])
                    have = true
                    real_name = gene_names[z]
                    break
                end
            end

            if have
                dis_gene_num = gene_to_num[real_name]
                number_of_cand = parse(Int, strip(readline(f)))
                for j in range(1, number_of_cand)
                    j_cand = strip(readline(f))
                    try
                        j_num = gene_to_num[j_cand]
                        if length(cand_list) < 100
                            push!(cand_list, j_num)
                        end
                    end
                end
                #     if candidate_genes also have alt names, use the below:
                #     i didn't put it in since it could slow things down

                #     if contains(j_cand, ",")
                #         cand_names = split(j_cand, ",")
                #     else
                #         cand_names = [j_cand]
                #     end
                #     has = false
                #     real_cand_name = ""
                #     for w in range(1, length(cand_names))
                #         if haskey(gene_to_num, cand_names[w])
                #             has = true
                #             real_cand_name = cand_names[w]
                #             real_cand_num = gene_to_num[real_cand_name]
                #             if length(cand_list) < 100
                #                 push!(cand_list, real_cand_num)
                #             end
                #             break
                #         end
                #     end
                # end
            else
                number_of_cand = parse(Int, strip(readline(f)))
                for j in range(1, number_of_cand)
                    readline(f)
                end
            end

            if real_name == ""
                gene_dict[dis_gene_name] = unique(cand_list)
            else
                gene_dict[real_name] = unique(cand_list)
            end
        end
        disease_dict[disease_name] = gene_dict
    end
    close(f)
    return disease_dict
end

##############################################################################################################################
# Extracting results
#############################################################################################################################

# WHAT IT DOES:
# for each disease, uses kernels in kernel_list to give rankings.
# a disease gets its rankings by seeing how well a kernel
# can predict each of the disease genes, given the other disease genes (see kernel_score above)

# GIVEN: 
# x_dict -- disease_dict formatted as described above, disease --> (gene --> list of candidate genes)
# gene_to_number -- dictionary mapping gene --> number
# kernel_list -- list of kernels. kernels are represented as 2D matrices
# use_clustering -- if true, use new clustering method

# RETURNS:
# dictionary mapping diseases --> n-row array, where row i is kernel_list[i]'s ranking for each
# of the disease genes
function dict_results(x_dict :: Dict, gene_to_num :: Dict, kernel_list :: Array, use_clustering=false, threshold=0)

    disease_rankings_dict = Dict()
    for disease in keys(x_dict)

        gene_dict = x_dict[disease]

        disease_gene_family_name = [key for key in keys(gene_dict)] #names of all genes in disease gene family

        #but now we need to take out the ones we dont have data for
        disease_gene_family_number = Int64[]
        for d_g in disease_gene_family_name
            if haskey(gene_to_num, d_g)
                push!(disease_gene_family_number, gene_to_num[d_g])
            end
        end

        disease_rankings = zeros(Int64, size(kernel_list,1), size(disease_gene_family_name, 1))

        for gene in keys(gene_dict)
            cand_list = gene_dict[gene]

            #get gene rankings for each kernel
            ranks = zeros(size(kernel_list, 1))

            #if its in the gene_to_num dict
            if size(cand_list, 1) > 0
                if use_clustering
                    scores = cluster_kernel_score(kernel_list, disease_gene_family_number, 
                        cand_list, gene_to_num[gene], threshold)
                else
                    scores = kernel_score(kernel_list, disease_gene_family_number, cand_list, gene_to_num[gene])
                end
                place_to_insert = findfirst(ranks, 0)
                count = 0
                for score in scores
                    ranks[place_to_insert+count] = score
                    count = count + 1
                end
            else
                place_to_insert = findfirst(ranks, 0)
                count = 0
                for kernel in kernel_list
                    ranks[place_to_insert + count] = 100
                    count = count + 1
                end
            end

            #add ranks to disease_rankings
            place_to_insert = findfirst(disease_rankings, 0)
            for i in range(1, size(kernel_list, 1))
                disease_rankings[place_to_insert + i - 1] = ranks[i]
            end
        end
        disease_rankings_dict[disease] = disease_rankings
    end
    return disease_rankings_dict
end

### FOR DEGREE DATA

function dict_results_degree(x_dict :: Dict, gene_to_num :: Dict, kernel_list :: Array, degrees)

    disease_rankings_dict = Dict()
    for disease in keys(x_dict)

        gene_dict = x_dict[disease]

        disease_gene_family_name = [key for key in keys(gene_dict)] #names of all genes in disease gene family

        #but now we need to take out the ones we dont have data for
        disease_gene_family_number = Int64[]
        for d_g in disease_gene_family_name
            if haskey(gene_to_num, d_g)
                push!(disease_gene_family_number, gene_to_num[d_g])
            end
        end

        disease_rankings = zeros(Int64, size(kernel_list,1), size(disease_gene_family_name, 1))
        degree_list = Int[]
        for gene in keys(gene_dict)
            cand_list = gene_dict[gene]

            #get gene rankings for each kernel
            ranks = zeros(size(kernel_list, 1))

            #if its in the gene_to_num dict
            if size(cand_list, 1) > 0
                scores = kernel_score(kernel_list, disease_gene_family_number, cand_list, gene_to_num[gene])
                place_to_insert = findfirst(ranks, 0)
                count = 0
                for score in scores
                    ranks[place_to_insert+count] = score
                    count = count + 1
                end
            else
                place_to_insert = findfirst(ranks, 0)
                count = 0
                for kernel in kernel_list
                    ranks[place_to_insert + count] = 100
                    count = count + 1
                end
            end

            #add ranks to disease_rankings
            place_to_insert = findfirst(disease_rankings, 0)
            for i in range(1, size(kernel_list, 1))
                disease_rankings[place_to_insert + i - 1] = ranks[i]
            end

            try
                n = gene_to_num[gene]
                deg = degrees[n]
                push!(degree_list, deg)
            catch
                push!(degree_list, -1)
            end
        end
        disease_rankings_dict[disease] = disease_rankings, degree_list
    end
    return disease_rankings_dict
end

#for computing naive predictions stuff
function dict_results_deg(x_dict, gene_to_num, degs)

    disease_rankings_dict = Dict()
    for disease in keys(x_dict)

        gene_dict = x_dict[disease]

        disease_rankings = zeros(Int64, 1, length(keys(gene_dict)))

        for gene in keys(gene_dict)
            cand_list = gene_dict[gene]

            rank = 0

            #if its in the gene_to_num dict
            if size(cand_list, 1) > 0
                #scores = kernel_score(kernel_list, disease_gene_family_number, cand_list, gene_to_num[gene])
                rank = kernel_score_deg(degs, cand_list, gene_to_num[gene])
            else
                rank = 100
            end

            #add ranks to disease_rankings
            place_to_insert = findfirst(disease_rankings, 0)
            disease_rankings[place_to_insert] = rank
        end
        disease_rankings_dict[disease] = disease_rankings
    end
    return disease_rankings_dict
end

# WHAT IT DOES:
# given a results dict (like the one returned above), returns the average rank/enrichment for each kernel
# by getting a disease's average rank/enrichment, and then averaging over all diseases

# GIVEN: 
# results -- disease_dict formatted as so: disease --> (gene --> (list of ranks for each kernel))
# include_100 -- boolean indicating whether to include ranks of 100 in the average or not
# use_enrich -- boolean indicating whether to look at avg. ranking or enrichment

# RETURNS:
# list of average rankings for each kernel
function get_average(results::Dict, include_100::Bool, use_enrich::Bool)
    # make dict mapping disease to ranks (list of numbers for each kernel)
    disease_rank = Dict()

    kernel_num = 0 #this is hacky, but need to get the number of kernels somehow

    # loop through diseases and get the avg rank
    for disease in keys(results)
        d = results[disease]

        # if requested, remove 100's
        if !include_100
            new_d = Array(Int, size(d, 1), 0) #make new array

            # we loop through d and add to new_d if the rank isn't a 100
            for i in range(1, size(d, 2))
                if d[1,i] != 100
                    vec = Int[]
                    for j in range(1, size(d, 1))
                        vec = [vec; d[j, i]]
                    end
                    new_d = [new_d vec]
                end
            end
            d = new_d
        end

        # if looking at enrichment, do 50/x for each x in d
        if use_enrich
            d = 50 ./ d
        end

        d_avg = sum(d, 2) ./ size(d, 2) #average rank for each kernel
        if size(d,2) == 0
            continue
        end
        disease_rank[disease] = d_avg #add this to the disease ranking dict
        kernel_num = size(d_avg, 1) #for the size of running_sum array
    end

    # now we loop through disease_rank and add the scores for each disease
    # and then divide by the number of diseases

    #the dict we will be returning
    running_sum = zeros(Int, kernel_num, 1) #need kernel_num for its size
    total = 0
    for dis in keys(disease_rank)
        running_sum = running_sum + disease_rank[dis]
        total = total + 1
    end
    return running_sum ./ total #use ./ since we're looking at a list of numbers
end

# command line parsing
# function parse_commandline()
#     s = ArgParseSettings()

#     @add_arg_table s begin
#         "--mload", "-l"
#             help = "if user wants to save/load matrices files. argument is path to save in; enter \"here\" to save in current dir."
#             #arg_type = String
#         "--dload"
#             help = "folder to load monogenic/polygenic/cancer disease .txt files from (they should be named monogenic_data.txt, polygenic_data.txt, cancer_data.txt)"
#             arg_type = String
#         "--rwr", "-r"
#             help = "use random walk w/ restart kernel, argument is r value"
#             arg_type = Union(Int, FloatingPoint)
#         "--dk", "-d"
#             help = "use diffusion kernel, argument is beta value"
#             arg_type = Union(Int, FloatingPoint)
#         "--kernel"
#             help = "use user-input kernel, argument[s] being the path[s] to kernel[s] as a .jld file. for each file, the next argument should be the jld file's key for the kernel, since jld files are dicts"
#             nargs = '*'
#         "-m"
#             help = "look at monogenic disease data. if -l isn't used, assumed to be in same directory as this script"
#             action = :store_true
#         "-p"
#             help = "look at polygenic disease data. if -l isn't used, assumed to be in same directory as this script"
#             action = :store_true
#         "-c"
#             help = "look at cancer disease data. if -l isn't used, assumed to be in same directory as this script"
#             action = :store_true
#         "-o"
#             help = "look at other disease data file[s]. argument[s] are path[s] to file[s]. also argparse kinda sucks so don't put index or edge files after this"
#             nargs = '*'
#             arg_type = String
#         "--raw"
#             help = "give this option if you just want raw data instead of averages"
#             action = :store_true
#         "index_file"
#             help = "required argument: gene index file"
#             required = true
#         "edge_file"
#             help = "required argument: edge list file"
#             required = true

#     end

#     return parse_args(s)
# end

function main()
    # get the parsed arguments

    parsed_args = parse_commandline()

    ###########################################################################################
    # load index file, edge list, and process these files
    # to create gene_to_number, number_to_gene, and adjacency amtrix A

    index_file = parsed_args["index_file"]
    edge_file = parsed_args["edge_file"]

    gene_to_number = Dict{String, Int}()
    number_to_gene = Dict{Int, String}()
    process_index_file_new(index_file, gene_to_number, number_to_gene)    
    A = load_edge_file(edge_file)

    ###########################################################################################
    # loading candidate dicts

    # get loading prefix if provided
    cand_load_prefix = parsed_args["dload"]
    if cand_load_prefix == nothing
        cand_load_prefix = ""
    else
        # if it doesn't end n a '/', add one
        if cand_load_prefix[length(cand_load_prefix)] != "/"
            cand_load_prefix = cand_load_prefix * "/"
        end
    end

    # to_look_at keeps track of the candidates the user requested
    to_look_at = Dict()

    if parsed_args["m"] == true
        monogenic_dict = read_candidates(cand_load_prefix * "monogenic_data.txt", gene_to_number)
        to_look_at["monogenic"] = monogenic_dict
    end

    if parsed_args["p"] == true
        polygenic_dict = read_candidates(cand_load_prefix * "polygenic_data.txt", gene_to_number)
        to_look_at["polygenic"] = polygenic_dict
    end

    if parsed_args["c"] == true
        cancer_dict = read_candidates(cand_load_prefix * "cancer_data.txt", gene_to_number)
        to_look_at["cancer"] = cancer_dict
    end

    if parsed_args["o"] != nothing
        other_list = parsed_args["o"]
        # loop through and read the user-inputted candidates
        for o in other_list
            other_dict = read_candidates(o, gene_to_number)
            o_name = split(o, "/")
            o_name = o_name[size(o_name,1)]
            to_look_at[o_name] = other_dict
        end
    end

    ###########################################################################################
    # load/create kernels

    # for naming purposes, if user wants PPR, K to be saved
    # raw_edge_name will be included in the saved file's name
    raw_edge_name = ""
    if contains(edge_file, "/")
        sp = split(edge_file, "/")
        raw_edge_name = sp[size(sp,1)]
    else
        raw_edge_name = edge_file
    end


    kernel_list = Matrix[] # doing this instead of Matrix{FloatingPoint}[] makes push!-ing a LOT faster
    kernel_names = String[] # names of the kernels, for when results are printed out

    # parse where to load matrices from
    mload_prefix = parsed_args["mload"]

    if mload_prefix=="here"
        mload_prefix = ""
    end

    # put PPR, K in highest scope
    PPR = -1 
    K= -1

    # get potential r, beta
    r = parsed_args["rwr"]
    beta = parsed_args["dk"]

    # now to load PPR, K kernels!

    # if user wants to load/save matrices
    if mload_prefix != nothing
        if r!= nothing
            try
                PPR = load(mload_prefix * raw_edge_name * "_rwr_mat_" * string(r) * ".jld", "M") #not sure why i named it M
            catch
                println("dont be here!")
                PPR = make_rwr_mat(A, r)
                save(mload_prefix * raw_edge_name * "_rwr_mat_" * string(r) * ".jld", "M", PPR) 
            end
            push!(kernel_list, PPR)
            push!(kernel_names, "rwr")
        end

        if beta != nothing
            try
                K = load(mload_prefix * raw_edge_name * "_dk_mat_" * string(beta) * ".jld", "K")
            catch
                println("dont be here!")
                K = make_dk_mat(A, beta)
                save(mload_prefix * raw_edge_name * "_dk_mat_" * string(beta) * ".jld", "K", K)
            end
            push!(kernel_list, K)
            push!(kernel_names, "dk")
        end
    # otherwise just make them
    else
        if r != nothing
            PPR = make_rwr_mat(A, r)
            push!(kernel_list, PPR)
            push!(kernel_names, "rwr")
        end
        if beta != nothing
            K = make_dk_mat(A, beta)
            push!(kernel_list, K)
            push!(kernel_names, "dk")
        end
    end

    # time to load the other kernels!
    if parsed_args["kernel"] != nothing
        k_list = parsed_args["kernel"]

        for i in range(1,size(k_list,1))
            if i%2 ==1
                kern = load(k_list[i], k_list[i+1]) #load k_list[i+1] from the dict
                push!(kernel_list, kern)

                #get the kernel name
                i_split = split(k_list[i], "/")

                #add that to kernel name list
                push!(kernel_names, i_split[size(i_split, 1)])
            end
        end
    end

    ###########################################################################################
    # getting/printing results

    for t in keys(to_look_at)
        t_results = dict_results(to_look_at[t], gene_to_number, kernel_list)

        #if user just wants raw data:
        if parsed_args["raw"]
            println(t)
            println(t_results)

        #otherwise, print avgs
        else
            for include_100 in [true, false]
                enrich_avgs_t = get_average(t_results, include_100, true) # enrichment averages
                reg_avgs_t = get_average(t_results, include_100, false) # rank averages

                println("including 100: " * string(include_100))

                count = 1
                for name in kernel_names
                    println("enrich avgs " * name * " " * t * ": " * string(enrich_avgs_t[count]))
                    println("real avgs " * name * " " * t * ": " *  string(reg_avgs_t[count]))
                    count = count + 1
                end
            end
        end
    end
end

function make_rank_approx_plot_K(index_file, edge_file, beta)

    edge_raw = split(edge_file, "/")
    edge_raw = edge_raw[length(edge_raw)]

    gene_to_number = Dict{String, Int}()
    number_to_gene = Dict{Int, String}()
    process_index_file_new(index_file, gene_to_number, number_to_gene)    
    A = load_edge_file(edge_file)

    println("making K!")
    K=5
    try
        K = load(edge_raw * "_dk_mat_" * string(beta) * ".jld", "K")
    catch
        K = make_dk_mat(A, beta)
        save(edge_raw * "_dk_mat_" * string(beta) * ".jld", "K", K)
    end
    println("made K!")
    K = Hermitian(K)
    println("finding eigenvalues!")
    e = eigvals(K*transpose(K))
    e = sort(e, rev=true)
    println("found eigenvalues!")
    cur = sum(e .* e)
    a=FloatingPoint[]
    for i=1:size(K,1)
        cur = cur - e[i]
        if cur < 0
            cur = 0
        end
        push!(a, sqrt(cur))
    end
    return a
end

function make_rank_approx_plot_PPR(index_file, edge_file, r)

    edge_raw = split(edge_file, "/")
    edge_raw = edge_raw[length(edge_raw)]

    gene_to_number = Dict{String, Int}()
    number_to_gene = Dict{Int, String}()
    process_index_file_new(index_file, gene_to_number, number_to_gene)    
    A = load_edge_file(edge_file)
    println("hey!")
    println("making PPR!")
    PPR=5
    try
        PPR = load(edge_raw * "_rwr_mat_" * string(r) * ".jld", "M")
    catch
        PPR = make_rwr_mat(A, r)
        save(edge_raw * "_rwr_mat_" * string(r) * ".jld", "M", PPR)
    end
    println("made PPR!")
    PPR2 = Hermitian(PPR * transpose(PPR))
    println("finding eigenvalues!")
    e = eigvals(PPR2)
    e = sort(e, rev=true)
    println(e[1:100])
    println("found eigenvalues!")
    s=size(PPR,1)
    a = [sqrt(sum(e[i:s])) for i=2:(s+1)]
    #println(a)
    return 5
    plot(1:s, a)
    xlabel("Rank")
    ylabel("Frobenius Norm")
    suptitle("Rank k Approximation Error For iRef14 RWR Kernel, r=" * string(r))
    savefig("iref14_rwr_rank_approx_" * string(r) * ".pdf")
    xscale("log")
    savefig("iref14_rwr_rank_approx_" * string(r) * "_log.pdf")
    clf()
end

function make_rwr_mats(index_file, edge_file, r_list)
    edge_raw = split(edge_file, "/")
    edge_raw = edge_raw[length(edge_raw)]

    gene_to_number = Dict{String, Int}()
    number_to_gene = Dict{Int, String}()
    process_index_file_new(index_file, gene_to_number, number_to_gene)    
    A = load_edge_file(edge_file)

    for i=1:length(r_list)
        println(r_list[i])
        PPR = make_rwr_mat(A, r_list[i])
        save(edge_raw * "_rwr_mat_" * string(r_list[i]) * ".jld", "M", PPR)
    end
end


# to get data for plotting rank vs. degree
function degree_data()
    index_file = "networks/iref14_index_genes"
    edge_file = "networks/iref14_edge_list"

    gene_to_number = Dict{String, Int}()
    number_to_gene = Dict{Int, String}()
    process_index_file_new(index_file, gene_to_number, number_to_gene)    
    A = load_edge_file(edge_file)
    degree_list = sum(A, 1)

    monogenic_dict = read_candidates("disease_gene_data/monogenic_data.txt", gene_to_number)
    polygenic_dict = read_candidates("disease_gene_data/polygenic_data.txt", gene_to_number)
    cancer_dict = read_candidates("disease_gene_data/cancer_data.txt", gene_to_number)


    # r = 0.36
    # beta = .000003162
    # PPR = make_rwr_mat(A, r)
    # K = make_dk_mat(A, beta)

    K=5
    try 
        K = load("iref14_edge_list_dk_mat_0.0015.jld", "K")
    catch
        K = make_dk_mat(A, .0015)
        save("iref14_edge_list_dk_mat_0.0015.jld", "K", K)
    end

    PPR = 5
    try
        PPR = load("iref14_edge_list_rwr_mat_0.35.jld", "M")
    catch
        PPR = make_rwr_mat(A, 0.35)
        save("iref14_edge_list_rwr_mat_0.35.jld", "M", PPR)
    end

    kernel_list = Matrix[]
    push!(kernel_list, PPR)
    push!(kernel_list, K)
    println("mongenic")
    r1 = dict_results_degree(monogenic_dict, gene_to_number, kernel_list, degree_list)
    println(r1)
    println("polygenic")
    r2 = dict_results_degree(polygenic_dict, gene_to_number, kernel_list, degree_list)
    println(r2)
    println("cancer")
    r3 = dict_results_degree(cancer_dict, gene_to_number, kernel_list, degree_list)
    println(r3)


end

#@time main()

function degree_scores()
    index_file = "networks/iref14_index_genes"
    edge_file = "networks/iref14_edge_list"

    gene_to_number = Dict{String, Int}()
    number_to_gene = Dict{Int, String}()
    process_index_file_new(index_file, gene_to_number, number_to_gene)    
    A = load_edge_file(edge_file)

    degs = sum(A,1)

    monogenic_dict = read_candidates("disease_gene_data/monogenic_data.txt", gene_to_number)
    polygenic_dict = read_candidates("disease_gene_data/polygenic_data.txt", gene_to_number)
    cancer_dict = read_candidates("disease_gene_data/cancer_data.txt", gene_to_number)

    c=dict_results_deg(cancer_dict, gene_to_number, degs)
    m=dict_results_deg(monogenic_dict, gene_to_number, degs)
    p=dict_results_deg(polygenic_dict, gene_to_number, degs)

    println(c)
    println(p)
    return -1

    include_100 = false

    reg_avgs_c = get_average(c, include_100, false)
    reg_avgs_m = get_average(m, include_100, false)
    reg_avgs_p = get_average(p, include_100, false)

    println("cancer: " * string(reg_avgs_c))
    println("monogenic: " * string(reg_avgs_m))
    println("polygenic: " * string(reg_avgs_p))
end


#########################################################################################################

# take normal disease dict and remove candidates
# thus gene_dict will map disease --> (list of disease genes in network)
# after this, use remove_small_lists to filter
function remove_candidates(x_dict, gene_to_num)
    new_dict = Dict()
    for disease in keys(x_dict)
        genes = collect(keys(x_dict[disease]))
        new_genes = String[]
        for i in range(1, size(genes,1))
            g = genes[i]
            if haskey(gene_to_num, genes[i])
                push!(new_genes, genes[i])
            end
        end
        new_dict[disease] = new_genes
    end
    return new_dict
end

function remove_candidates_no_filt(x_dict, gene_to_num)
    new_dict = Dict()
    for disease in keys(x_dict)
        genes = collect(keys(x_dict[disease]))
        new_dict[disease] = genes
    end
    return new_dict
end

function get_duplicates(m,p,c)
    gene_list = Set()
    duplicates = Set()

    for g_l in values(m)
        for gene in g_l
            if !in(gene, gene_list)
                push!(gene_list, gene)
            else
                push!(duplicates, gene)
            end
        end
    end

    for g_l in values(p)
        for gene in g_l
            if !in(gene, gene_list)
                push!(gene_list, gene)
            else
                push!(duplicates, gene)
            end
        end
    end

    for g_l in values(c)
        for gene in g_l
            if !in(gene, gene_list)
                push!(gene_list, gene)
            else
                push!(duplicates, gene)
            end
        end
    end

    return duplicates
end

function duplicates_rank()
    index_file = "networks/iref14_index_genes"
    edge_file = "networks/iref14_edge_list"

    gene_to_number = Dict()
    number_to_gene = Dict()
    process_index_file_new(index_file, gene_to_number, number_to_gene)    
    A = load_edge_file(edge_file)

    degs = sum(A,1)[:]

    kernel_list = Matrix[]

    PPR = load("iref14_edge_list_rwr_mat_0.35.jld", "M")
    push!(kernel_list, PPR)

    K = load("iref14_edge_list_dk_mat_0.0015.jld", "K")
    push!(kernel_list, K)

    monogenic_dict = read_candidates("disease_gene_data/monogenic_data.txt", gene_to_number)
    polygenic_dict = read_candidates("disease_gene_data/polygenic_data.txt", gene_to_number)
    cancer_dict = read_candidates("disease_gene_data/cancer_data.txt", gene_to_number)

    m_dis = remove_candidates(monogenic_dict, gene_to_number)
    p_dis = remove_candidates(polygenic_dict, gene_to_number)
    c_dis = remove_candidates(cancer_dict, gene_to_number)

    duplicates = get_duplicates(m_dis, p_dis, c_dis)

    duplicates_rank_map = Dict() # maps gene --> [(rank, dis), ...]
    rank_map = Dict()

    for dis in keys(monogenic_dict)
        dis_genes = monogenic_dict[dis]
        dis_members = Int[]
        for g in keys(dis_genes)
            if haskey(gene_to_number, g)
                push!(dis_members, gene_to_number[g])
            end
        end

        for gene in keys(dis_genes)
            if haskey(gene_to_number, gene)
                to_push = 5
                if in(gene, duplicates)
                    to_push = duplicates_rank_map
                else
                    to_push = rank_map
                end


                gene_num = gene_to_number[gene]
                score=kernel_score(kernel_list, dis_members, dis_genes[gene], gene_num)
                if haskey(to_push, gene)
                    push!(to_push[gene], score)
                else
                    to_push[gene] = Any[score]
                end
            end
        end
    end

    for dis in keys(cancer_dict)
        dis_genes = cancer_dict[dis]
        dis_members = Int[]
        for g in keys(dis_genes)
            if haskey(gene_to_number, g)
                push!(dis_members, gene_to_number[g])
            end
        end

        for gene in keys(dis_genes)
            if haskey(gene_to_number, gene)
                to_push = 5
                if in(gene, duplicates)
                    to_push = duplicates_rank_map
                else
                    to_push = rank_map
                end


                gene_num = gene_to_number[gene]
                score=kernel_score(kernel_list, dis_members, dis_genes[gene], gene_num)
                if haskey(to_push, gene)
                    push!(to_push[gene], score)
                else
                    to_push[gene] = Any[score]
                end
            end
        end
    end

    for dis in keys(polygenic_dict)
        dis_genes = polygenic_dict[dis]
        dis_members = Int[]
        for g in keys(dis_genes)
            if haskey(gene_to_number, g)
                push!(dis_members, gene_to_number[g])
            end
        end

        for gene in keys(dis_genes)
            if haskey(gene_to_number, gene)
                to_push = 5
                if in(gene, duplicates)
                    to_push = duplicates_rank_map
                else
                    to_push = rank_map
                end


                gene_num = gene_to_number[gene]
                score=kernel_score(kernel_list, dis_members, dis_genes[gene], gene_num)
                if haskey(to_push, gene)
                    push!(to_push[gene], score)
                else
                    to_push[gene] = Any[score]
                end
            end
        end
    end

    #lets add degrees too
    for i in keys(duplicates_rank_map)
        inum = gene_to_number[i]
        deg = degs[inum]
        push!(duplicates_rank_map[i], deg)
    end

    for i in keys(rank_map)
        inum = gene_to_number[i]
        deg = degs[inum]
        push!(rank_map[i], deg)
    end

    println(duplicates_rank_map)
    println(rank_map)

end

function duplicates_deg()
    index_file = "networks/iref14_index_genes"
    edge_file = "networks/iref14_edge_list"

    gene_to_number = Dict()
    number_to_gene = Dict()
    process_index_file_new(index_file, gene_to_number, number_to_gene)    
    A = load_edge_file(edge_file)

    monogenic_dict = read_candidates("disease_gene_data/monogenic_data.txt", gene_to_number)
    polygenic_dict = read_candidates("disease_gene_data/polygenic_data.txt", gene_to_number)
    cancer_dict = read_candidates("disease_gene_data/cancer_data.txt", gene_to_number)

    m_dis = remove_candidates(monogenic_dict, gene_to_number)
    p_dis = remove_candidates(polygenic_dict, gene_to_number)
    c_dis = remove_candidates(cancer_dict, gene_to_number)

    duplicates = get_duplicates(m_dis, p_dis, c_dis)

    degrees = Any[]

    for gene in duplicates
        gene_num = gene_to_number[gene]
        gene_deg = sum(A[gene_num, :])
        push!(degrees, (gene, gene_deg))
    end

    println(degrees)

end

#duplicates_rank()
#duplicates_deg()

function dk_sensitivity()

    index_file = "networks/iref14_index_genes"
    edge_file = "networks/iref14_edge_list"

    gene_to_number = Dict()
    number_to_gene = Dict()
    process_index_file_new(index_file, gene_to_number, number_to_gene)    
    A = load_edge_file(edge_file)

    #cancer_dict = read_candidates("disease_gene_data/cancer_data.txt", gene_to_number)
    poly_dict = read_candidates("disease_gene_data/polygenic_data.txt", gene_to_number)

    mono_dict = read_candidates("disease_gene_data/monogenic_data.txt", gene_to_number)
    # new_mono = Dict()
    # mono_keys = collect(keys(mono_dict))
    # keys_ind = 1:7:83
    # for i in keys_ind
    #     k = mono_keys[i]
    #     new_mono[k] = mono_dict[k]
    # end
    # mono_dict = new_mono

    l = logspace(0, -3, 100)
    p_enrich_scores = zeros(100)
    p_reg_scores = zeros(100)

    m_enrich_scores = zeros(100)
    m_reg_scores = zeros(100)

    for ind in 1:length(l)
        beta = l[ind]
        println(beta)
        K = make_dk_mat(A, beta)

        p_results = dict_results(poly_dict, gene_to_number, Array[K])
        m_results = dict_results(mono_dict, gene_to_number, Array[K])

        include_100=false
        p_enrich_avgs = get_average(p_results, include_100, true) # enrichment averages
        p_reg_avgs = get_average(p_results, include_100, false) # rank averages

        m_enrich_avgs = get_average(m_results, include_100, true) # enrichment averages
        m_reg_avgs = get_average(m_results, include_100, false) # rank averages

        p_enrich_scores[ind] = p_enrich_avgs[1]
        p_reg_scores[ind] = p_reg_avgs[1]

        m_enrich_scores[ind] = m_enrich_avgs[1]
        m_reg_scores[ind] = m_reg_avgs[1]

    end
    #println(l)
    println(p_enrich_scores)
    println(p_reg_scores)
    println(m_enrich_scores)
    println(m_reg_scores)
end

function rwr_sensitivity()

    index_file = "networks/iref14_index_genes"
    edge_file = "networks/iref14_edge_list"

    gene_to_number = Dict()
    number_to_gene = Dict()
    process_index_file_new(index_file, gene_to_number, number_to_gene)    
    A = load_edge_file(edge_file)

    #cancer_dict = read_candidates("disease_gene_data/cancer_data.txt", gene_to_number)
    poly_dict = read_candidates("disease_gene_data/polygenic_data.txt", gene_to_number)

    mono_dict = read_candidates("disease_gene_data/monogenic_data.txt", gene_to_number)
    # new_mono = Dict()
    # mono_keys = collect(keys(mono_dict))
    # keys_ind = 1:7:83
    # for i in keys_ind
    #     k = mono_keys[i]
    #     new_mono[k] = mono_dict[k]
    # end
    # mono_dict = new_mono

    l = linspace(0,0.99,201)[2:end]
    p_enrich_scores = zeros(200)
    p_reg_scores = zeros(200)

    m_enrich_scores = zeros(200)
    m_reg_scores = zeros(200)

    for ind in 1:length(l)
        r=l[ind]
        println(r)
        PPR = make_rwr_mat(A, r)

        p_results = dict_results(poly_dict, gene_to_number, Array[PPR])
        m_results = dict_results(mono_dict, gene_to_number, Array[PPR])

        include_100=false
        p_enrich_avgs = get_average(p_results, include_100, true) # enrichment averages
        p_reg_avgs = get_average(p_results, include_100, false) # rank averages

        m_enrich_avgs = get_average(m_results, include_100, true) # enrichment averages
        m_reg_avgs = get_average(m_results, include_100, false) # rank averages

        p_enrich_scores[ind] = p_enrich_avgs[1]
        p_reg_scores[ind] = p_reg_avgs[1]

        m_enrich_scores[ind] = m_enrich_avgs[1]
        m_reg_scores[ind] = m_reg_avgs[1]

    end
    #println(l)
    println(p_enrich_scores)
    println(p_reg_scores)
    println(m_enrich_scores)
    println(m_reg_scores)
end

#dk_sensitivity()
#rwr_sensitivity()

function dk_gen()

    index_file = "networks/iref14_index_genes"
    edge_file = "networks/iref14_edge_list"

    gene_to_number = Dict()
    number_to_gene = Dict()
    process_index_file_new(index_file, gene_to_number, number_to_gene)    
    A = load_edge_file(edge_file)

    beta_list = [0.01, 0.05, 0.1, 0.5, 1]
    for beta in beta_list
        println(beta)
        K = make_dk_mat(A, beta)
        save("iref14_edge_list_dk_mat_" * string(beta) * ".jld", "K", K)
    end
end

# function deg_result_changes()

#     p_old={"Alzheimer Disease"=>[1 1 1 1],"Essential hypertension"=>[1 24 23 1 16 15 21 12 86 1 1 7],"Inflammatory Bowel Disease"=>[9 13 7 56 100 16 73 3],"Pheochromocytoma"=>[2 100 24 5 13 56],"Graves disease"=>[5 5 8 38 61],"Non-Insulin-Dependent Diabetes Mellitus"=>[67 8 37 36 100 95 100 5 9 1 45 17 1 25 2 25 44 2 21],"Systemic lupus erythematodes"=>[1 8 57 58 19 100 6],"Mycobacterium tuberculosis, susceptibility to"=>[16 47 25 100 76 10],"Obesity"=>[1 15 18 1 51 34 2 100 3 2 1 18 1],"Age-Related Macular Degeneration"=>[39 54 6 11 13 59 9 2 100 98 2 16],"Rheumatoid arthiritis"=>[75 28 2 5 7 38 18],"Maturity-Onset Diabetes of the Young (MODY)"=>[100 10 10 100 6 90 100 51 68]}
#     m_old={"Cataract, autosomal dominant"=>[1 1 2 1 3 2 86 1 100 62 2],"Hirschsprung Disease"=>[19 17 3 9 1 1],"Stickler syndrome"=>[1 2 1 36],"Charcot Marie Tooth Disease"=>[1 64 26 7 34 5 21 4 28 4 6 1 1 1 100 29 10 23 24 2 24],"Holoprosencephaly"=>[52 1 1 2 1 3],"Xeroderma pigmentosum"=>[2 1 1 1 18 2 1 1],"Peters anomaly"=>[1 2 1 42],"Nemaline myopathy"=>[3 1 12 2 4 2],"Fanconi anemia"=>[1 1 1 1 1 1 1 1 1 1 1 1],"Myoclonic dystonia"=>[29 70 37],"Epidermolysis bullosa"=>[3 1 1 1 1 1 1 1 1 1 1 1],"Bare lymphocyte syndrome type II"=>[1 1 1 1],"Juvenile myoclonic epilepsy"=>[23 48 7 12 37],"Cholestasis"=>[43 1 3],"Limb-Girdle Muscle Dystrophy"=>[6 12 84 1 1 1 1 2 1 1 1 100 1 1],"Cornelia de Lange syndrome"=>[1 1 1],"Hypercholesterolemia, familial"=>[1 1 2],"Bare lymphocyte syndrome type I"=>[3 3 3],"Progressive external ophthalmoplegia"=>[2 1 100 1],"Fundus albipunctatus"=>[90 1 1],"Elliptocytosis"=>[1 1 2 1],"Joubert syndrome"=>[33 1 1 13],"Nephronophthisis, hereditary"=>[1 1 1 1],"Familial hyperinsulinemic hypoglycemia"=>[4 9 2 15 29 2],"Arthrogryposis"=>[2 4 2 2],"Refsum disease"=>[1 1 1 1 1],"Primary open-angle glaucoma"=>[6 14 18],"Hypokalemic periodic paralysis"=>[74 61 59],"Spondylocostal dysostosis"=>[100 100 67],"Dilated cardiomyopathy"=>[40 41 5 10 58 22 1 17 28 2 2 1 2 59 61 4 7 3 1 1],"Chondrodysplasia punctata"=>[52 1 35 2 1 1 32 1 1 1 1 5 1 1],"Distal hereditary motor neuronopathy"=>[5 33 25 1 1 4 95],"Microphthalmia"=>[29 39 54 22 47 6 5 20 43],"Leigh Syndrome"=>[2 100 8 24 100 100 100 100 3 4 28 100 15 100 100 60 63 2 1 100 100 16 18 100],"Noonan Syndrome, Costello syndrome, Cardiofaciocutaneous Syndrome"=>[1 1 3 1 2 4 1 1],"Retinitis pigmentosa"=>[38 39 1 88 25 90 1 69 30 3 33 47 100 26 31 100 1 83 1 40 100],"Multiple epiphyseal dysplasia AD"=>[1 1 1 1 1],"Night-blindness, congenital stationary"=>[66 1 100 54 67 1 27],"Bile-acid synthesis defect, congenital"=>[44 30 100 100],"Hemochromatosis"=>[100 1 1 1 1],"Waardenburg syndrome"=>[1 1 1 1 1 5],"Hypertrophic cardiomyopathy"=>[1 15 10 1 9 1 1 64 100 1 1 100 19 11 33],"Spastic paraplegia"=>[15 63 67 15 7 9],"Ectodermal dysplasia"=>[1 1 1],"Kartagener syndrome"=>[100 78 32],"Amyloidosis VI"=>[13 2 1],"Maple-syrup urine disease"=>[13 1 1 4],"Hemophagocytic lymphohistiocytosis"=>[1 17 11 1],"Leukoencephalopathy with vanishing white matter"=>[1 1 1 1 1],"Hermansky-Pudlak syndrome"=>[2 100 3 3 1 1 28 1],"Spinocerebellar Ataxia"=>[2 16 3 6 24 10 24 100 48 1 1 53],"Aicardi-Goutieres syndrome"=>[65 31 65 31],"Bardet-Biedl Syndrome"=>[1 2 4 1 1 1 1 1 1 2 1 1 1],"Ehlers Danlos syndrome"=>[9 2 75 2 1 2 73 13 7],"Amyotrophic lateral sclerosis"=>[13 17 27 32 23 4 25 43],"Primary microcephaly"=>[13 12 27 23],"Nonsyndromic hearing loss"=>[1 2 88 100 80 100 50 100 100 3 41 100 3 2 100 43 15 25 100 2 1 1 100 1 45 13 3 1 38 1 24 100 100 2 66 69 60 45 35 100 8 38],"Cutis laxa"=>[6 1 1 1],"Familial exudative vitreoretinopathy"=>[64 100 18],"Long QT Syndrome"=>[9 12 2 2 1 10 2 60 1],"Combined oxidative phosphorylation deficiency"=>[15 7 1 18],"Congenital myasthenic syndromes"=>[1 1 1 2 1],"Achromatopsia"=>[26 100 60],"Congenital central hypoventilation syndrome"=>[33 62 2 30 9 79],"Hyper-IgM syndrome"=>[1 9 1 6],"Cerebrooculofacioskeletal syndrome"=>[1 4 2 1],"Kallmann syndrome"=>[1 1 100 100],"Neuronal ceroid lipofuscinosis"=>[1 31 13 9 3 9 100],"Polycystic kidney disease"=>[1 1 61],"Pseudohypoaldosteronism, type I, autosomal recessive"=>[2 1 2],"Multiple Acyl-CoA Dehydrogenase deficiency"=>[1 1 24],"Adrenoleukodystrophy"=>[1 1 1 1 1],"Pituitary dwarfism"=>[1 2 82 1],"Generalized epilepsy with febrile seizures plus"=>[79 42 63 45],"Brachydactyly"=>[1 18 1 22 29],"Arrhythmogenic right ventricular dysplasia (ARVD)"=>[2 70 1 2 1 18],"Mitochondrial complex I deficiency disorders"=>[15 1 100 100 3 100 23 1 13 100 19 100 100 11 100 15],"Nonbullous congenital ichthyosiform erythroderma"=>[1 47 2 2],"Severe congenital neutropenia"=>[10 5 100],"Keratosis palmoplantaris striata"=>[4 3 1],"Osteopetrosis"=>[65 1 36 53 1],"Pulmonary surfactant metabolism dysfunction"=>[54 70 62],"Atypical mycobacteriosis, familial"=>[1 2 1 2 1 1]}
#     c_old={"Prostate cancer"=>[11 2 56 1 9 1 17 21 11 2 4 21 22],"Esophageal carcinoma"=>[2 100 38 11 60 3 12 36 5 6],"Bladder Cancer"=>[1 1 17 3],"Lung cancer"=>[1 59 8 3 6],"Glioma of brain, familial"=>[33 1 2 4 17 11 5 2 1 5 33 17 2 45 1 91 3 25 73 7 2],"Medulloblastoma"=>[9 25 3 21 81 1],"Juvenile myelomonocytic leukemia"=>[26 1 1 10 2],"Breast cancer familial"=>[7 10 30 1 1 1 52 3 3 1 1 1 12 1 1],"Hepataocellular carcinoma"=>[2 3 1],"Pancreatic carcinoma"=>[1 7 5 5 100 6],"Thyroid carcinoma, papillary"=>[9 2 5 17 4 3 26 4],"Hereditary nonpolyposis colorectal cancer"=>[2 2 2 2 2 1 1]}

#     p_new={"Alzheimer Disease"=>[1 1 6 1],"Essential hypertension"=>[1 22 4 1 18 15 43 1 88 1 1 9],"Inflammatory Bowel Disease"=>[2 12 2 52 100 18 80 2],"Pheochromocytoma"=>[9 100 17 5 17 56],"Graves disease"=>[4 2 2 35 35],"Non-Insulin-Dependent Diabetes Mellitus"=>[52 16 37 36 100 81 100 6 10 1 39 7 1 30 1 2 45 2 2],"Systemic lupus erythematodes"=>[2 8 22 45 14 100 2],"Mycobacterium tuberculosis, susceptibility to"=>[8 31 19 100 42 5],"Obesity"=>[1 11 16 1 39 25 3 100 6 2 1 30 1],"Age-Related Macular Degeneration"=>[55 39 3 8 3 59 2 2 100 98 2 6],"Rheumatoid arthiritis"=>[77 21 1 1 14 5 5],"Maturity-Onset Diabetes of the Young (MODY)"=>[100 4 8 100 2 89 100 48 62]}
#     m_new={"Cataract, autosomal dominant"=>[1 1 2 1 3 2 83 1 100 56 2],"Hirschsprung Disease"=>[1 18 1 1 1 1],"Stickler syndrome"=>[1 1 1 22],"Charcot Marie Tooth Disease"=>[1 57 27 8 44 5 26 6 34 15 3 1 1 1 100 32 16 23 20 1 29],"Holoprosencephaly"=>[28 1 1 2 1 2],"Xeroderma pigmentosum"=>[1 1 1 1 8 2 1 1],"Peters anomaly"=>[1 1 1 55],"Nemaline myopathy"=>[3 1 9 6 1 3],"Fanconi anemia"=>[1 1 1 1 1 2 2 1 1 1 1 1],"Myoclonic dystonia"=>[29 38 51],"Epidermolysis bullosa"=>[2 2 1 1 1 1 1 1 1 1 1 1],"Bare lymphocyte syndrome type II"=>[1 1 1 1],"Juvenile myoclonic epilepsy"=>[29 35 7 4 24],"Cholestasis"=>[52 1 1],"Limb-Girdle Muscle Dystrophy"=>[8 1 97 1 1 1 1 4 1 1 1 100 1 1],"Cornelia de Lange syndrome"=>[1 1 1],"Hypercholesterolemia, familial"=>[1 1 2],"Bare lymphocyte syndrome type I"=>[3 3 3],"Progressive external ophthalmoplegia"=>[1 1 100 1],"Fundus albipunctatus"=>[88 1 1],"Elliptocytosis"=>[1 1 1 1],"Joubert syndrome"=>[48 1 1 25],"Nephronophthisis, hereditary"=>[1 1 1 1],"Familial hyperinsulinemic hypoglycemia"=>[1 10 2 21 13 2],"Arthrogryposis"=>[1 1 2 2],"Refsum disease"=>[1 1 1 1 1],"Primary open-angle glaucoma"=>[12 18 16],"Hypokalemic periodic paralysis"=>[38 36 47],"Spondylocostal dysostosis"=>[100 100 67],"Dilated cardiomyopathy"=>[21 35 5 1 38 5 1 6 11 5 2 1 5 28 60 3 2 2 1 1],"Chondrodysplasia punctata"=>[49 1 36 2 1 1 42 1 1 1 1 2 1 1],"Distal hereditary motor neuronopathy"=>[4 49 34 1 3 15 96],"Microphthalmia"=>[24 50 50 1 55 8 4 2 10],"Leigh Syndrome"=>[1 100 10 28 100 100 100 100 2 6 33 100 3 100 100 69 75 1 2 100 100 28 5 100],"Noonan Syndrome, Costello syndrome, Cardiofaciocutaneous Syndrome"=>[3 1 3 1 1 7 1 4],"Retinitis pigmentosa"=>[33 38 1 81 28 70 1 1 3 3 31 1 100 4 45 100 1 81 1 45 100],"Multiple epiphyseal dysplasia AD"=>[1 1 1 1 1],"Night-blindness, congenital stationary"=>[67 1 100 10 18 1 18],"Bile-acid synthesis defect, congenital"=>[42 43 100 100],"Hemochromatosis"=>[100 1 1 1 1],"Waardenburg syndrome"=>[1 1 1 1 1 6],"Hypertrophic cardiomyopathy"=>[2 16 4 1 19 1 1 70 100 1 1 100 3 3 18],"Spastic paraplegia"=>[3 18 77 10 3 2],"Ectodermal dysplasia"=>[1 1 1],"Kartagener syndrome"=>[100 79 32],"Amyloidosis VI"=>[18 3 2],"Maple-syrup urine disease"=>[11 1 1 2],"Hemophagocytic lymphohistiocytosis"=>[1 7 1 1],"Leukoencephalopathy with vanishing white matter"=>[1 1 1 1 1],"Hermansky-Pudlak syndrome"=>[1 100 2 2 1 1 21 1],"Spinocerebellar Ataxia"=>[13 17 8 4 34 10 26 100 80 4 1 44],"Aicardi-Goutieres syndrome"=>[78 38 77 37],"Bardet-Biedl Syndrome"=>[1 2 3 1 1 1 1 1 1 2 1 1 1],"Ehlers Danlos syndrome"=>[2 2 67 1 1 1 68 15 2],"Amyotrophic lateral sclerosis"=>[14 26 36 36 8 6 19 29],"Primary microcephaly"=>[21 11 24 7],"Nonsyndromic hearing loss"=>[1 1 87 100 61 100 59 100 100 4 43 100 3 2 100 42 16 34 100 2 1 1 100 1 1 6 4 1 48 1 20 100 100 1 70 49 57 46 31 100 6 31],"Cutis laxa"=>[3 1 1 1],"Familial exudative vitreoretinopathy"=>[38 100 4],"Long QT Syndrome"=>[2 10 2 2 1 2 1 17 1],"Combined oxidative phosphorylation deficiency"=>[6 15 2 10],"Congenital myasthenic syndromes"=>[1 1 1 2 1],"Achromatopsia"=>[50 100 64],"Congenital central hypoventilation syndrome"=>[42 45 1 6 1 66],"Hyper-IgM syndrome"=>[1 26 1 8],"Cerebrooculofacioskeletal syndrome"=>[1 2 2 1],"Kallmann syndrome"=>[1 1 100 100],"Neuronal ceroid lipofuscinosis"=>[1 24 9 11 3 25 100],"Polycystic kidney disease"=>[1 1 60],"Pseudohypoaldosteronism, type I, autosomal recessive"=>[2 1 2],"Multiple Acyl-CoA Dehydrogenase deficiency"=>[1 1 33],"Adrenoleukodystrophy"=>[1 1 1 1 1],"Pituitary dwarfism"=>[1 2 61 1],"Generalized epilepsy with febrile seizures plus"=>[78 31 50 28],"Brachydactyly"=>[1 4 1 22 16],"Arrhythmogenic right ventricular dysplasia (ARVD)"=>[2 51 1 2 1 29],"Mitochondrial complex I deficiency disorders"=>[22 1 100 100 2 100 34 1 4 100 22 100 100 16 100 7],"Nonbullous congenital ichthyosiform erythroderma"=>[1 63 2 2],"Severe congenital neutropenia"=>[20 17 100],"Keratosis palmoplantaris striata"=>[4 4 1],"Osteopetrosis"=>[70 1 21 69 1],"Pulmonary surfactant metabolism dysfunction"=>[54 82 62],"Atypical mycobacteriosis, familial"=>[1 8 1 2 1 1]}
#     c_new={"Prostate cancer"=>[14 3 48 6 2 3 22 25 6 1 5 25 17],"Esophageal carcinoma"=>[11 100 51 1 52 4 8 38 3 17],"Bladder Cancer"=>[3 1 22 3],"Lung cancer"=>[13 58 3 5 2],"Glioma of brain, familial"=>[35 3 8 8 11 12 3 2 4 4 36 11 1 47 14 90 6 18 68 5 1],"Medulloblastoma"=>[12 32 5 36 77 6],"Juvenile myelomonocytic leukemia"=>[23 1 1 7 6],"Breast cancer familial"=>[1 1 29 1 2 1 61 13 4 2 1 1 5 3 10],"Hepataocellular carcinoma"=>[14 8 8],"Pancreatic carcinoma"=>[9 5 4 2 100 1],"Thyroid carcinoma, papillary"=>[10 2 19 9 22 2 19 4],"Hereditary nonpolyposis colorectal cancer"=>[2 2 3 2 3 1 1]}

#     index_file = "networks/iref14_index_genes"
#     edge_file = "networks/iref14_edge_list"

#     gene_to_number = Dict()
#     number_to_gene = Dict()
#     process_index_file_new(index_file, gene_to_number, number_to_gene)    
#     A = load_edge_file(edge_file)
#     degs = sum(A,1)

#     m_dict = read_candidates("disease_gene_data/monogenic_data.txt", gene_to_number)
#     p_dict = read_candidates("disease_gene_data/polygenic_data.txt", gene_to_number)
#     c_dict = read_candidates("disease_gene_data/cancer_data.txt", gene_to_number)

#     m=remove_candidates_no_filt(m_dict, gene_to_number)
#     p=remove_candidates_no_filt(p_dict, gene_to_number)
#     c=remove_candidates_no_filt(c_dict, gene_to_number)

#     up_deg = Int[] #degree of everything whose score went up
#     down_deg = Int[] #degree of everything whose score went down

#     for dis in keys(p_old)
#         old_scores = p_old[dis]
#         new_scores = p_new[dis]

#         #check which ones are positive and which are negative
#         for i=1:length(old_scores)

#             println(dis)
#             println(p[dis])
#             println(i)
#             gene = p[dis][i]
#             if haskey(gene_to_number, gene)
#                 gene_num = gene_to_number[gene]
#                 gene_deg = degs[gene_num]

#                 diff = new_scores[i] - old_scores[i]

#                 if diff > 0
#                     push!(up_deg, gene_deg)
#                 elseif diff < 0
#                     push!(down_deg, gene_deg)
#                 end
#             end
#         end
#     end

#     for dis in keys(m_old)
#         old_scores = m_old[dis]
#         new_scores = m_new[dis]

#         #check which ones are positive and which are negative
#         for i=1:length(old_scores)

#             gene = m[dis][i]
#             if haskey(gene_to_number, gene)
#                 gene_num = gene_to_number[gene]
#                 gene_deg = degs[gene_num]

#                 diff = new_scores[i] - old_scores[i]

#                 if diff > 0
#                     push!(up_deg, gene_deg)
#                 elseif diff < 0
#                     push!(down_deg, gene_deg)
#                 end
#             end
#         end
#     end

#     for dis in keys(c_old)
#         old_scores = c_old[dis]
#         new_scores = c_new[dis]

#         #check which ones are positive and which are negative
#         for i=1:length(old_scores)

#             gene = c[dis][i]
#             if haskey(gene_to_number, gene)
#                 gene_num = gene_to_number[gene]
#                 gene_deg = degs[gene_num]

#                 diff = new_scores[i] - old_scores[i]

#                 if diff > 0
#                     push!(up_deg, gene_deg)
#                 elseif diff < 0
#                     push!(down_deg, gene_deg)
#                 end
#             end
#         end
#     end

#     println(down_deg)
#     println(up_deg)

#     #[16,22,7,3,37,4,5,28,26,98,13,9,3,14,4,2,3,4,91,14,4,3,5,4,14,10,21,98,12,44,57,88,2,6,6,2,43,6,7,15,23,77,28,16,16,14,77,16,3,5,6,2,4,10,50,5,3,5,9,16,32,361,8,27,17,22,38,15,37,23,38,6,5,10,3,14,56,4,16,114,5,22,12,19,10,10,6,11,11,28,5,5,15,10,6,1,13,35,109,4,2,16,25,4,3,335,1,9,21,19,5,26,5,62,15,1,5,4,4,9,3,1,4,6,6,1,15,12,15,9,18,17,14,26,32,38,3,11,14,60,7,6,3,10,18,95,13,63,2,3,44,12,2,6,28,11,6,55,13,20,9,1,1,2,4,56,3,3,7,4,2,17,5,7,11,12,24,22,14,28,12,12,7,9,50,17,5,5,20,6,30,2,8,4,4,4,6,4,7,13,19,5,5,19,9,46,17,33,10,56,3,69,186,2,62,57,26,54,41,19,53,2,23,2,170,165,5,11,26,70,75,10,24,265,57,62,186,12,65,13]
#     #[2009,30,4,4,114,17,1,417,17,77,106,25,6,5,23,239,36,3,2,3,12,24,18,43,4,15,20,4,206,12,89,12,2,75,66,13,75,4,54,10,3,63,2,206,4,14,27,8,99,44,55,206,8,6,10,15,20,73,1,1,2,30,12,6,22,6,3,2,76,4,67,148,111,4,3,4,1,1,19,18,23,6,2,2,1,10,8,2009,167,30,263,1,53,1,38,1,1,1,15,25,45,6,9,15,73,39,1,104,4,9,8,42,4,3,2,8,62,4,1,2,12,61,28,5,2,22,5,6,9,4,4,20,63,57,11,3,1,409,22,144,107,57,16,5,91,42,865,2,170,5,240,258,49,865,863,24,50,863,107,141,57,13,9,865,91,57,9,170,31,362,148,530,2,110,13,865,91,326,865,121,362,865,51,75,116,165,69]

# end
#deg_result_changes()

function add_dict_to_dict(main, to_add)
    for k in keys(to_add)
        if main[k] == Any[]
            main[k] = to_add[k]
        else
            main[k] = [main[k]; to_add[k]]
        end
    end
end

#gets data on rankings of each gene in each disease for a range of beta values
function optimal_beta()
    index_file = "networks/iref14_index_genes"
    edge_file = "networks/iref14_edge_list"

    gene_to_number = Dict()
    number_to_gene = Dict()
    process_index_file_new(index_file, gene_to_number, number_to_gene)    
    A = load_edge_file(edge_file)

    poly_dict = read_candidates("disease_gene_data/polygenic_data.txt", gene_to_number)
    mono_dict = read_candidates("disease_gene_data/monogenic_data.txt", gene_to_number)
    cancer_dict = read_candidates("disease_gene_data/cancer_data.txt", gene_to_number)

    poly_results = Dict()
    for k in keys(poly_dict)
        poly_results[k] = Any[]
    end

    mono_results = Dict()
    for k in keys(mono_dict)
        mono_results[k] = Any[]
    end

    cancer_results = Dict()
    for k in keys(cancer_dict)
        cancer_results[k] = Any[]
    end

    #l = logspace(-1.4, -0.3, 50)
    #l = logspace(-3, -0.3, 50)
    #l=logspace(-6, 0, 61)
    l = linspace(0.01, 0.99, 99)

    for ind in 1:length(l)
        beta = l[ind]
        println(beta)
        K = make_dk_mat(A, beta)

        c_results = dict_results(cancer_dict, gene_to_number, Array[K])
        p_results = dict_results(poly_dict, gene_to_number, Array[K])
        m_results = dict_results(mono_dict, gene_to_number, Array[K])

        add_dict_to_dict(poly_results, p_results)
        add_dict_to_dict(mono_results, m_results)
        add_dict_to_dict(cancer_results, c_results)

    end
    return mono_results, poly_results, cancer_results
end

function optimal_r()
    index_file = "networks/iref14_index_genes"
    edge_file = "networks/iref14_edge_list"

    gene_to_number = Dict()
    number_to_gene = Dict()
    process_index_file_new(index_file, gene_to_number, number_to_gene)    
    A = load_edge_file(edge_file)

    poly_dict = read_candidates("disease_gene_data/polygenic_data.txt", gene_to_number)
    mono_dict = read_candidates("disease_gene_data/monogenic_data.txt", gene_to_number)
    cancer_dict = read_candidates("disease_gene_data/cancer_data.txt", gene_to_number)

    poly_results = Dict()
    for k in keys(poly_dict)
        poly_results[k] = Any[]
    end

    mono_results = Dict()
    for k in keys(mono_dict)
        mono_results[k] = Any[]
    end

    cancer_results = Dict()
    for k in keys(cancer_dict)
        cancer_results[k] = Any[]
    end

    l=linspace(0.01, .99, 99)

    for ind in 1:length(l)
        r = l[ind]
        println(r)
        K = make_rwr_mat(A, r)

        c_results = dict_results(cancer_dict, gene_to_number, Array[K])
        p_results = dict_results(poly_dict, gene_to_number, Array[K])
        m_results = dict_results(mono_dict, gene_to_number, Array[K])

        add_dict_to_dict(poly_results, p_results)
        add_dict_to_dict(mono_results, m_results)
        add_dict_to_dict(cancer_results, c_results)

    end
    return mono_results, poly_results, cancer_results
end
#x=optimal_beta()
#println(x)