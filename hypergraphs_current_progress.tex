\documentclass[a4paper,11pt]{article}

\usepackage[uheader=true, ulibertine=true, uthesis=true, uboxthm=true, usectnum=true]{uchitra_1}
\linespread{1.2}


\usepackage{mathtools}
\usepackage{subcaption}
\usepackage{float}
\usepackage{mathrsfs}
\usepackage{bbm}
\usepackage{dsfont}

\usepackage{bm}

\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\makeatletter
\def\BState{\State\hskip-\ALG@thistlm}
\makeatother

\DeclareCaptionFormat{algor}{%
  \hrulefill\par\offinterlineskip\vskip1pt%
    \textbf{#1#2}#3\offinterlineskip\hrulefill}
\DeclareCaptionStyle{algori}{singlelinecheck=off,format=algor,labelsep=space}
\captionsetup[algorithm]{style=algori}

\DeclareMathOperator{\score}{score}

% tikz stuff
\usepackage{tikz}

\tikzstyle{vertex} = [fill,shape=circle,node distance=80pt]
\tikzstyle{edge} = [fill,opacity=.5,fill opacity=.5,line cap=round, line join=round, line width=50pt]
\tikzstyle{elabel} =  [fill,shape=circle,node distance=30pt]

\pgfdeclarelayer{background}
\pgfsetlayers{background,main}

\newlength{\drop}% for my convenience
\newcommand*{\plogo}{\includegraphics[scale=0.5]{hypergraphs_brown_logo.pdf}}

\title{Random Walks on Hypergraphs with Applications to Disease-Gene Prioritization}
\author{Uthsav Chitra \\ Advisor: Prof. Ben Raphael}
\begin{document}

In the first two sections, I copy some material from my thesis. The third section contains all my results.

\tableofcontents

\section{Hypergraphs}

\begin{defnbox}
Let $V$ be a (finite) set, and let $E \subset 2^V$, with $|e| > 1$ for all $e \in E$. We call $H=(V, E)$ a \textbf{hypergraph} with vertices $V$ and hyperedges $E$. We call a hypergraph a \textbf{simple graph} if $|e|=2$ for all $e \in E$. 

Moreover, for each vertex $u \in V$, we define $N(u) \overset{\Delta}{=} \{e \in E : u \in e\}$, or equivalently, the set of hyperedges incident to $u$.
\end{defnbox}

Hypergraphs are a generalization of simple graphs, where edges can connect more than two vertices.
  
%\begin{figure}[H]
%\begin{center}
%\includegraphics[scale=0.6]{hyper_example}
%\end{center}
%\end{figure}
%
%Or also
\begin{center}
\begin{tikzpicture}
\node[vertex,label=above:\(v_1\)] (v1) {};
\node[vertex,right of=v1,label=above:\(v_2\)] (v2) {};
\node[vertex,right of=v2,label=above:\(v_3\)] (v3) {};
\node[vertex,below of=v1,label=above:\(v_4\)] (v4) {};
\node[vertex,right of=v4,label=above:\(v_5\)] (v5) {};
\node[vertex,right of=v5,label=above:\(v_6\)] (v6) {};

\begin{pgfonlayer}{background}
\draw[edge,color=purple] (v4) -- (v1) -- (v4);
\draw[edge,color=yellow] (v1) -- (v2) -- (v3);
\begin{scope}[transparency group,opacity=.5]
\draw[edge,opacity=1,color=green] (v3) -- (v5) -- (v6) -- (v3);
\fill[edge,opacity=1,color=green] (v3.center) -- (v5.center) -- (v6.center) -- (v3.center);
\end{scope}
\draw[edge,color=red,line width=40pt] (v2) -- (v3);
\end{pgfonlayer}
%\node[elabel,color=yellow,label=right:\(e_1\)]  (e1) at (-3,0) {};
%\node[elabel,below of=e1,color=red,label=right:\(e_2\)]  (e2) {};
%\node[elabel,below of=e2,color=green,label=right:\(e_3\)]  (e3) {};
%\node[elabel,below of=e3,color=purple,label=right:\(e_4\)]  (e4) {};
\end{tikzpicture}
\end{center}
    
The following two definitions are motivated by our goal of mapping hypergraphs to simple graphs and vice-versa.

\begin{defnbox}
Let $H=(V,E)$ be a hypergraph. We define the \textbf{clique graph} $G(H)$ as a simple graph on $V$, with an edge between $u, v \in V$ if there is a hyperedge $e \in E$ with $u, v \in e$.
\end{defnbox}

The name clique graph comes from the fact that, if $e=\{v_1, ..., v_n\}$ is a hyperedge in $H$, then $v_1, ..., v_n$ form a clique in $G$.

\begin{center}
\begin{tikzpicture}[
    tlabel/.style={pos=0.4,right=-1pt},
    baseline=(current bounding box.center)
    ]
\node[vertex] (v1) at (0,0) {};
\node[vertex] (v2) at (2,0) {};
\node[vertex] (v3) at (2,2) {};
\node[vertex] (v4) at (0,2) {};

\node[vertex] (w1) at (8,0) {};
\node[vertex] (w2) at (10,0) {};
\node[vertex] (w3) at (10,2) {};
\node[vertex] (w4) at (8,2) {};

\node (arrow) at (5.5,1) {$\xRightarrow{\hspace*{1.5cm}}$};

\begin{pgfonlayer}{background}
\begin{scope}[transparency group,opacity=.5]
\draw[edge,opacity=1,color=green] (v1) -- (v2) -- (v3) -- (v4) -- (v1);
\fill[edge,opacity=1,color=green] (v1.center) -- (v2.center) -- (v3.center) -- (v4.center) -- (v1.center);
\end{scope}
\end{pgfonlayer}

\draw (w1) -- (w2) -- (w3) -- (w4) -- (w1);
\draw (w1) -- (w3);
\draw (w2) -- (w4);

\node (l1) at (1,-1.5) {$H$};
\node (l2) at (9,-1.5) {$G(H)$};

\end{tikzpicture}
\end{center}

\section{Random Walks}

In this section, and in the rest of the thesis, assume all hypergraphs are undirected.

\begin{defnbox}
A \textbf{random walk} on a hypergraph $H$ is a time-homogeneous Markov chain with states $V$ and transition probabilities $p_{u,v} = \BP(v_t = v \mid v_{t-1}=u)$.
\end{defnbox}

What are the transition probabilities $p_{u,v}$? Intuitively, at vertex $v_t$, we can think of the random walk as doing the following.
\begin{itemize}
\item
With probability distribution $\pi$, pick a hyperedge $e$ incident to $v_t$.
\item
Then, with probability distribution $\gamma$, pick a vertex $v_{t+1} \in e$.
\end{itemize}

To specify $\pi, \gamma$, we propose the following model: each hyperedge $e$ has weight $\omega(e) > 0$, and each vertex $v$ in a hyperedge $e$ has weight $\lambda_e(v)$. Going back to our gene interaction network example, if we let hyperedges denote protein complexes, then the hyperedge weights express the general importance of a protein complex, while the vertex weights represent the relative importance of genes in a protein complex.

Then, $\pi$ is the distribution on $N(v_t)$ with $\pi(e) \propto \omega(e)$, and $\gamma$ is the distribution with $\gamma(v) \propto \lambda_e(v)$. It would be natural to write that $\gamma$ is a distribution on vertices in $e$. However, that would make it so that $v_{t+1}=v_t$ has positive probability, an event which does not happen in a simple random walk on a simple graph. To fix this issue, we define two types of random walks on a hypergraph $H$. In the \emph{simple random walk}, $\gamma$ is a distribution on $e \setminus \{v_t\}$, while in the \emph{lazy random walk}, $\gamma$ is instead a distribution on $e$.

\begin{exmp}

Consider the following hypergraph.

\begin{center}
\begin{tikzpicture}[
    tlabel/.style={pos=0.4,right=-1pt},
    baseline=(current bounding box.center)
    ]
\node[vertex,label=above:\(b\)] (v1) at (0,0) {};
\node[vertex,label=above:\(a\)] (v2) at (1.87,1) {};
\node[vertex,label=below:\(c\)] (v3) at (1.87,-1) {};
\node[vertex,label=above:\(d\)] (v4) at (3.74,0) {};

\begin{pgfonlayer}{background}
\begin{scope}[transparency group,opacity=.5]
\draw[edge,opacity=1,color=orange] (v1) -- (v2) -- (v3) -- (v1);
\fill[edge,opacity=1,color=orange] (v1.center) -- (v2.center) -- (v3.center) -- (v1.center);
\draw[edge,color=blue] (v2) -- (v3) -- (v4) -- (v2);
\fill[edge,color=blue] (v2.center) -- (v3.center) -- (v4.center) -- (v3.center);
\end{scope}
\end{pgfonlayer}

\node (e1) at (0.1,-1.6) {\large $\color{orange} \bm{e_1}$};
\node (e2) at (3.64,-1.6) {\large $\color{blue} \bm{e_2}$};

\end{tikzpicture}
\end{center}
Suppose the hyperedge weights are $\omega(e_1)=1$ and $\omega(e_2)=2$, and the vertex weights are
\begin{align*}
\big( \lambda_{e_1}(a), \lambda_{e_1}(b), \lambda_{e_1}(c) \big) &= (1,1,1), \\
\big( \lambda_{e_2}(a), \lambda_{e_2}(c), \lambda_{e_2}(d) \big) &= (2,1,1).
\end{align*}
Then, in the simple random walk, the probability of going from $c$ to $a$ in one step is
\begin{equation*}
\begin{split}
p_{c,a} &= \sum_{i=1,2} \BP(\text{start from $c$, pick $e_i$}) \cdot \BP(\text{pick $a$ from $e_i$}) \\[5pt]
&= \left( \frac{\omega(e_1)}{\omega(e_1) + \omega(e_2)} \right) \left( \frac{\lambda_{e_1}(a)}{\lambda_{e_1}(a) + \lambda_{e_1}(b)} \right) + \left( \frac{\omega(e_2)}{\omega(e_1) + \omega(e_2)} \right) \left( \frac{\lambda_{e_2}(a)}{\lambda_{e_2}(a) + \lambda_{e_2}(d)} \right) \\[5pt]
&= \frac{1}{3} \cdot \frac{1}{2} + \frac{2}{3} \cdot \frac{2}{3} \\[5pt]
&= \frac{11}{18},
\end{split}
\end{equation*}
while in the lazy random walk, the probability of going from $c$ to $a$ in one step is
\begin{equation*}
\begin{split}
p_{c,a} &= \sum_{i=1,2} \BP(\text{start from $c$, pick $e_i$}) \cdot \BP(\text{pick $a$ from $e_i$}) \\[5pt]
&= \left( \frac{\omega(e_1)}{\omega(e_1) + \omega(e_2)} \right) \left( \frac{\lambda_{e_1}(a)}{\lambda_{e_1}(a) + \lambda_{e_1}(b) + \lambda_{e_1}(c)} \right) \\[5pt]
&\hspace{1cm} + \left( \frac{\omega(e_2)}{\omega(e_1) + \omega(e_2)} \right) \left( \frac{\lambda_{e_2}(a)}{\lambda_{e_2}(a) + \lambda_{e_2}(c) + \lambda_{e_2}(d)} \right) \\[5pt]
&= \frac{1}{3} \cdot \frac{1}{3} + \frac{2}{3} \cdot \frac{2}{4} \\[5pt]
&= \frac{4}{9}.
\end{split}
\end{equation*}

In general, the simple and lazy random walks do not have identical transition probabilities. Another important point is that each vertex $v$ has multiple vertex weights $\lambda_e(v)$, depending on which hyperedge $e$ we are looking at. For example, $a$ and $c$ have the same weight in $e_1$, while $a$ has twice the weight of $c$ in $e_2$.

\end{exmp}

We can formally define the simple and lazy random walks via their transition probabilities. For example, the simple random walk has transition probabilities
\begin{equation*}
\begin{split}
p_{u,v} &= \sum_{e \in E} \big(\BP(\text{start at $u$, pick hyperedge $e$}) \cdot \mathbb{1}\{v \in e\} \cdot \BP(\text{pick $v$ from hyperedge $e$})\big) \\
&= \sum_{e \in E} \left[ \left( \frac{\omega(e) \cdot \mathbb{1}\{u \in e\}}{\sum_{f \in N(u)} \omega(f)} \right) \cdot \mathbb{1}\{v \in e\} \cdot \left( \frac{\lambda_e(v)\cdot\mathbb{1}(u\neq v)}{\sum_{\substack{z \in e \\ z\neq u}} \lambda_e(z)} \right) \right] \\[5pt]
&=\sum_{e \in E} \left[ \frac{\omega(e)}{\sum_{f \in N(u)} \omega(f)} \cdot \mathbb{1}(u, v \in e) \cdot \frac{\lambda_e(v)\cdot\mathbb{1}(u\neq v)}{\sum_{\substack{z \in e \\ z\neq u}} \lambda_e(z)}\right].
\end{split}
\end{equation*}

We summarize this discussion in the definition below.

\begin{defnbox}
The \textbf{simple random walk} on an undirected hypergraph $H$ is a time-homogeneous Markov chain, with states $V$ and transition probabilities
\begin{equation*}
p_{u, v} = \sum_{e \in E} \left[ \frac{\omega(e)}{\sum_{f \in N(u)} \omega(f)} \cdot \mathbb{1}(u, v \in e) \cdot \frac{\lambda_e(v)\cdot\mathbb{1}(u\neq v)}{\sum_{\substack{z \in e \\ z\neq u}} \lambda_e(z)}\right].
\end{equation*}

The \textbf{lazy random walk} on an undirected hypergraph $H$ is also a time-homogeneous Markov chain on $V$, with transition probabilities
\begin{equation*}
p_{u, v} = \sum_{e \in E} \left[ \frac{\omega(e)}{\sum_{f \in N(u)} \omega(f)} \cdot \mathbb{1}(u, v \in e) \cdot \frac{\lambda_e(v)}{\sum_{z \in e} \lambda_e(z)}\right].
\end{equation*}
\end{defnbox}

Below we define two particular choices of weights.

\begin{itemize}
\item
In the \textbf{uniform edge-weighted simple random walk on $H$} (resp. \textbf{uniform edge-weighted lazy random walk on $H$}), $\omega(e)=|e|-1$ (resp. $\omega(e)=|e|$) and $\lambda_e(v)=1$, for all hyperedges $e$ and vertices $v \in e$.
\item
In the \textbf{unweighted (simple/lazy) random walk on $H$}, $\omega(e)=1$ and $\lambda_e(v)=1$ for all hyperedges $e$ and vertices $v \in e$.
\end{itemize}

\section{Results}

In all theorems and discussions, assume $H$ is connected.

\subsection{Previous Results}

Our current goal is to find the stationary distribution of a (either simple or lazy) random walk on a hypergraph. First, recall the following two results from my thesis.

\begin{theorem}
\label{simple_db}
Let $H$ be a hypergraph with hyperedge weights $\omega(e)$ and vertex weights $\lambda_e(v)$, such that $\lambda_e(v)=\lambda_e(w)$ for all $v, w \in e$. Then, there exist weights $w_{u,v}$ on $G(H)$ such that the \textbf{simple} random walk on $H$ and the weighted random walk on $G(H)$ are equivalent, and the weights are given by
\begin{equation}
\label{simple_db_edge_weights}
w_{u,v} = \sum_{e \in N(u) \cap N(v)} \frac{\omega(e)}{|e|-1}.
\end{equation}
\end{theorem}

\begin{theorem}
\label{lazy_db}
Let $H$ be a hypergraph with hyperedge weights $\omega(e)$ and vertex weights $\lambda_e(v)$. Suppose that, for each hyperedge $e$ and vertex $v \in e$, $\lambda_e(v)$ is independent of $e$. Then, there exist weights $w_{u,v}$ on $G(H)$ such that the \textbf{lazy} random walk on $H$ and the weighted random walk on $G(H)$ are equivalent.
\end{theorem}

Now, suppose hypergraph $H$, with edge weights $\omega(e)$, satisfies the conditions of Theorem \ref{simple_db}. Then, the stationary distribution $\pi$ of a simple random walk on $H$ is the stationary distribution of a simple random walk on $G(H)$ with edge weights given by equation \eqref{simple_db_edge_weights}:
\begin{equation}
\pi_u \propto \sum_v w_{u,v} = \sum_v \left( \sum_{e \in N(u) \cap N(v)} \frac{\omega(e)}{|e|-1} \right).
\end{equation}

Similarly, suppose hypergraph $H$, with edge weights $\omega(e)$ and vertex weights $\lambda_e(v)$, satisfies the conditions of Theorem \ref{lazy_db}. Then, it's not too hard to show that the stationary distribution $\pi$ of a lazy random walk on $H$ is given by
\begin{equation}
\label{stat_dist_lazy_db}
\pi_u \propto \sum_{e \in N(u)} \omega(e) \lambda_e(v).
\end{equation}

\subsection{New Result}

Below, we give a (somewhat) explicit formula for the stationary distribution of a lazy random walk on a hypergraph $H$ with arbitrary vertex weights.

\newpage

\begin{theorem}
\label{lazy_stat_dist}
Let $H=(V,E)$ be a hypergraph with hyperedge weights $\omega(e)$ and vertex weights $\lambda_e(v)$. Without loss of generality, suppose the vertex weights are normalized, i.e. $\sum_{v \in e} \lambda_e(v)=1$ for each hyperedge $e$. For each vertex $v$, let 
\begin{equation}
a_v = \frac{1}{\sum_{e \in N(v)} \omega(e)}.
\end{equation}
Then, the stationary distribution $\pi$ of a lazy random walk on $H$ is given by
\begin{equation}
\label{stat_dist_lazy_thm}
\pi_x = \sum_{e \in N(x)} \rho_e\cdot \omega(e) \cdot \lambda_e(x),
\end{equation}
where $\rho_e$ are positive constants, indexed by hyperedges $e$, satisfying
\begin{equation}
\label{rho_constr1}
\rho_e = \sum_{y \in e} \sum_{f \in N(y)} a_y \cdot \rho_f \cdot \omega(f)\cdot \lambda_f(y)
\end{equation}
and
\begin{equation}
\label{rho_constr2}
\sum_e \rho_e \cdot \omega(e) = 1.
\end{equation}
\end{theorem}

In other words, the stationary distribution looks \textit{almost} like equation \eqref{stat_dist_lazy_db}. The difference is that each hyperedge needs to have specific vertex weight values, i.e. $\rho_e \lambda_e(v)$.

We prove part of Theorem \ref{lazy_stat_dist} in the following lemma.

\begin{lem}
Using the notation of Theorem \ref{lazy_stat_dist}, there exist positive constants $\rho_e$ satisfying equations \eqref{rho_constr1} and \eqref{rho_constr2}.
\end{lem}

\begin{proof}
We first prove the lemma in the case where the hyperedge weights are trivial, i.e. $\omega(e)$ is independent of $e$. Without loss of generality, let $\omega(e)=1$ for all hyperedges $e$.  By switching the order of summation, we have
\begin{equation}
\begin{split}
\sum_{y \in e} \sum_{f \in N(y)} a_y \cdot \rho_f \cdot \lambda_f(y) &= \sum_{\substack{\text{edges } \\ f}} \sum_{y \in e \cap f} a_y \cdot \rho_f \cdot \lambda_f(y) \\
&=  \sum_{f} \rho_f \cdot \left( \sum_{y \in e \cap f} a_y \cdot\lambda_f(y)\right).
\end{split}
\end{equation}
Now let $A$ be a square matrix of size $|E| \times |E|$, with entries
\begin{equation}
\label{a_mat}
A_{e,f} = \sum_{y \in e \cap f} a_y \cdot\lambda_f(y).
\end{equation}
Finding $\rho_e$ satisfying \eqref{rho_constr1} is equivalent to showing there exists a positive eigenvector $v$ of $A$ with eigenvalue $1$. We have
\begin{equation}
\begin{split}
\sum_e A_{e,f} &= \sum_e \sum_{y \in e \cap f} a_y \cdot\lambda_f(y) \\
&= \sum_{y \in f} \sum_{e \in N(y)} a_y \cdot\lambda_f(y) \\
&= \sum_{y \in f} a_y \cdot \lambda_f(y) \cdot |N(y)| \\
&= \sum_{y \in f} \lambda_f(y), \text{ since $a_y = \frac{1}{|N(y)|}$}, \\
&= 1,
\end{split}
\end{equation}

so the sum of each column of $A$ is $1$. Since $H$ is connected, $A$ is irreducible, so by the Perron-Frobenius theorem, $A$ has a positive eigenvector $v$ with eigenvalue $1$. Normalizing $v$, we can make $\rho_e$ satisfy \eqref{rho_constr2} as well.

Next, suppose the hyperedge weights are rational, i.e. $\omega(e) \in \BQ$ for all $e$. Multiplying through by denominators, assume $\omega(e) \in \BN$. Now, create hypergraph $H'$ in the following way:
\begin{itemize}
\item
For each hyperedge $e$, replace $e$ with hyperedges $e_1, ..., e_{\omega(e)}$, where:
\begin{itemize}
\item
each $e_i$ contains the same vertices as $e$, 
\item
each $e_i$ has weight $\omega(e_i)=1$, and
\item 
each $e_i$ has the same vertex weights as $e$, so that $\lambda_{e_i}(v) = \lambda_e (v)$ for all $v \in e$.
\end{itemize}
\end{itemize}

Let $E'$ be the edges of $H'$, and let $M(y)$ be the hyperedges incident to vertex $y$ in $H'$. By the first part of the proof, we can find constants $\rho_{e_i}$ that satisfy equations \eqref{rho_constr1} and \eqref{rho_constr2} for $H'$. Note that, by symmetry, $\rho_{e_i} = \rho_{e_j}$ for $i, j \in \{1, 2, ..., \omega(e)\}$.

Now, for each hyperedge $e$ in $H$, let $\sigma_e = \rho_{e_i}$. I claim that $\sigma_e$ solves equations \eqref{rho_constr1} and \eqref{rho_constr2} for $H$. It is easy to see that equation \eqref{rho_constr2} is satisfied, since
\begin{equation}
\begin{split}
\omega(e) \cdot \sigma_e &= \omega(e) \cdot \rho_{e_1} \\
&= \rho_{e_1} + \dots + \rho_{e_{\omega(e)}} \\
&= \rho_{e_1} \omega(e_1) + \dots + \rho_{e_{\omega(e)}} \omega(e_{\omega(e)}) \\
&= \sum_{i=1}^{\omega(e)} \rho_{e_i} \omega(e_i),
\end{split}
\end{equation}

which implies
\begin{equation}
\sum_{e \in E} \sigma_e \cdot \omega(e) = \sum_{e \in E} \sum_{i=1}^{\omega(e)} \rho_{e_i} \omega(e_i) = \sum_{e \in E'} \rho_{e_i} \omega(e_i) = 1.
\end{equation}

For the system of equations \eqref{rho_constr1} (in $H$), consider a single term
\begin{equation}
a_y \cdot \sigma_f \cdot \omega(f)\cdot \lambda_f(y).
\end{equation}

Note that, in both $H$ and $H'$, $a_y$ is the same for each vertex $y$. Now, let $e, f$ be hyperedges in $H$, and let $y \in e \cap f$. Then,
\begin{equation}
\begin{split}
a_y \cdot \sigma_f \cdot \omega(f)\cdot \lambda_f(y) &= a_y \cdot \sigma_f \sum_{i=1}^{\omega(f)} \lambda_{f_i}(y) \\
&= a_y \sum_{i=1}^{\omega(f)} \big(\rho_{f_i} \cdot \omega(f_i)\cdot \lambda_{f_i}(y)\big) \\
&= \sum_{i=1}^{\omega(f)} a_y \cdot \rho_{f_i} \cdot \omega(f_i)\cdot \lambda_{f_i}(y).
\end{split}
\end{equation}

Thus,
\begin{equation}
\begin{split}
\sum_{y \in e} \sum_{f \in N(y)} a_y \cdot \sigma_f \cdot \omega(f)\cdot \lambda_f(y) &= \sum_{y \in e} \sum_{f \in N(y)} \sum_{i=1}^{\omega(f)} a_y \cdot \rho_{f_i} \cdot \omega(f_i)\cdot \lambda_{f_i}(y) \\
&= \sum_{y \in e} \sum_{f \in M(y)} a_y \cdot \rho_{f} \cdot \omega(f)\cdot \lambda_{f}(y) \\
&= \sum_{y \in e_1} \sum_{f \in M(y)} a_y \cdot \rho_{f} \cdot \omega(f)\cdot \lambda_{f}(y) \\
&= \rho_{e_1} \\
&= \sigma_e.
\end{split}
\end{equation}

It follows that $\sigma_e$ solves equations \eqref{rho_constr1} and \eqref{rho_constr2}, completing the case where the hyperedge weights are rational.

%ok what follows is incredibly poorly written

Finally, consider the general case, where we assume nothing about hyperedge weights $\omega(e)$. Like before, we have
\begin{equation}
\begin{split}
\sum_{y \in e} \sum_{f \in N(y)} a_y \cdot \rho_f \cdot \omega(f) \cdot \lambda_f(y) &= \sum_{\substack{\text{edges } \\ f}} \sum_{y \in e \cap f} a_y \cdot \rho_f \cdot \omega(f) \cdot \lambda_f(y) \\
&=  \sum_{f} \rho_f \cdot \left( \sum_{y \in e \cap f} a_y \cdot\omega(f) \cdot \lambda_f(y)\right).
\end{split}
\end{equation}

Let $A$ be a matrix of size $|E| \times |E|$ with entries
\begin{equation}
\label{A_eq}
A_{e,f} = \sum_{y \in e \cap f} a_y \cdot\omega(f) \cdot \lambda_f(y).
\end{equation}

Again, showing the existence of positive $\rho_e$ is equivalent to showing that $A$ has a positive eigenvector with eigenvalue $1$. By the Perron-Frobenius theorem, this is equivalent to $A$ having spectral radius $1$.

Let $w=(\omega(e_1), \dots, \omega(e_n))$ be the hyperedge weights of $H$, where $n=|E|$. Let $w_i = (q_{i,1}, ..., q_{i,n})$, where $q_{i,j} \in \BQ$ and $\lim_{i\to\infty} q_{i,j} = \omega(e_i)$, and let $H_i$ be $H$ with weights $w_i$. 

By the previous part of the proof, there exist positive constants $\rho_{e_i, j}$ that, for each fixed $j$, satisfy equation \eqref{rho_constr1} for $H_i$. Equivalently, letting $A_i$ be the matrix from equation \eqref{A_eq}, then $A_i$ has spectral radius $1$. Now, $A_i$ depends continuously on the weights $w_i$. Furthermore, the spectral radius is a continuous function. It follows that the spectral radius of $A$ is the limit of the spectral radius of $A_i$. Thus, the spectral radius of $A$ is $1$, and by the Perron-Frobenius theorem, it follows that we can find positive $\rho_e$ satisfying equation \eqref{rho_constr1}. Normalizing, we can also make $\rho_e$ satisfy equation \eqref{rho_constr2}, completing the proof.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{proof}[Proof of Theorem \ref{lazy_stat_dist}]

From the lemma, we can find positive $\rho_e$ that satisfy equations \eqref{rho_constr1} and \eqref{rho_constr2}. So it suffices to show that the distribution $\pi$ from equation \ref{stat_dist_lazy_thm} is indeed the stationary distribution of $P$, the probability transition matrix of a lazy random walk on $H$. Indeed, we have

\begin{equation}
\begin{split}
\sum_y \pi_y P_{y,x} &= \sum_y \pi_y \left( \sum_{f \in N(y) \cap N(x)} \frac{1}{|N(y)|} \cdot \lambda_f(x) \right) \\[5pt]
&= \sum_y \sum_{f \in N(y) \cap N(x)} \pi_y a_y \lambda_f(x) \\[5pt]
&= \sum_{\substack{\text{edges } \\ f}} \sum_{y \in f} \pi_y a_y \lambda_f(x), \text{ switching the order of summation}, \\
&= \sum_f \lambda_f(x) \left( \sum_{y \in f} a_y \pi_y \right) \\[5pt]
&= \sum_f \lambda_f(x) \rho_e, \text{ plugging in equation \eqref{rho_constr1}}, \\[5pt]
&= \pi_x,
\end{split}
\end{equation}

completing the proof.
\end{proof}

\end{document}