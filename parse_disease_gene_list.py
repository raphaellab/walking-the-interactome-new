from bs4 import BeautifulSoup
from urllib.request import urlopen
from pprint import pprint
import pickle

soup = BeautifulSoup(open("ajhg111mmc1-2.html"))

#########################################################################################
# CAT_DICT AND NUM_TO_DISEASE DICT
# cat_dict : maps 0,1,2 (0=mono, 1=poly, 2=cancer) to list of diseases
# ex. cat_dict[1] = [2, 3, 5, ...]

# num_to_disease lets you decode this, as it maps a number to the disease name
# ex. num_to_disease[105] = 'Spondylocostal dysostosis'
#########################################################################################

per_category_list = soup.find_all("ul")
cat_dict = {}
num_to_disease = {}
for i in range(3):
    cat_html = per_category_list[i]
    li_list = cat_html.find_all("li")
    cat = []
    for j in range(len(li_list)):
        name = li_list[j]
        name_split = name.text.split(' ')
        num = name_split[0]
        num = num[0:len(num)-1]
        cat.append(int(num))

        real_name = name.a.text
        num_to_disease[int(num)] = real_name
    cat_dict[i]=cat


########################################################################################
# DISEASE_DICT
# disease_dict maps numbers (indicating the disease) to list of known genes
# ex. disease_dict[108] ['CCDC6', 'TIF1G', 'TPR', 'PRKAR1A', 'NTRK1', 'NCOA4', 'TPM3', 'RET']
########################################################################################

disease_dict = {}
raw_disease_list = soup.find_all("table", width="80%")
for k in range(len(raw_disease_list)):
    k_disease = raw_disease_list[k]
    zoom_in = k_disease.find("table", border="1")
    tr_list = zoom_in.find_all("tr")
    tr_list = tr_list[2:]
    k_genes = []
    for m in tr_list:
        m_td = m.find("td")
        m_td_text = m_td.text.strip()
        # need to clean two special cases:
        if m_td_text == 'PRPH2 (RDS)':
            m_td_text = 'PRPH2'
        elif m_td_text == 'ORP1 (aka RP1)':
            m_td_text = 'RP1'
        #now for the general case
        if len(m_td_text) > 0: 
            k_genes.append(m_td_text)
    disease_dict[k+1] = list(set(k_genes))

########################################################################################
# GET GENOMIC NEIGHBORS
# ITT: code very sensitive to HTML changes
# it's also sensitive to the cookies in your browser, for what the search returns
# SO USE AT YOUR OWN RISK. 

# Advice: visit URL_BASE + "TAP1" first. then change the search settings
# to find 200 closest genes, use the latest assembly, sort by gene distance, etc.
# only after you do that should you run this.
########################################################################################

URL_BASE = "http://genome.ucsc.edu/cgi-bin/hgNear?hgsid=431307333_fuAXm7v3guaYJ0aNjUTUxRcAZxMP&near_search="

# this is horrifying
# anyway, given a gene name (ex. "TAP1"), gets the 100 genes closest
def get_candidates(gene):
    #print("currently at: " + gene) # just to track in the terminal how much is done

    html = urlopen(URL_BASE + gene).read()
    gene_soup = BeautifulSoup(html, "lxml")

    #in this case, the site gives a warning that the gene cannot be found.
    scripts = gene_soup.find_all("script")
    to_examine = scripts[len(scripts)-2]
    if 'warnList' in str(to_examine):
        return (gene, [])

    # otherwise, we have two cases: 
    # CASE 1: the website makes us select the gene we're looking for
    # CASE 2: we're directly given the site with the genomic neighbors
    # we can tell which case we're in based on the title variable below
    title = gene_soup.find("div", id="sectTtl").text.strip()

    # CASE 1
    if title == 'Simple Search Results':
        #need to click the link that takes us to the gene we want
        x=gene_soup.find("table", bgcolor="#FFFEE8", width="100%", border="0", cellspacing="0", cellpadding="0")
        x=x.find("table", bgcolor="#FFFEE8", width="100%", cellpadding="0")

        all_a = x.find_all("a") #possible links

        #now we can look through each link
        for n in range(len(all_a)):
            name = all_a[n].next_element #name of search entry
            side_text = name.next_element
            split_side_text = side_text.split(' ')

            # does the text next to the link give an alternate name?
            if "(aka" in split_side_text:
                aka_index = split_side_text.index("(aka")
                aka_name = split_side_text[aka_index + 1]
                aka_name = name[0:len(name)-1] #name of (aka BLAH)
            else:
                aka_name = ""

            if name == gene or aka_name == gene:
                real_url = "http://genome.ucsc.edu" + all_a[n]["href"][2:] #the url we want
                ans = extract_candidates(real_url)
                na = ""
                if aka_name != "":
                    na = name + "," + aka_name
                else:
                    na = name
                return (na, ans)
        else:
            # it's not found.
            ans = []
            return (gene, ans)
    else:
        # CASE 2
        ans = extract_candidates(URL_BASE+gene)
        aka_name = ans[0]
        na = gene
        if aka_name != na:
            na = na + "," + aka_name
        return (na, ans)

# helper method for above
# given a URL where the 100 candidates are, this just parses the HTML and gets them
def extract_candidates(url):
    html1 = urlopen(url).read()
    extract_soup = BeautifulSoup(html1, "lxml")
    big_table = extract_soup.find("table", cellspacing="0", cellpadding="1", cols="7")
    row_list = big_table.find_all("tr")
    row_list = row_list[1:]
    raw_neighbors = []

    for i in range(len(row_list)):
        row = row_list[i]
        td_list = row.find_all("td")
        if(len(td_list)>1):
            name = td_list[1].string
            raw_neighbors.append(name)

    #now to filter out the neighbors that we need
    to_return = [];
    for gene in raw_neighbors:
        if len(to_return) > 199:
            break;
        if gene != "\xa0" and " " not in gene:
            to_return.append(gene)

    return to_return

# below: given a gene (say TAP1), gets all alternate names of the gene from
# the UCSC gene sorter. however, it was really slow to use. feel free to use it
# if needed!

# GENE_NAME_HASH = {}

# def get_gene_names(gene):
#     html = urlopen(URL_BASE + gene).read()
#     gene_soup = BeautifulSoup(html, "lxml") #READ DA HTML

#     title = gene_soup.find("div", id="sectTtl").text.strip()

#     if gene in GENE_NAME_HASH:
#         return GENE_NAME_HASH[gene]

#     # CASE 1
#     if title == 'Simple Search Results':
#         #need to click the link that takes us to the gene we want
#         x=gene_soup.find("table", bgcolor="#FFFEE8", width="100%", border="0", cellspacing="0", cellpadding="0")
#         x=x.find("table", bgcolor="#FFFEE8", width="100%", cellpadding="0")

#         all_a = x.find_all("a") #possible links

#         #now we can look through each link
#         for n in range(len(all_a)):
#             name = all_a[n].next_element #name of search entry
#             side_text = name.next_element
#             split_side_text = side_text.split(' ')

#             # does the text next to the link give an alternate name?
#             if "(aka" in split_side_text:
#                 aka_index = split_side_text.index("(aka")
#                 aka_name = split_side_text[aka_index + 1]
#                 aka_name = name[0:len(name)-1] #name of (aka BLAH)
#             else:
#                 aka_name = ""

#             if name == gene or aka_name == gene:
#                 if aka_name == "":
#                     aka_name = name
#                 real_url = "http://genome.ucsc.edu" + all_a[n]["href"][2:] #the url we want
#                 other_name = get_gene_names_helper(real_url)
#                 names = list(set([name, aka_name, other_name]))
#                 to_return = ""
#                 if len(names) > 1:
#                     for n in names:
#                         to_return += n + ","
#                     to_return = to_return[0:len(to_return)-1]
#                 else:
#                     to_return = names[0]
#                 GENE_NAME_HASH[gene] = to_return
#                 return to_return
#         else:
#             # it's not found.
#             GENE_NAME_HASH[gene] = gene
#             return gene
#     else:
#         # CASE 2
#         ans = get_gene_names_helper(URL_BASE+gene)
#         ans = list(set([gene, ans]))
#         to_return = ""
#         if len(ans) > 1:
#             to_return = ans[0]+ "," + ans[1]
#         else:
#             to_return = ans[0]
#         GENE_NAME_HASH[gene] = to_return
#         return to_return

# def get_gene_names_helper(url):
#     html1 = urlopen(url).read()
#     extract_soup = BeautifulSoup(html1, "lxml")
#     big_table = extract_soup.find("table", cellspacing="0", cellpadding="1", cols="7")
#     row_list = big_table.find_all("tr")
#     row_list = row_list[1:]
#     raw_neighbors = []

#     for i in range(len(row_list)):
#         row = row_list[i]
#         td_list = row.find_all("td")
#         if(len(td_list)>1):
#             name = td_list[1].string
#             raw_neighbors.append(name)

#     #only need top name sooo
#     for gene in raw_neighbors:
#         if gene != "\xa0":
#             return gene

# Writing data!
def write_data():
    for q in range(2):
        print(q) #again, for tracking purposes
        if q == 0:
            f = open("monogenic_data.txt", "w")
        elif q==1:
            f = open("polygenic_data.txt", "w")
        elif q==2:
            f = open("cancer_data.txt", "w")
        diseases = cat_dict[q]
        for disease_num in diseases:
            disease_name = num_to_disease[disease_num]
            f.write(disease_name + "\n") #write disease NAME
            print(disease_name)
            disease_genes = disease_dict[disease_num]
            f.write(str(len(disease_genes)) + "\n") #write NUMBER of disease genes
            print(str(len(disease_genes)))
            for gene in disease_genes:
                name, candidates = get_candidates(gene)

                f.write(name + "\n") #write disease gene NAME
                f.write(str(len(candidates)) + "\n") #write NUMBER of candidates
                for cand in candidates:
                    f.write(cand+"\n") #write candidate NAME
    f.close()

write_data()

