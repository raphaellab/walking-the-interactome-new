from bs4 import BeautifulSoup
from urllib.request import urlopen
from pprint import pprint
import pickle

import requests

# layout of file to write to
# family name, number of genes, list of genes
# (for each gene in the list, include all alternate names)


base = 'http://www.genenames.org/cgi-bin/genefamilies/set/'
f = open("gene_families.txt", "w")
# open every possible URL
for i in range(1, 1500):
    print(i)
    url = base + str(i)
    page = requests.get(url)
    soup = BeautifulSoup(page.text) #have text of site

    x = soup.findAll("tr", attrs={"class": "genefam_5col-row-odd"})
    y = soup.findAll("tr", attrs={"class": "genefam_5col-row-even"})

    # tr is list of all entries in table
    tr = x + y
    if len(tr) == 0:
        continue;

    #write how many genes there are in the family
    f.write(str(len(tr)) + "\n")

    for gene in tr:
        gene_td = gene.findAll("td")

        # get all names from the table row
        potential_name1 = gene_td[0].findAll("span")
        if len(potential_name1) > 0:
            name1 = [potential_name1[0].text.strip()]
        else:
            name1 = [gene_td[0].text.strip()]

        name2 = gene_td[2].text
        if name2 == '':
            name2 = []
        else:
            name2 = name2.strip().split(", ")

        other_names = gene_td[3].text
        if other_names == '':
            other_names = []
        else:
            other_names = other_names.split(", ")
        
        all_names = name1 + name2 + other_names

        f.write(','.join(all_names) + "\n")

