###########################################################################

using StatsBase

### building random hypergraph

# random preferential attachment hypergraph? https://arxiv.org/pdf/1502.02401v1.pdf

#below we do (approximately) d-regular hypergraph
# output is matrix whose columns are hyperedges, rows are nodes
function create_rand_pref_d_reg_hypergraph(time, p, d)
    edge_mat = ones(1,1) #n by m
    n=1 #number of nodes
    m=1 #number of edges
    degs = [1]
    for t=1:time
        r = rand()

        # with probability p, add new vertex and make it connect to d-1 others
        if r < p
            #other edges it connects to
            #neighbors= rand(1:n, 1, min(d-1, n))
            # println(Array(1:length(degs)))
            # println(min(d-1, n))
            # println(degs)
            neighbors = sample(Array(1:length(degs)), WeightVec(degs), min(d-1, n))

            edge = zeros(n+1, 1)
            edge[neighbors] = 1
            edge[n+1] = 1

            edge_mat = [edge_mat zeros(n, 1)]
            edge_mat = [edge_mat; zeros(1, m+1)]
            edge_mat[:, m+1] = edge

            #update n, m
            n=n+1
            m=m+1

            #update degs
            for v in neighbors
                degs[v] = degs[v] + 1
            end

            #degs = [degs, min(d-1, n)]
            push!(degs, min(d-1, n))
        else
            #with probability 1-p, select d vertices and make a new edge
            neighbors = rand(1:n, 1, min(d, n))
            edge = zeros(n, 1)
            edge[neighbors] = 1

            edge_mat = [edge_mat edge]
            m=m+1

            #update degs
            for v in neighbors
                degs[v] = degs[v] + 1
            end
        end
    end
    return edge_mat
end

###########################################################################

#H is matrix |V| x |E| matrix, where i,j entry is weight of vertex i in edge j
function construct_lazy_probs_trans_mat(H; edge_weights=false)
    n=size(H,1)
    m=size(H,2)

    e_weights = edge_weights
    if edge_weights == false
        e_weights=ones(1,m)
    end

    P=zeros(n,n)
    for i=1:n
        relevant_edges = H[:, find(H[i, :])]
        #relevant_edges[i, :] = 0 #make sure i cant go to itself

        relevant_e_weights = e_weights[find(H[i, :])]
        normalized_rel_e_weights = relevant_e_weights / sum(relevant_e_weights)


        probs = relevant_edges ./ sum(relevant_edges, 1)
        probs = probs .* (normalized_rel_e_weights') #the ' is an annoying fix
        probs = sum(probs,2)

        P[i,:] = probs'
    end
    return P

end

###########################################################################

# H is |V| \times |E| incidence matrix, where i,j entry is weight of vertex i in edge j
function construct_probs_trans_mat_glob(H; edge_weights=false)
    n=size(H,1)
    m=size(H,2)

    e_weights = edge_weights
    if edge_weights == false
        e_weights=ones(1,m)
    end

    P=zeros(n,n)
    for i=1:n
        relevant_edges = H[:, find(H[i, :])]
        relevant_edges[i, :] = 0 #make sure i cant go to itself

        relevant_e_weights = e_weights[find(H[i, :])]
        normalized_rel_e_weights = relevant_e_weights / sum(relevant_e_weights)


        probs = relevant_edges ./ sum(relevant_edges, 1)
        probs = probs .* (normalized_rel_e_weights') #the ' is an annoying fix
        probs = sum(probs,2)

        P[i,:] = probs'
    end
    return P

end

#below is case of global weights
## H is |V| x |E| incidence matrix
function construct_probs_trans_mat(H; vertex_weights=false, edge_weights=false)
    n = size(H,1)

    if !(vertex_weights)
        vertex_weights = ones(1,n)
    end

    P = zeros(n,n)
    for i=1:n
        probs = zeros(n,1)
        relevant_edges = H[:, find(H[i, :])]
        relevant_edges[i, :] = 0 #make sure i cant go to itself
        num_rel_edges = size(relevant_edges,2)

        for j=1:num_rel_edges
            col = relevant_edges[:,j]
            col = col.*vec(vertex_weights)
            col = col./sum(col)
            if edge_weights != false
                relevant_edge_weights = edge_weights[find(H[i, :])]
                col = col * relevant_edge_weights[j]
            else
                col = col * (1/num_rel_edges)
            end
            probs = probs + col
        end

        if edge_weights != false
            relevant_edge_weights = edge_weights[find(H[i, :])]
            probs = probs ./ sum(relevant_edge_weights)
        else
            probs = probs ./ sum(probs)
        end

        P[i,:]=probs'
    end
    return P
end

function find_stat_dist(e)
    vals=e[:values]
    vecs=e[:vectors]

    m, i = findmax(abs(vals))
    v=vecs[:,i]
    return v ./ sum(v)
end

function edge_stat_dist(e, vertex_weights)
    v=find(e)
    n=length(v)
    P=zeros(n,n)
    for i=1:n
        v_weights = vertex_weights[v]
        v_weights[i]=0
        P[i,:]=(v_weights ./ sum(v_weights))'
    end

    # println(e)
    # println(P)

    # vecs, vals = eig(P')
    # k = find(x -> (x<1.01) & (x > 0.999), vecs)
    # stat = vals[:,k]
    # stat = stat./sum(stat)

    stat = find_stat_dist(eigfact(P'))

    # println("stat")
    # println(stat)

    to_return = zeros(1,length(e))
    for i=1:n
        to_return[v[i]]=stat[i]
    end
    return to_return
end

function test_stat_dist(H, vertex_weights; edge_weights=false)
    P=construct_probs_trans_mat(H, vertex_weights, edge_weights=edge_weights)
    n=length(vertex_weights)

    pi_guess = zeros(1,n)

    for i=1:size(H,2)
        # println("new case")
        # println(i)
        # println(edge_stat_dist(H[:,i], vertex_weights))
        if edge_weights != false
            pi_guess = pi_guess + edge_weights[i]*edge_stat_dist(H[:,i], vertex_weights)
        else
            pi_guess = pi_guess + edge_stat_dist(H[:,i], vertex_weights)
        end
    end
    pi_guess = pi_guess ./ sum(pi_guess)

    # eigs = eigvecs(P')
    # pi = eigs[:,1]
    # pi = pi ./ sum(pi)

    pi = find_stat_dist(eigfact(P'))

    #println(pi_guess)
    println(pi)
    #println(pi_guess/minimum(pi_guess))
    println(pi/minimum(pi))
end

function normalize_stat_dist(p)
    newp = map(x -> real(x), p)
    return newp ./ minimum(newp)
end

#### testing vertex weights

function convert_hyper_to_simple(H)
    n=size(H,1)
    A=zeros(n,n)
    for i=1:n
        relevant_edges = H[:, find(H[i, :])]
        relevant_edges[i, :] = 0 #make sure i cant go to itself
        connections = Int.(sum(relevant_edges,2) .> 0)
        A[:,i]=connections
    end
    return A
end

function stat_dist_vertex_weights(H, v_weights)
    A=convert_hyper_to_simple(H)
    P=zeros(A)
    n=size(P,1)
    for i=1:n
        r = A[i,:] .* vec(v_weights)
        r=r/sum(r)
        P[i,:]=r
    end
    pi = find_stat_dist(eigfact(P'))
    println(pi)
    println(pi/minimum(pi))
end

###########################################################################

### testing detailed balance

function test_detailed_bal(P, stat_dist)
    n=size(P,1)
    for i=1:n
        for j=(i+1):n
            if abs(stat_dist[i]*P[i,j] - stat_dist[j]*P[j,i]) > 1e-6
                return false
            end
        end
    end
    return true
end

# with global vertex weights

# B is bound on vertex weight size
function detailed_bal(H; B=10, trials=100000)
    success=0
    for t=1:trials
        n=size(H,1)
        v_weights=rand(1:B,1,n)

        P=construct_probs_trans_mat(H, v_weights)
        pi=find_stat_dist(eigfact(P'))

        to_return=test_detailed_bal(P, pi)

        if to_return
            success=success+1
            println(v_weights)
        end
    end
    println(success/trials)
    #return to_return
end

# generate random graphs, test if they have detailed balance

function rand_detailed_bal()
    s=5
    e=8
    B=3
    r = zeros(1,e-s+1)
    for n=s:e
        #println(n)
        num_graphs=5
        for c=1:num_graphs
            println(c)
            trials=1000
            success=0
            H=create_rand_pref_d_reg_hypergraph(2*n, 0.5, B)
            H=H[:, find(x -> x > 1, sum(H,1))] #get rid of edges w 1 vertex
            println(H)
            for t=1:trials
                newH = H .* rand(1:B, size(H,1), size(H,2))
                P=construct_probs_trans_mat_glob(newH, edge_weights=false)
                if length(find(x -> (x == Inf) | (x == NaN), P)) > 0
                    println(H)
                    println(P)
                end
                pi=find_stat_dist(eigfact(P'))
                to_return=test_detailed_bal(P, pi)

                if to_return==true
                    success = success+1
                end
            end
            r[n-s+1] = r[n-s+1] + (success/trials)
        end
        r[n-s+1] = r[n-s+1] / num_graphs
    end
    println(r)
    return r
end

######

#construct C_ef matrix

function construct_C(edges, edge_weights)
    num_edges = size(edges,2)
    num_vertices = size(edges,1)

    C=zeros(num_edges, num_edges)

    a_y=zeros(num_vertices,1)
    for v=1:num_vertices
        incident_edges = find(edges[v,:])
        a_y[v,1] = 1/(sum(edge_weights[incident_edges]))
    end

    for i=1:num_edges
        for j=1:num_edges
            intersecting_verts = find(edges[:,i] .* edges[:,j])
            for y in intersecting_verts
                C[i,j] = C[i,j] + (edge_weights[j] * (a_y[y] * edges[y,j]))
            end
        end
    end
    return C
end

######


p=10
#edges = [1 1 p 0; 1 0 1 1]'
#edges = [1 2 3 0; 2 0 1 3]'
#edges = [1 2 3 0 0 0 0; 0 0 4 6 4 0 0; 0 0 0 1 1 3 10]'
#for p=1:5
#p=2

edges=[2 1 3 0; 2 0 1 3]'
#edges2=[2 1 3 0; 2 0 1 3; 2 0 1 3]'
#edges=[1 1 4 0 0 0; 2 0 3 0 0 1; 0 1 1 2 2 0; 0 0 0 3 1 2]'
#edges=edges ./ sum(edges,1)
#edges2=edges2 ./ sum(edges2,1)
#C1=construct_C(edges,[1 1 1 1])
#C2=construct_C(edges,[1 2 1 1])
#C1=construct_C(edges, [1 2])
#C2=construct_C(edges2, [1 1 1])
#println(C1)
#println(C2)
#edges=[1 2 7 0 0 0 0; 0 2 7 5 3 7 0; 1 0 7 0 0 7 5]'
#edges = [1 2 2 0 0 0 0; 2 0 6 4 6 3 0; 0 1 1 0 0 1 1]'
#edges=[1 1 4 0 0 0; 2 0 3 0 0 1; 0 1 1 2 2 0; 0 0 0 3 1 2]'

#P=construct_probs_trans_mat_glob(edges; edge_weights=[1 1]) #pe=.486527; pf=.229042
#pi1=find_stat_dist(eigfact(P'))

P2=construct_lazy_probs_trans_mat(edges; edge_weights=[1 1])
pi2=find_stat_dist(eigfact(P2'))

#P2=construct_probs_trans_mat_glob(edges)
#pi2=find_stat_dist(eigfact(P2'))

# println("new p")
# println(p)
#println(P)


#println(pi1)
#println(pi2)
#println(abs(sum((pi1-pi2).^2)))


#println(normalize_stat_dist(pi1))
#println(normalize_stat_dist(pi2))
#println()
#end

#edges = [1 1 1 0; 0 1 1 1]'
#v_weights=[1 1 5 1]

#edges = [1 1 1 0 0 0; 1 0 1 1 0 0; 0 0 1 1 1 1]'

# edges = [1 1 1 0 0; 1 0 1 1 0; 0 0 1 1 1]'
# p=2
# v_weights=[1 p 1 1 1]

# H=[1 2 3 0 0; 0 1 2 3 0; 0 0 1 2 3]'
# e_weights=[1 1 1]
# #P=construct_probs_trans_mat_glob(H, edge_weights=e_weights)

# rand_detailed_bal()

###

# println("ACTUAL")
# test_stat_dist(edges, v_weights)
# println("GUESS")

# guess_v_weights = [2 p 3 2 1]
# stat_dist_vertex_weights(edges, guess_v_weights)

###

# println("next")
# println(test_stat_dist(edges, [1 1 2 1 1]))
# println("next")
# println(test_stat_dist(edges, [1 2 2 1 1]))


#detailed_bal(edges)