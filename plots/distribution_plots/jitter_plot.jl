using PyPlot

function rand_jitter(arr)
    stdev = .01 * (maximum(arr) - minimum(arr))
    return arr + rand(size(arr,1)) * stdev
end

function remove_100(l)
    l2 = Array(Int, 2, 0)
    for i in range(1, size(l,2))
        if (l[1,i] < 100) & (l[2,i] < 100)
            l2 = [l2 [l[1,i]; l[2,i]]]
        end
    end
    return l2
end

function dict_to_list(raw_dict)
    r = Array(Int, 2, 0)
    for dis in keys(raw_dict)
        r = [r remove_100(raw_dict[dis])]
    end
    return r
end

function dict_to_list1(d)
    to_return = Int[]
    for i in keys(d)
        l = d[i]
        for j=1:length(l)
            elt = l[j]
            if elt != 100
                push!(to_return, elt)
            end
        end
    end
    return to_return
end


# trying matplotlib
function make_jitter(m_dict, p_dict, c_dict)
    m = dict_to_list(m_dict)
    p = dict_to_list(p_dict)
    c = dict_to_list(c_dict)

    m_rwr = rand_jitter(m[1,:][:])
    m_dk = rand_jitter(m[2,:][:])

    p_rwr = rand_jitter(p[1,:][:])
    p_dk = rand_jitter(p[2,:][:])

    c_rwr = rand_jitter(c[1,:][:])
    c_dk = rand_jitter(c[2,:][:])

    m_size = size(m_rwr,1)
    p_size = size(p_rwr, 1)
    c_size = size(c_rwr, 1)

    xticks(range(1,6), ("RWR, Mono", "DK, Mono", "RWR, Poly", "DK, Poly", "RWR, Cancer", "DK, Cancer"))

    x = xticks()
    labels = x[2]
    setp(labels, rotation=30, ha="right", fontsize=10)


    plot(0.15*rand(m_size)+1, m_rwr, linestyle="None", marker="o", c="black", ms=4)
    plot(0.15*rand(m_size)+2, m_dk, linestyle="None", marker="o", c="black", ms=4)

    plot(0.15*rand(p_size)+3, p_rwr, linestyle="None", marker="o", c="black", ms=4)
    plot(0.15*rand(p_size)+4, p_dk, linestyle="None", marker="o", c="black", ms=4)

    plot(0.15*rand(c_size)+5, c_rwr, linestyle="None", marker="o", c="black", ms=4)
    plot(0.15*rand(c_size)+6, c_dk, linestyle="None", marker="o", c="black", ms=4)
    margins(0.08)
    #tight_layout()

    suptitle("Distribution of Rank for Various Ranking Methods, Gene Classes", fontsize=12)
    xlabel("Ranking Method, Gene Classes")
    ylabel("Ranking")

    show()
    savefig("jitter_scatter_ranking_matplotlib.pdf")
    print("Hit <enter> to continue") #so the plot will stay
    readline()
end

function make_jitter_new(m_dict, p_dict, c_dict, mdd, pdd, cdd)
    m = dict_to_list(m_dict)
    p = dict_to_list(p_dict)
    c = dict_to_list(c_dict)

    md1 = dict_to_list1(mdd)
    pd1 = dict_to_list1(pdd)
    cd1 = dict_to_list1(cdd)

    m_rwr = rand_jitter(m[1,:][:])
    m_dk = rand_jitter(m[2,:][:])

    p_rwr = rand_jitter(p[1,:][:])
    p_dk = rand_jitter(p[2,:][:])

    c_rwr = rand_jitter(c[1,:][:])
    c_dk = rand_jitter(c[2,:][:])

    m_deg = rand_jitter(md1)
    p_deg = rand_jitter(pd1)
    c_deg = rand_jitter(cd1)

    # println("m_rwr=" * string(m_rwr))
    # println("m_dk=" * string(m_dk))
    # println("m_deg=" * string(m_deg))

    # println("p_rwr=" * string(p_rwr))
    # println("p_dk=" * string(p_dk))
    # println("p_deg=" * string(p_deg))

    # println("c_rwr=" * string(c_rwr))
    # println("c_dk=" * string(c_dk))
    # println("c_deg=" * string(c_deg))

    # return -1

    m_size = size(m_rwr,1)
    p_size = size(p_rwr, 1)
    c_size = size(c_rwr, 1)

    xticks(range(1,9), ("RWR, Mono", "DK, Mono", "Deg, Mono", "RWR, Poly", "DK, Poly", "Deg, Poly", "RWR, Cancer", "DK, Cancer", "Deg, Cancer"))

    x = xticks()
    labels = x[2]
    setp(labels, rotation=30, ha="right", fontsize=10)


    plot(0.15*rand(m_size)+1, m_rwr, linestyle="None", marker="o", c="black", ms=4)
    plot(0.15*rand(m_size)+2, m_dk, linestyle="None", marker="o", c="black", ms=4)
    plot(0.15*rand(m_size)+3, m_deg, linestyle="None", marker="o", c="black", ms=4)

    plot(0.15*rand(p_size)+4, p_rwr, linestyle="None", marker="o", c="black", ms=4)
    plot(0.15*rand(p_size)+5, p_dk, linestyle="None", marker="o", c="black", ms=4)
    plot(0.15*rand(p_size)+6, p_deg, linestyle="None", marker="o", c="black", ms=4)

    plot(0.15*rand(c_size)+7, c_rwr, linestyle="None", marker="o", c="black", ms=4)
    plot(0.15*rand(c_size)+8, c_dk, linestyle="None", marker="o", c="black", ms=4)
    plot(0.15*rand(c_size)+9, c_deg, linestyle="None", marker="o", c="black", ms=4)
    margins(0.08)
    #tight_layout()

    suptitle("Distribution of Rank for Various Ranking Methods, Gene Classes", fontsize=12)
    xlabel("Ranking Method, Gene Classes")
    ylabel("Ranking")

    show()
end

# let's try this w/ plotly
# jk labeling axes in plotly is pretty annoying

using Plotly

function make_jitter_plotly(m_dict, p_dict, c_dict)
    m = dict_to_list(m_dict)
    p = dict_to_list(p_dict)
    c = dict_to_list(c_dict)

    m_rwr = rand_jitter(m[1,:][:])
    m_dk = rand_jitter(m[2,:][:])

    p_rwr = rand_jitter(p[1,:][:])
    p_dk = rand_jitter(p[2,:][:])

    c_rwr = rand_jitter(c[1,:][:])
    c_dk = rand_jitter(c[2,:][:])

    m_size = size(m_rwr,1)
    p_size = size(p_rwr, 1)
    c_size = size(c_rwr, 1)

    mrwr = Dict()
    mrwr["x"] = 0.1*rand(m_size) + 1
    mrwr["y"] = rand_jitter(m_rwr)
    mrwr["type"] = "scatter"
    mrwr["mode"] = "markers"
    mrwr["name"] = "RWR, Monogenic"

    mdk = Dict()
    mdk["x"] = 0.1*rand(m_size) + 2
    mdk["y"] = rand_jitter(m_dk)
    mdk["type"] = "scatter"
    mdk["mode"] = "markers"
    mdk["name"] = "DK, Monogenic"

    prwr = Dict()
    prwr["x"] = 0.1*rand(p_size) + 3
    prwr["y"] = rand_jitter(p_rwr)
    prwr["type"] = "scatter"
    prwr["mode"] = "markers"
    prwr["name"] = "RWR, Polygenic"

    pdk = Dict()
    pdk["x"] = 0.1*rand(p_size) + 4
    pdk["y"] = rand_jitter(p_dk)
    pdk["type"] = "scatter"
    pdk["mode"] = "markers"
    pdk["name"] = "DK, Polygenic"

    crwr = Dict()
    crwr["x"] = 0.1*rand(c_size) + 5
    crwr["y"] = rand_jitter(c_rwr)
    crwr["type"] = "scatter"
    crwr["mode"] = "markers"
    crwr["name"] = "RWR, Cancer"

    cdk = Dict()
    cdk["x"] = 0.15*rand(c_size) + 6
    cdk["y"] = rand_jitter(c_dk)
    cdk["type"] = "scatter"
    cdk["mode"] = "markers"
    cdk["name"] = "DK, Cancer"

    #change marker
    marker = Dict()
    marker["size"] = 5
    l = [mrwr, mdk, prwr, pdk, crwr, cdk]
    for i in l
        i["marker"] = marker
    end


    layout = Dict()
    layout["title"] = "Distribution of Rank for Various Ranking Methods, Gene Classes"

    xaxis = Dict()
    #xaxis["title"] = "Ranking Method, Disease Class"
    xaxis["showgrid"] = false
    xaxis["ticks"] = ""
    xaxis["showticklabels"] = false
    xaxis["showline"] = false

    yaxis = Dict()
    yaxis["title"] = "Ranking"

    layout["xaxis"] = xaxis
    layout["yaxis"] = yaxis

    response = Plotly.plot([mrwr, mdk, prwr, pdk, crwr, cdk], ["layout" => layout, "filename" => "jitter_scatter_ranking", "fileopt" => "overwrite"])
    println(response["url"])
end

polygenic = {"Alzheimer Disease"=>[1 1 1 1
 1 1 1 1],"Essential hypertension"=>[1 22 8 1 16 16 43 5 68 1 1 6
 1 24 23 1 16 15 21 12 86 1 1 7],"Inflammatory Bowel Disease"=>[2 16 2 45 100 16 84 5
 9 13 7 56 100 16 73 3],"Pheochromocytoma"=>[1 100 23 4 21 59
 2 100 24 5 13 56],"Graves disease"=>[4 5 5 36 33
 5 5 8 38 61],"Non-Insulin-Dependent Diabetes Mellitus"=>[60 11 38 42 100 92 100 4 15 2 42 11 1 36 3 3 47 2 6
 67 8 37 36 100 95 100 5 9 1 45 17 1 25 2 25 44 2 21],"Systemic lupus erythematodes"=>[2 13 5 39 29 100 4
 1 8 57 58 19 100 6],"Mycobacterium tuberculosis, susceptibility to"=>[12 21 7 100 36 8
 16 47 25 100 76 10],"Obesity"=>[1 6 5 1 47 22 1 100 5 1 2 28 1
 1 15 18 1 51 34 2 100 3 2 1 18 1],"Age-Related Macular Degeneration"=>[53 38 3 9 4 59 3 2 100 98 2 12
 39 54 6 11 13 59 9 2 100 98 2 16],"Rheumatoid arthiritis"=>[75 22 1 2 19 12 17
 75 28 2 5 7 38 18],"Maturity-Onset Diabetes of the Young (MODY)"=>[100 14 9 100 7 72 100 40 49
 100 10 10 100 6 90 100 51 68]}

monogenic = {"Cataract, autosomal dominant"=>[1 3 2 1 3 2 90 1 100 55 2
 1 1 2 1 3 2 86 1 100 62 2],"Hirschsprung Disease"=>[1 16 1 1 1 1
 19 17 3 9 1 1],"Stickler syndrome"=>[1 3 1 12
 1 2 1 36],"Charcot Marie Tooth Disease"=>[1 59 20 10 38 6 42 10 34 2 10 1 1 2 100 33 8 22 16 1 28
 1 64 26 7 34 5 21 4 28 4 6 1 1 1 100 29 10 23 24 2 24],"Holoprosencephaly"=>[30 1 1 2 1 4
 52 1 1 2 1 3],"Xeroderma pigmentosum"=>[2 1 1 1 18 1 1 1
 2 1 1 1 18 2 1 1],"Peters anomaly"=>[1 2 1 55
 1 2 1 42],"Nemaline myopathy"=>[3 1 14 4 4 1
 3 1 12 2 4 2],"Fanconi anemia"=>[1 1 1 1 1 3 1 1 1 1 1 1
 1 1 1 1 1 1 1 1 1 1 1 1],"Myoclonic dystonia"=>[21 30 50
 29 70 37],"Epidermolysis bullosa"=>[5 2 1 1 1 1 1 1 1 1 1 1
 3 1 1 1 1 1 1 1 1 1 1 1],"Bare lymphocyte syndrome type II"=>[1 1 1 1
 1 1 1 1],"Juvenile myoclonic epilepsy"=>[33 33 10 5 21
 23 48 7 12 37],"Cholestasis"=>[42 1 2
 43 1 3],"Limb-Girdle Muscle Dystrophy"=>[5 4 85 1 2 1 1 2 1 1 1 100 1 1
 6 12 84 1 1 1 1 2 1 1 1 100 1 1],"Cornelia de Lange syndrome"=>[1 1 1
 1 1 1],"Hypercholesterolemia, familial"=>[2 1 1
 1 1 2],"Bare lymphocyte syndrome type I"=>[3 3 3
 3 3 3],"Progressive external ophthalmoplegia"=>[2 1 100 1
 2 1 100 1],"Fundus albipunctatus"=>[79 1 1
 90 1 1],"Elliptocytosis"=>[1 1 2 1
 1 1 2 1],"Joubert syndrome"=>[45 1 1 22
 33 1 1 13],"Nephronophthisis, hereditary"=>[1 1 1 1
 1 1 1 1],"Familial hyperinsulinemic hypoglycemia"=>[1 14 2 19 23 2
 4 9 2 15 29 2],"Arthrogryposis"=>[4 2 2 2
 2 4 2 2],"Refsum disease"=>[1 1 1 2 1
 1 1 1 1 1],"Primary open-angle glaucoma"=>[3 9 16
 6 14 18],"Hypokalemic periodic paralysis"=>[19 37 26
 74 61 59],"Spondylocostal dysostosis"=>[100 100 66
 100 100 66],"Dilated cardiomyopathy"=>[20 41 3 5 37 11 1 19 25 3 2 1 2 18 61 5 8 2 1 1
 40 41 5 10 58 22 1 17 28 2 2 1 2 59 61 4 7 3 1 1],"Chondrodysplasia punctata"=>[41 1 30 2 1 1 40 1 1 1 1 3 1 1
 52 1 35 2 1 1 32 1 1 1 1 5 1 1],"Distal hereditary motor neuronopathy"=>[6 44 37 1 4 7 96
 5 33 25 1 1 4 95],"Microphthalmia"=>[19 48 44 1 53 9 2 5 7
 29 39 54 22 47 6 5 20 43],"Leigh Syndrome"=>[2 100 18 32 100 100 100 100 5 9 44 100 24 100 100 67 74 4 1 100 100 32 27 100
 2 100 8 24 100 100 100 100 3 4 28 100 15 100 100 60 63 2 1 100 100 16 18 100],"Noonan Syndrome, Costello syndrome, Cardiofaciocutaneous Syndrome"=>[2 1 3 1 2 3 1 1
 1 1 3 1 2 4 1 1],"Retinitis pigmentosa"=>[31 39 1 69 40 67 1 1 10 4 32 1 100 17 47 100 1 71 3 43 100
 38 39 1 88 25 90 1 69 30 3 33 47 100 26 31 100 1 83 1 40 100],"Multiple epiphyseal dysplasia AD"=>[1 1 1 1 1
 1 1 1 1 1],"Night-blindness, congenital stationary"=>[73 1 100 8 20 1 19
 66 1 100 54 67 1 27],"Bile-acid synthesis defect, congenital"=>[38 44 100 100
 44 30 100 100],"Hemochromatosis"=>[100 1 1 1 1
 100 1 1 1 1],"Waardenburg syndrome"=>[2 1 1 1 1 17
 1 1 1 1 1 5],"Hypertrophic cardiomyopathy"=>[1 18 9 1 34 1 1 69 100 1 1 100 11 18 16
 1 15 10 1 9 1 1 64 100 1 1 100 19 11 33],"Spastic paraplegia"=>[4 19 73 12 3 4
 15 63 67 15 7 9],"Ectodermal dysplasia"=>[1 1 1
 1 1 1],"Kartagener syndrome"=>[100 80 33
 100 78 32],"Amyloidosis VI"=>[15 6 1
 13 2 1],"Maple-syrup urine disease"=>[16 1 1 6
 13 1 1 4],"Hemophagocytic lymphohistiocytosis"=>[1 8 8 1
 1 17 11 1],"Leukoencephalopathy with vanishing white matter"=>[1 1 1 2 1
 1 1 1 1 1],"Hermansky-Pudlak syndrome"=>[2 100 2 2 1 1 19 1
 2 100 3 3 1 1 28 1],"Spinocerebellar Ataxia"=>[3 19 1 1 36 6 20 100 84 2 1 46
 2 16 3 6 24 10 24 100 48 1 1 53],"Aicardi-Goutieres syndrome"=>[75 36 79 29
 65 31 65 31],"Bardet-Biedl Syndrome"=>[1 2 2 1 1 1 1 1 1 2 1 1 1
 1 2 4 1 1 1 1 1 1 2 1 1 1],"Ehlers Danlos syndrome"=>[5 2 64 1 1 1 63 20 7
 9 2 75 2 1 2 73 13 7],"Amyotrophic lateral sclerosis"=>[8 31 33 36 8 3 14 29
 13 17 27 32 23 4 25 43],"Primary microcephaly"=>[15 9 28 12
 13 12 27 23],"Nonsyndromic hearing loss"=>[1 14 86 100 77 100 60 100 100 2 40 100 4 2 100 42 23 38 100 2 1 1 100 1 3 7 4 1 50 1 21 100 100 2 68 57 57 47 36 100 20 29
 1 2 88 100 80 100 50 100 100 3 41 100 3 2 100 43 15 25 100 2 1 1 100 1 45 13 3 1 38 1 24 100 100 2 66 69 60 45 35 100 8 38],"Cutis laxa"=>[9 1 1 1
 6 1 1 1],"Familial exudative vitreoretinopathy"=>[14 100 3
 64 100 18],"Long QT Syndrome"=>[2 22 2 2 1 6 3 19 1
 9 12 2 2 1 10 2 60 1],"Combined oxidative phosphorylation deficiency"=>[11 16 3 25
 15 7 1 18],"Congenital myasthenic syndromes"=>[1 1 2 2 1
 1 1 1 2 1],"Achromatopsia"=>[51 100 68
 26 100 60],"Congenital central hypoventilation syndrome"=>[43 45 1 8 1 51
 33 62 2 30 9 79],"Hyper-IgM syndrome"=>[1 27 1 10
 1 9 1 6],"Cerebrooculofacioskeletal syndrome"=>[1 3 2 1
 1 4 2 1],"Kallmann syndrome"=>[1 1 100 100
 1 1 100 100],"Neuronal ceroid lipofuscinosis"=>[1 34 5 13 3 36 100
 1 31 13 9 3 9 100],"Polycystic kidney disease"=>[1 1 61
 1 1 61],"Pseudohypoaldosteronism, type I, autosomal recessive"=>[2 1 2
 2 1 2],"Multiple Acyl-CoA Dehydrogenase deficiency"=>[1 1 33
 1 1 24],"Adrenoleukodystrophy"=>[1 1 1 1 1
 1 1 1 1 1],"Pituitary dwarfism"=>[1 2 43 1
 1 2 82 1],"Generalized epilepsy with febrile seizures plus"=>[62 24 46 24
 79 42 63 45],"Brachydactyly"=>[1 12 1 23 24
 1 18 1 22 29],"Arrhythmogenic right ventricular dysplasia (ARVD)"=>[3 40 1 3 1 21
 2 70 1 2 1 18],"Mitochondrial complex I deficiency disorders"=>[24 1 100 100 5 100 49 1 21 100 22 100 100 28 100 26
 15 1 100 100 3 100 23 1 13 100 19 100 100 11 100 15],"Nonbullous congenital ichthyosiform erythroderma"=>[1 48 2 2
 1 47 2 2],"Severe congenital neutropenia"=>[15 7 100
 10 5 100],"Keratosis palmoplantaris striata"=>[3 4 1
 4 3 1],"Osteopetrosis"=>[57 1 14 69 1
 65 1 36 53 1],"Pulmonary surfactant metabolism dysfunction"=>[47 78 59
 54 70 62],"Atypical mycobacteriosis, familial"=>[1 1 1 2 1 1
 1 2 1 2 1 1]}

cancer= {"Prostate cancer"=>[16 1 47 1 6 1 23 28 12 1 3 19 27
 11 2 56 1 9 1 17 21 11 2 4 21 22],"Esophageal carcinoma"=>[1 100 59 8 57 3 8 37 1 1
 2 100 38 11 60 3 12 36 5 6],"Bladder Cancer"=>[1 1 10 2
 1 1 17 3],"Lung cancer"=>[1 59 1 1 3
 1 59 8 3 6],"Glioma of brain, familial"=>[38 2 1 3 19 5 3 2 2 4 38 13 2 51 2 93 4 24 68 2 2
 33 1 2 4 17 11 5 2 1 5 33 17 2 45 1 91 3 25 73 7 2],"Medulloblastoma"=>[11 33 2 20 70 1
 9 25 3 21 81 1],"Juvenile myelomonocytic leukemia"=>[27 1 1 13 2
 26 1 1 10 2],"Breast cancer familial"=>[2 3 38 1 1 1 60 7 4 1 1 1 6 1 1
 7 10 30 1 1 1 52 3 3 1 1 1 12 1 1],"Hepataocellular carcinoma"=>[1 2 1
 2 3 1],"Pancreatic carcinoma"=>[1 2 5 2 100 1
 1 7 5 5 100 6],"Thyroid carcinoma, papillary"=>[11 2 6 11 7 4 22 7
 9 2 5 17 4 3 26 4],"Hereditary nonpolyposis colorectal cancer"=>[2 2 2 1 2 1 1
 2 2 2 2 2 1 1]}

cd={"Prostate cancer"=>[15 2 33 1 8 6 21 35 21 1 4 11 36],"Esophageal carcinoma"=>[1 100 65 10 68 2 7 39 1 1],"Bladder Cancer"=>[1 3 5 2],"Lung cancer"=>[1 62 3 1 4],"Glioma of brain, familial"=>[29 11 1 1 18 3 8 1 6 9 36 12 6 48 1 87 4 27 68 2 2],"Medulloblastoma"=>[6 30 2 12 58 1],"Juvenile myelomonocytic leukemia"=>[39 10 3 18 2],"Breast cancer familial"=>[5 6 38 10 1 6 62 3 38 1 3 2 26 4 1],"Hepataocellular carcinoma"=>[1 4 1],"Pancreatic carcinoma"=>[1 2 6 3 100 1],"Thyroid carcinoma, papillary"=>[9 3 4 10 6 7 22 9],"Hereditary nonpolyposis colorectal cancer"=>[3 7 2 8 7 8 6]}
pd = {"Alzheimer Disease"=>[7 8 1 4],"Essential hypertension"=>[18 21 23 37 12 8 61 47 60 5 15 3],"Inflammatory Bowel Disease"=>[69 16 63 43 100 19 88 6],"Pheochromocytoma"=>[1 100 18 3 20 64],"Graves disease"=>[3 18 21 38 24],"Non-Insulin-Dependent Diabetes Mellitus"=>[60 7 28 50 100 85 100 5 13 16 48 22 1 34 5 18 54 15 33],"Systemic lupus erythematodes"=>[21 24 66 45 57 100 24],"Mycobacterium tuberculosis, susceptibility to"=>[18 14 3 100 37 10],"Obesity"=>[34 7 3 32 50 22 1 100 3 36 15 50 29],"Age-Related Macular Degeneration"=>[55 39 72 4 63 59 59 57 100 98 15 15],"Rheumatoid arthiritis"=>[69 21 5 12 24 38 37],"Maturity-Onset Diabetes of the Young (MODY)"=>[100 18 7 100 20 74 100 42 42]}
md = {"Cataract, autosomal dominant"=>[48 55 63 37 33 43 86 56 100 51 23],"Hirschsprung Disease"=>[37 14 3 37 59 17],"Stickler syndrome"=>[16 45 36 32],"Charcot Marie Tooth Disease"=>[26 53 15 9 50 5 45 11 41 2 31 35 64 10 100 33 7 22 12 1 28],"Holoprosencephaly"=>[34 31 30 22 23 12],"Xeroderma pigmentosum"=>[21 12 5 6 22 15 15 4],"Peters anomaly"=>[32 5 12 60],"Nemaline myopathy"=>[17 3 24 4 9 4],"Fanconi anemia"=>[10 6 17 24 19 38 3 5 4 16 7 12],"Myoclonic dystonia"=>[29 19 61],"Epidermolysis bullosa"=>[27 12 24 5 39 37 12 8 37 47 14 33],"Bare lymphocyte syndrome type II"=>[12 56 35 23],"Juvenile myoclonic epilepsy"=>[37 27 57 36 24],"Cholestasis"=>[52 47 48],"Limb-Girdle Muscle Dystrophy"=>[4 36 87 27 25 17 3 2 15 45 31 100 41 8],"Cornelia de Lange syndrome"=>[6 2 13],"Hypercholesterolemia, familial"=>[15 28 12],"Bare lymphocyte syndrome type I"=>[15 27 19],"Progressive external ophthalmoplegia"=>[2 65 100 34],"Fundus albipunctatus"=>[62 34 89],"Elliptocytosis"=>[7 29 45 16],"Joubert syndrome"=>[49 10 41 19],"Nephronophthisis, hereditary"=>[38 10 55 40],"Familial hyperinsulinemic hypoglycemia"=>[1 13 28 23 42 42],"Arthrogryposis"=>[17 32 44 31],"Refsum disease"=>[33 37 48 31 21],"Primary open-angle glaucoma"=>[2 6 11],"Hypokalemic periodic paralysis"=>[39 33 55],"Spondylocostal dysostosis"=>[100 100 50],"Dilated cardiomyopathy"=>[23 35 8 9 37 36 25 30 36 4 26 3 2 45 63 27 9 4 2 1],"Chondrodysplasia punctata"=>[40 37 26 33 39 10 44 48 37 31 43 55 31 37],"Distal hereditary motor neuronopathy"=>[31 42 33 1 11 3 98],"Microphthalmia"=>[15 62 59 70 58 9 1 66 50],"Leigh Syndrome"=>[16 100 29 48 100 100 100 100 16 17 57 100 68 100 100 75 86 7 4 100 100 44 68 100],"Noonan Syndrome, Costello syndrome, Cardiofaciocutaneous Syndrome"=>[5 20 18 3 3 2 4 2],"Retinitis pigmentosa"=>[23 44 11 64 37 56 7 62 36 2 32 59 100 48 58 100 16 65 6 45 100],"Multiple epiphyseal dysplasia AD"=>[42 48 74 40 36],"Night-blindness, congenital stationary"=>[78 98 100 68 80 71 23],"Bile-acid synthesis defect, congenital"=>[36 54 100 100],"Hemochromatosis"=>[100 36 62 25 52],"Waardenburg syndrome"=>[11 22 13 59 17 19],"Hypertrophic cardiomyopathy"=>[16 17 30 36 63 26 3 77 100 3 1 100 39 26 20],"Spastic paraplegia"=>[25 13 80 13 9 62],"Ectodermal dysplasia"=>[26 23 19],"Kartagener syndrome"=>[100 82 34],"Amyloidosis VI"=>[16 31 1],"Maple-syrup urine disease"=>[22 56 72 17],"Hemophagocytic lymphohistiocytosis"=>[6 8 40 63],"Leukoencephalopathy with vanishing white matter"=>[33 33 29 18 43],"Hermansky-Pudlak syndrome"=>[57 100 82 48 55 5 13 57],"Spinocerebellar Ataxia"=>[2 14 1 3 51 5 13 100 95 10 3 44],"Aicardi-Goutieres syndrome"=>[95 57 98 35],"Bardet-Biedl Syndrome"=>[21 21 4 26 54 30 23 33 18 12 10 24 15],"Ehlers Danlos syndrome"=>[51 14 64 13 13 34 67 19 62],"Amyotrophic lateral sclerosis"=>[12 45 47 33 12 3 15 39],"Primary microcephaly"=>[13 6 24 8],"Nonsyndromic hearing loss"=>[21 55 90 100 97 100 65 100 100 2 46 100 5 33 100 41 30 50 100 54 35 34 100 47 70 11 3 63 50 26 30 100 100 63 56 46 56 45 32 100 27 32],"Cutis laxa"=>[26 15 14 17],"Familial exudative vitreoretinopathy"=>[48 100 17],"Long QT Syndrome"=>[11 27 61 45 10 33 9 32 23],"Combined oxidative phosphorylation deficiency"=>[27 27 7 61],"Congenital myasthenic syndromes"=>[52 79 71 96 27],"Achromatopsia"=>[55 100 73],"Congenital central hypoventilation syndrome"=>[48 40 3 20 37 59],"Hyper-IgM syndrome"=>[16 35 12 11],"Cerebrooculofacioskeletal syndrome"=>[4 24 15 15],"Kallmann syndrome"=>[48 4 100 100],"Neuronal ceroid lipofuscinosis"=>[3 50 4 12 3 58 100],"Polycystic kidney disease"=>[16 10 59],"Pseudohypoaldosteronism, type I, autosomal recessive"=>[17 25 16],"Multiple Acyl-CoA Dehydrogenase deficiency"=>[34 13 43],"Adrenoleukodystrophy"=>[37 48 10 37 37],"Pituitary dwarfism"=>[35 9 49 56],"Generalized epilepsy with febrile seizures plus"=>[56 57 51 31],"Brachydactyly"=>[6 59 40 20 42],"Arrhythmogenic right ventricular dysplasia (ARVD)"=>[21 30 5 30 8 16],"Mitochondrial complex I deficiency disorders"=>[28 16 100 100 16 100 57 19 68 100 19 100 100 44 100 68],"Nonbullous congenital ichthyosiform erythroderma"=>[37 59 80 75],"Severe congenital neutropenia"=>[18 6 100],"Keratosis palmoplantaris striata"=>[9 10 5],"Osteopetrosis"=>[48 34 11 80 63],"Pulmonary surfactant metabolism dysfunction"=>[55 96 33],"Atypical mycobacteriosis, familial"=>[22 1 51 3 33 44]}


#make_jitter(monogenic, polygenic, cancer)
make_jitter_new(monogenic, polygenic, cancer, md, pd, cd)
#make_jitter_plotly(monogenic, polygenic, cancer)