mult = {"RHO"=>{[31,38],[19,27],15},"PTEN"=>{[1,1],[3,4],107},"KCNJ11"=>{[2,2],[38,37],11},"PALB2"=>{[1,1],[1,1],34},"DCTN1"=>{[7,4],[3,4],73},"ECE1"=>{[1,19],[1,1],10},"TPM2"=>{[3,3],[4,2],22},"BRCA2"=>{[1,1],[1,1],[2,1],[11,9],[1,1],[5,5],57},"SLC22A18"=>{[59,59],[60,52],2},"DSP"=>{[1,1],[1,1],[1,1],73},"TNNT2"=>{[25,28],[1,1],10},"RET"=>{[1,3],[1,2],[2,2],[4,5],50},"TPM3"=>{[4,2],[6,5],75},"COL9A1"=>{[1,1],[1,1],3},"BRIP1"=>{[3,1],[4,3],13},"CHEK2"=>{[3,4],[4,3],[1,1],91},"SETX"=>{[37,25],[36,32],15},"MLH1"=>{[2,2],[2,2],165},"GDNF"=>{[1,9],[1,9],5},"NDUFS3"=>{[2,2],[1,1],21},"KRAS"=>{[2,2],[1,1],[1,8],[1,1],[2,5],62},"TRIM32"=>{[5,6],[2,4],63},"HNF4A"=>{[11,8],[9,10],77},"ABCC8"=>{[2,2],[49,68],6},"TCAP"=>{[2,1],[1,1],30},"PRPH2"=>{[79,90],[1,69],4},"NDUFS1"=>{[5,3],[5,3],19},"CAV3"=>{[1,1],[18,15],23},"LRP5"=>{[14,64],[57,65],11},"CTNNB1"=>{[1,1],[1,1],362},"CIITA"=>{[1,1],[2,5],28},"MYBPC3"=>{[2,2],[1,1],12},"GARS"=>{[10,4],[4,1],20},"PMS2"=>{[2,2],[1,1],53},"LMNA"=>{[2,4],[2,2],[2,2],206},"TP53"=>{[1,2],[1,1],[2,1],[1,1],[1,2],[1,1],865},"PTPN11"=>{[3,4],[2,2],148},"PSEN1"=>{[2,3],[1,1],109},"PTPN22"=>{[2,1],[22,28],23},"APC"=>{[3,3],[2,7],[2,3],170},"HSPB1"=>{[1,2],[1,1],361},"ACTC1"=>{[1,1],[1,1],59},"GCK"=>{[23,29],[40,51],5},"NDUFS4"=>{[32,16],[28,11],4},"PEX10"=>{[1,1],[1,1],10},"COL11A2"=>{[12,36],[29,38],5},"PEX26"=>{[1,1],[1,1],[1,1],5},"APOE"=>{[1,1],[6,15],57},"EGFR"=>{[1,3],[1,2],863},"ENPP1"=>{[42,36],[47,51],2},"PSEN2"=>{[3,5],[1,1],66},"BRAF"=>{[1,1],[3,6],57},"SCN5A"=>{[5,10],[3,2],28},"PEX7"=>{[1,1],[2,2],9},"MYH7"=>{[19,17],[9,10],15},"TTN"=>{[1,1],[1,1],[1,1],104},"EDN3"=>{[1,1],[1,1],[51,79],5},"EDNRB"=>{[1,1],[1,1],10},"HSPB8"=>{[10,6],[6,5],16},"NDUFS7"=>{[44,28],[49,23],6},"NDUFV1"=>{[24,15],[21,13],5},"EYA4"=>{[61,61],[1,1],1},"ERCC5"=>{[1,1],[1,1],19},"TGFBR2"=>{[8,12],[2,2],69},"CTLA4"=>{[33,61],[4,6],14},"NF1"=>{[3,3],[19,17],[13,10],[23,24],26},"CDKN2A"=>{[1,5],[2,2],[1,6],186},"ERCC6"=>{[1,1],[9,11],43},"NPHP1"=>{[1,1],[1,1],22},"PEX5"=>{[1,1],[1,1],80},"ERCC2"=>{[1,2],[2,2],34},"ABCA4"=>{[1,47],[59,59],1},"SLC22A4"=>{[2,9],[75,75],3},"PEX1"=>{[1,1],[1,1],[1,1],6},"PEX2"=>{[2,1],[1,1],6},"SGCD"=>{[1,1],[18,59],6},"APP"=>{[1,1],[1,1],2009},"NEUROD1"=>{[3,25],[14,10],14},"FBLN5"=>{[1,1],[12,16],15},"VDR"=>{[4,5],[7,25],98},"GABRD"=>{[10,7],[24,42],4},"NDUFS8"=>{[27,18],[26,15],5},"HRAS"=>{[1,1],[2,3],111}}

sing = {"COL5A2"=>{[5,9],2},"CLN6"=>{[34,31],6},"DBT"=>{[16,13],11},"AHI1"=>{[1,1],5},"CA4"=>{[69,88],5},"PIK3CA"=>{[3,10],75},"ESPN"=>{[77,80],1},"PAX3"=>{[1,1],25},"GLI2"=>{[1,1],9},"BRCA1"=>{[1,1],530},"LAMC2"=>{[1,1],11},"CYP7B1"=>{[44,30],1},"MITF"=>{[2,1],30},"COL9A3"=>{[1,1],3},"LHX3"=>{[43,82],8},"PMS1"=>{[1,1],48},"SHH"=>{[1,1],10},"GABRG2"=>{[24,45],6},"CRYGC"=>{[2,2],7},"MIP"=>{[1,1],9},"MYO6"=>{[4,3],42},"LIPC"=>{[11,17],4},"SURF1"=>{[67,60],3},"RFXAP"=>{[1,1],6},"NIPBL"=>{[1,1],22},"INSR"=>{[1,4],114},"CDH23"=>{[2,2],3},"COCH"=>{[57,60],4},"ZFHX3"=>{[27,22],10},"CABP4"=>{[1,1],1},"HFE"=>{[1,1],6},"SPG20"=>{[12,15],32},"SPG21"=>{[3,7],38},"HOXD13"=>{[24,29],7},"TGM1"=>{[1,1],10},"AGT"=>{[1,1],23},"PLP1"=>{[4,15],14},"TNFRSF1B"=>{[6,7],114},"ABCB1"=>{[16,16],17},"BCKDHA"=>{[1,1],4},"DNASE1"=>{[29,19],4},"NRL"=>{[67,90],4},"COL1A1"=>{[1,2],44},"CRYGS"=>{[3,1],4},"C2"=>{[4,13],6},"FGFR1"=>{[1,1],72},"PRPF31"=>{[4,3],77},"PDHA1"=>{[9,4],22},"GJB2"=>{[2,2],8},"UNG"=>{[27,9],12},"DTNBP1"=>{[1,1],61},"ACTA1"=>{[1,1],157},"TG"=>{[5,8],9},"DNM2"=>{[8,10],89},"CRYBB2"=>{[2,2],8},"MYL2"=>{[11,19],9},"ASPM"=>{[28,27],13},"GJB1"=>{[28,24],12},"DSG2"=>{[3,2],15},"TAP1"=>{[3,3],25},"CDH1"=>{[1,2],144},"MSR1"=>{[47,56],9},"EGR2"=>{[20,26],18},"NEB"=>{[4,4],37},"CACNA1A"=>{[1,6],95},"RYR2"=>{[21,18],22},"TRIM33"=>{[11,9],51},"SFTPB"=>{[47,54],1},"CHRNE"=>{[2,2],1},"ETFDH"=>{[33,24],2},"CFHR1"=>{[2,2],4},"PEX6"=>{[1,1],9},"NPHP3"=>{[1,1],6},"GJB3"=>{[3,45],4},"DSC2"=>{[3,2],9},"MLH3"=>{[1,2],44},"AP3B1"=>{[19,28],18},"BBS4"=>{[1,1],26},"NOS3"=>{[1,1],54},"MAPK8IP1"=>{[1,1],31},"FOXC1"=>{[1,1],9},"HCCS"=>{[48,39],1},"HPS1"=>{[2,3],3},"CLCN2"=>{[33,23],10},"EFHC1"=>{[33,48],6},"POLG2"=>{[1,1],3},"MYLK2"=>{[18,11],18},"IFNGR2"=>{[1,1],8},"UNC13D"=>{[1,1],4},"PCSK9"=>{[2,1],26},"VAPB"=>{[8,13],45},"ATXN10"=>{[19,16],30},"ATXN1"=>{[1,3],263},"CHRNA1"=>{[1,1],4},"GJA8"=>{[1,1],2},"TSC2"=>{[3,5],54},"PPT1"=>{[36,9],5},"SPTA1"=>{[1,1],28},"RPGR"=>{[1,1],16},"SDHC"=>{[59,56],4},"TSFM"=>{[25,18],7},"CRYBB1"=>{[3,3],6},"SIX3"=>{[30,52],8},"ABCA3"=>{[78,70],1},"MSH2"=>{[2,2],83},"MXI1"=>{[12,11],17},"BCKDHB"=>{[1,1],2},"KCNC3"=>{[84,48],1},"CRYM"=>{[21,24],3},"BBS7"=>{[2,2],24},"IHH"=>{[12,18],4},"PKP2"=>{[1,1],31},"NTRK1"=>{[4,3],65},"AGTR1"=>{[1,1],25},"CLN3"=>{[3,3],88},"LDLR"=>{[1,1],21},"MUC3A"=>{[84,73],1},"GFM1"=>{[16,7],8},"SLC2A2"=>{[6,21],4},"KLF6"=>{[16,11],22},"KCNH2"=>{[1,1],27},"TNNT3"=>{[2,2],9},"RUNX1"=>{[1,2],77},"CACNA1S"=>{[26,59],6},"SCN1A"=>{[46,63],4},"ABCB11"=>{[1,1],5},"KAL1"=>{[1,1],3},"PAX6"=>{[2,2],38},"ADD1"=>{[16,15],30},"NCOA3"=>{[7,3],110},"PKD1"=>{[1,1],31},"SPG11"=>{[73,67],2},"WWOX"=>{[1,6],240},"RFXANK"=>{[1,1],18},"NDUFS2"=>{[1,1],18},"SIX1"=>{[1,1],13},"TLR5"=>{[39,58],5},"CEL"=>{[72,90],3},"DES"=>{[8,7],35},"COL7A1"=>{[1,1],16},"B4GALT7"=>{[64,75],3},"BFSP2"=>{[55,62],4},"TOR1A"=>{[21,29],20},"STRA6"=>{[53,47],2},"PRPH"=>{[33,27],9},"SPTBN2"=>{[46,53],13},"TPM1"=>{[1,1],41},"GRHL2"=>{[68,66],3},"CLN8"=>{[5,13],30},"TMC1"=>{[36,35],2},"CACNB4"=>{[21,37],10},"FANCL"=>{[1,1],36},"INVS"=>{[1,1],9},"DLG5"=>{[5,3],28},"RNASEH2C"=>{[75,65],1},"XPC"=>{[1,1],32},"PITX2"=>{[1,1],19},"ABCB4"=>{[2,3],3},"PLOD1"=>{[20,13],25},"TAZ"=>{[41,41],11},"PKHD1"=>{[61,61],2},"CAPN3"=>{[1,1],20},"CD40LG"=>{[1,1],12},"SCN4A"=>{[19,74],10},"NF2"=>{[4,5],41},"ATP1B1"=>{[16,16],30},"PEX12"=>{[1,1],3},"FANCB"=>{[1,1],10},"EIF2B3"=>{[1,1],10},"CRYGD"=>{[2,2],3},"PHYH"=>{[1,1],15},"TNNI2"=>{[2,2],14},"CDKN2B"=>{[13,17],19},"GC"=>{[5,5],13},"SLC2A4"=>{[2,1],45},"TAPBP"=>{[3,3],11},"RAX"=>{[5,20],1},"OSTM1"=>{[1,1],7},"GNPAT"=>{[1,1],8},"RAD51"=>{[1,1],86},"CHRNB1"=>{[1,1],3},"PEX3"=>{[3,5],2},"HPS4"=>{[1,1],2},"SCNN1B"=>{[2,2],22},"ATXN7"=>{[1,1],75},"TNNT1"=>{[1,2],66},"TGFB3"=>{[40,70],13},"FANCD2"=>{[1,1],44},"BDNF"=>{[8,30],17},"RNF6"=>{[37,36],5},"PTCH1"=>{[2,2],19},"MYO7A"=>{[1,1],16},"NDUFV2"=>{[24,15],5},"SOS1"=>{[2,1],67},"VSX2"=>{[1,22],3},"TBP"=>{[3,2],167},"KRT1"=>{[4,3],57},"PKD2"=>{[1,1],17},"RP9"=>{[40,25],4},"KCNJ2"=>{[6,10],14},"KCNQ1"=>{[1,1],17},"CNGA3"=>{[68,60],1},"CFHR3"=>{[3,6],2},"OPTN"=>{[3,6],99},"ALS2"=>{[31,17],6},"POLH"=>{[18,18],22},"PCSK1"=>{[22,34],6},"TRIOBP"=>{[38,25],8},"TNXB"=>{[7,7],6},"BMPR1B"=>{[1,1],70},"RLBP1"=>{[1,1],3},"MPZ"=>{[1,1],4},"SOX2"=>{[2,5],335},"CSRP3"=>{[5,4],13},"CD82"=>{[1,2],33},"RNASEH2A"=>{[79,65],1},"RP2"=>{[47,31],3},"IRS1"=>{[3,2],91},"SLC40A1"=>{[1,1],5},"CFH"=>{[2,2],19},"CA2"=>{[14,36],19},"BCOR"=>{[9,6],30},"SEMA4A"=>{[71,83],6},"MYOC"=>{[9,14],44},"LFNG"=>{[67,67],3},"CYP1B1"=>{[55,42],2},"SBF2"=>{[38,34],4},"EPB41"=>{[1,1],142},"NEFH"=>{[29,43],6},"HAX1"=>{[7,5],63},"PADI4"=>{[19,7],12},"CST3"=>{[15,13],10},"PLEC"=>{[1,1],61},"BSCL2"=>{[44,33],10},"NPHP4"=>{[1,1],8},"CD209"=>{[36,76],12},"NEFL"=>{[2,1],43},"RNASEH2B"=>{[36,31],1},"TUFM"=>{[3,1],62},"RAB27A"=>{[1,1],32},"EPHB2"=>{[19,21],42},"ATP7A"=>{[9,6],7},"LRPPRC"=>{[1,1],76},"CACNA1C"=>{[22,12],22},"YEATS4"=>{[24,25],23},"ERCC1"=>{[3,4],20},"LGI1"=>{[68,73],2},"ARL6"=>{[1,1],8},"KRT14"=>{[5,3],38},"HMCN1"=>{[53,39],2},"PRX"=>{[59,64],9},"DCC"=>{[8,11],56},"TSC1"=>{[2,1],50},"ZNF217"=>{[38,30],10},"BBS9"=>{[1,1],9},"ZFAT"=>{[36,38],3},"GRM6"=>{[20,67],1},"POMT1"=>{[85,84],2},"DLD"=>{[6,4],14},"MAD1L1"=>{[6,9],46},"KCNE3"=>{[37,61],10},"FCGR2A"=>{[13,8],15},"DFNA5"=>{[40,41],4},"HPS5"=>{[1,1],3},"DFNB31"=>{[1,1],5},"SGCG"=>{[1,1],22},"DMBT1"=>{[33,25],9},"USH1C"=>{[1,1],12},"DNAH5"=>{[33,32],1},"ITGA6"=>{[1,1],22},"MATN3"=>{[1,1],7},"MYO3A"=>{[47,45],2},"SGCA"=>{[1,1],10},"HADH"=>{[19,15],8},"NCOA4"=>{[11,17],12},"GABRA1"=>{[5,12],5},"AICDA"=>{[10,6],61},"PRPF3"=>{[1,1],51},"MC4R"=>{[1,1],11},"CCL2"=>{[21,47],21},"HTRA1"=>{[38,54],6},"TGFB1"=>{[5,18],88},"KRT5"=>{[2,1],54},"GNAT1"=>{[8,54],6},"GNB3"=>{[8,23],22},"MARVELD2"=>{[60,50],1},"FGF14"=>{[36,24],1},"FZD4"=>{[3,18],12},"TCF7L2"=>{[15,9],25},"MAP2K1"=>{[1,1],86},"EBP"=>{[40,32],6},"MYL3"=>{[34,9],6},"PTGIS"=>{[43,21],4},"CHRND"=>{[2,1],2},"RGS5"=>{[5,12],7},"HESX1"=>{[1,1],6},"ALOX12B"=>{[2,2],4},"ANK2"=>{[2,9],24},"ELAC2"=>{[23,17],16},"CFB"=>{[3,9],7},"MTMR2"=>{[1,1],5},"COL1A2"=>{[1,1],40},"COL5A1"=>{[1,2],12},"KCNE2"=>{[2,2],3},"GDF5"=>{[1,1],8},"PPM1D"=>{[6,12],24},"SDHA"=>{[4,2],26},"OTX2"=>{[7,43],9},"AGPS"=>{[1,1],12},"STX11"=>{[8,17],60},"LITAF"=>{[22,23],16},"ERCC4"=>{[2,2],17},"KIF1B"=>{[16,24],32},"DRD2"=>{[30,70],23},"COL2A1"=>{[1,1],33},"MET"=>{[2,3],121},"PEX13"=>{[1,1],7},"TFR2"=>{[1,1],6},"CAPN10"=>{[60,67],4},"PMP22"=>{[1,1],9},"PLN"=>{[20,40],11},"MYOT"=>{[4,12],14},"EDA"=>{[1,1],14},"SMC1A"=>{[1,1],84},"LDB3"=>{[11,22],5},"ELN"=>{[1,1],25},"MYH9"=>{[2,3],104},"CFL2"=>{[14,12],15},"CCDC28B"=>{[1,1],9},"NDRG1"=>{[6,5],77},"CLN5"=>{[1,1],33},"VHL"=>{[1,2],417},"PDCD1"=>{[5,57],3},"WDR36"=>{[16,18],19},"GDAP1"=>{[34,28],4},"FANCF"=>{[1,1],16},"SGCE"=>{[50,37],4},"FANCA"=>{[1,1],75},"GJB6"=>{[2,2],2},"IL23R"=>{[45,56],5},"EDARADD"=>{[1,1],11},"RDH5"=>{[1,1],2},"DNAI1"=>{[80,78],1},"EFEMP2"=>{[1,1],54},"FGFR3"=>{[10,17],49},"BBS2"=>{[1,1],25},"ATXN2"=>{[2,1],38},"ROR2"=>{[23,22],17},"RAPSN"=>{[1,1],9},"HSD3B7"=>{[38,44],12},"RPE65"=>{[17,26],4},"GLUD1"=>{[14,9],27},"ITM2B"=>{[6,2],8},"PDE6B"=>{[73,66],1},"BLOC1S3"=>{[2,2],6},"CDK5RAP2"=>{[15,13],39},"GNAT2"=>{[51,26],4},"ETFB"=>{[1,1],8},"CCDC6"=>{[22,26],13},"NOD2"=>{[16,13],37},"EPHX2"=>{[47,44],5},"SDHB"=>{[21,13],17},"ADRB3"=>{[28,18],3},"TGIF1"=>{[4,3],27},"ARSE"=>{[41,52],4},"COL3A1"=>{[2,2],15},"MYO1A"=>{[14,2],9},"ABHD5"=>{[48,47],4},"RB1"=>{[1,1],258},"SELE"=>{[22,24],16},"SOX10"=>{[1,1],30},"SPG7"=>{[19,63],26},"LEP"=>{[1,1],10},"SUFU"=>{[20,21],31},"ABCC9"=>{[37,58],5},"PRKCG"=>{[6,10],65},"SCNN1A"=>{[1,1],21},"MYH3"=>{[2,4],12},"EIF2B5"=>{[1,1],14},"ACTG1"=>{[4,3],140},"KLF11"=>{[7,6],16},"APOB"=>{[1,2],34},"COL17A1"=>{[1,1],13},"GLTSCR1"=>{[51,45],9},"ADRB2"=>{[1,2],239},"NDUFS6"=>{[22,19],9},"BARD1"=>{[1,1],253},"MYH6"=>{[69,64],2},"LRRN2"=>{[93,91],2},"TMPRSS3"=>{[50,38],4},"POLG"=>{[1,1],3},"GPD2"=>{[36,25],6},"COL9A2"=>{[1,1],6},"CTSD"=>{[13,9],28},"POMC"=>{[1,2],11},"ERCC3"=>{[1,1],44},"PRPF8"=>{[3,1],76},"IMPDH1"=>{[43,40],4},"PRKAG2"=>{[16,33],17},"ANG"=>{[14,25],11},"NFKBIL1"=>{[12,38],16},"OTOF"=>{[86,88],1},"BBS12"=>{[2,2],13},"DSG1"=>{[3,4],27},"GGCX"=>{[30,35],8},"PPARG"=>{[5,11],141},"XPA"=>{[1,1],43},"TNNI3"=>{[1,1],18},"FANCE"=>{[1,1],14},"CRB1"=>{[10,30],9},"CENPJ"=>{[9,12],55},"IL12RB1"=>{[1,1],7},"CEP290"=>{[22,13],14},"GJA3"=>{[1,1],6},"PPP1R3A"=>{[42,45],3},"BBS1"=>{[1,1],33},"ARHGAP26"=>{[27,26],11},"MYO15A"=>{[1,1],5},"SLC25A4"=>{[2,2],56},"RFX5"=>{[1,1],9},"PITX3"=>{[90,86],2},"LZTS1"=>{[57,60],3},"IL12B"=>{[1,1],4},"HLA-DRB1"=>{[17,18],16},"SGCB"=>{[1,1],6},"SCNN1G"=>{[2,2],16},"EIF2B2"=>{[1,1],15},"INSIG2"=>{[5,3],36},"SFTPC"=>{[59,62],14},"COMP"=>{[1,1],8},"IGHMBP2"=>{[96,95],1},"MYH14"=>{[20,8],17},"GLI1"=>{[38,33],24},"RB1CC1"=>{[2,7],70},"BCS1L"=>{[32,24],6},"RAB7A"=>{[10,7],43},"SOD1"=>{[8,23],28},"PTCH2"=>{[70,81],5},"EDAR"=>{[1,1],8},"RETN"=>{[92,95],2},"ATL1"=>{[4,9],3},"TREX1"=>{[29,31],15},"MBL2"=>{[12,16],10},"DDB2"=>{[1,1],44},"HPS6"=>{[2,3],10},"YARS"=>{[42,21],15},"LHFPL5"=>{[42,43],2},"EIF2B4"=>{[1,1],9},"ZIC2"=>{[1,1],7},"ITGB4"=>{[1,1],50},"RNASEL"=>{[28,21],5},"LAMA3"=>{[1,1],8},"MAP2K2"=>{[1,1],29},"HAMP"=>{[1,1],3},"BBS5"=>{[1,1],9},"CYP3A5"=>{[68,86],4},"CD40"=>{[1,1],49},"CLCN7"=>{[1,1],4},"COX15"=>{[74,63],2},"CACNA1F"=>{[1,1],2},"MRPS16"=>{[11,15],12},"ATP8B1"=>{[42,43],3},"SLC22A5"=>{[2,7],4},"MSH6"=>{[2,2],50},"SIX6"=>{[44,54],4},"DEC1"=>{[59,38],2},"RPGRIP1"=>{[1,1],25},"SLC4A1"=>{[2,2],16},"COL11A1"=>{[3,2],3},"LEPR"=>{[2,1],23},"MKKS"=>{[1,1],20},"PROP1"=>{[1,1],13},"GHRL"=>{[1,1],10},"STAT1"=>{[2,2],140},"GFI1"=>{[15,10],20},"NRAS"=>{[1,1],44},"TULP1"=>{[32,33],3},"ADAMTS2"=>{[63,73],2},"IKBKG"=>{[1,2],409},"CD151"=>{[1,1],10},"SCN1B"=>{[62,79],4},"GLTSCR2"=>{[38,33],13},"BBS10"=>{[1,1],17},"SMAD4"=>{[2,7],265},"IFNGR1"=>{[1,1],14},"SPTB"=>{[1,1],15},"FANCG"=>{[1,1],50},"PRF1"=>{[8,11],7},"RAX2"=>{[98,98],1},"LAMB3"=>{[1,1],8},"SNAI2"=>{[17,5],19},"TTC8"=>{[1,1],6},"ZEB2"=>{[16,17],24},"FANCC"=>{[1,1],60},"DMD"=>{[1,1],38},"AR"=>{[1,1],326},"ALOXE3"=>{[2,2],3},"DYSF"=>{[1,1],49},"MCPH1"=>{[12,23],20},"RP1"=>{[39,39],1},"HLA-A"=>{[8,10],44},"PHOX2B"=>{[43,33],2},"KCNE1"=>{[2,2],6},"SCO2"=>{[18,8],12},"EIF2B1"=>{[2,1],24},"PTPN1"=>{[4,5],106},"TMPO"=>{[3,2],55},"PPP2R2B"=>{[20,24],53},"TCIRG1"=>{[69,53],3},"ETFA"=>{[1,1],23},"WFS1"=>{[23,15],9},"NOS1AP"=>{[19,60],12},"TAP2"=>{[3,3],15},"TMEM67"=>{[45,33],4},"SNX3"=>{[19,29],25},"ASCL1"=>{[45,62],9},"DIAPH1"=>{[7,13],56},"MFN2"=>{[33,29],12},"SMC3"=>{[1,1],92},"KCNQ4"=>{[57,69],7},"POU1F1"=>{[2,2],14},"FANCM"=>{[1,1],26},"PRKAR1A"=>{[7,4],116},"AKT2"=>{[2,2],55},"TPR"=>{[7,4],38}}

mult_degs = Int[]
for v in values(mult)
    push!(mult_degs, v[length(v)])
end

mult_rwr = Int[]
for v in values(mult)
    for i=1:(length(v)-1)
        push!(mult_rwr, v[i][1])
    end
end

mult_dk = Int[]
for v in values(mult)
    for i=1:(length(v)-1)
        push!(mult_dk, v[i][2])
    end
end

sing_degs = Int[]
for v in values(sing)
    push!(sing_degs, v[length(v)])
end

sing_rwr = Int[]
for v in values(sing)
    push!(sing_rwr, v[1][1])
end

sing_dk = Int[]
for v in values(sing)
    push!(sing_dk, v[1][2])
end

println("mult_degs=" * string(mult_degs))
println("mult_rwr=" * string(mult_rwr))
println("mult_dk=" * string(mult_dk))
println("sing_degs=" * string(sing_degs))
println("sing_rwr=" * string(sing_rwr))
println("sing_dk=" * string(sing_dk))

# deg plot

# using PyPlot

# xticks(range(1,2), ("Mult", "Single"))

# # x = xticks()
# # labels = x[2]
# # setp(labels, rotation=30, ha="right", fontsize=10)

# plot(0.15*(rand(length(mult_rwr)) - 0.5)+1, mult_rwr, linestyle="None", marker="o", c="black", ms=4)
# plot(0.15*(rand(length(sing_rwr)) - 0.5)+2, sing_rwr, linestyle="None", marker="o", c="black", ms=4)


