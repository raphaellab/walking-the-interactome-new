using Plotly

rwr = Dict()
x = ["Monogenic", "Polygenic", "Cancer"]
#IREF14
#y_r = [24.518912747598335, 13.752307328294494, 25.135272723819615] #include_100
#y_r = [25.538636918201156, 14.616121602078906, 25.73736377269739] #dont include_100
#y_r = [19.277511310643845, 26.742565012959748, 12.283218864468866] #real avgs, include_100
#y_r = [14.427757365922936, 18.70756222943723, 10.252663308913307] #real avgs, dont include_100

y_r=[15.78796474609728, 23.06304961378491, 10.736726699226699] #DK t=0.0015

#INTHINT2
#y_r = [16.304153523647475, 9.940809045234568, 24.30499209344784] #include_100
#y_r = [17.72789436735101, 11.115970583087368, 25.42375656029544] #dont include_100
#y_r = [33.98473489362044, 37.071479580361164, 14.304105616605618] #real avgs, include_100
#y_r = [24.195587100461935, 24.62106782106782, 10.01839133089133] #real avgs, dont include_100

rwr["y"] = y_r
rwr["x"] = x
rwr["type"] = "bar"
rwr["name"] = "Diffusion Kernel, t=0.0015"

dk = Dict()
#IREF14
#y_d = [24.145063211074916, 12.136189552686544, 20.067734131199483] #include_100
#y_d = [25.162147306688553, 12.818948263240953, 20.360955153733368] #dont include_100
#y_d = [20.68670614860374, 30.154669660261764, 12.732097069597069] #real avgs, include_100
#y_d = [15.775916553326196, 23.06304961378491, 10.736726699226699] #real avgs, dont include_100

y_d = [14.665791363061084, 18.96054133774722, 12.310375966625964] #DK, t=0.1

#INTHINT2
#y_d = [16.604306478467503,  9.733143790475166, 22.768431237962005] #include_100
#y_d = [18.21617926817685, 10.685122585897313, 23.710618669004884] #dont include_100
#y_d = [33.64236320908007, 40.58910109140373, 15.910821123321126] #real avgs, include_100
#y_d = [23.673320955538543, 29.08860930735931, 11.762309218559217] #real avgs, dont include_100

dk["y"] = y_d
dk["x"] = x
dk["type"] = "bar"
dk["name"] = "Diffusion Kernel, t=0.1"

layout = Dict()

layout["title"] = "Diffusion Kernel Results w/ Iref14"

x_axis = Dict()
x_axis["title"] = "Disease Class"
layout["xaxis"] = x_axis

y_axis = Dict()
y_axis["title"] = "Ranking"
layout["yaxis"] = y_axis

layout["barmode"] = "group"

data = [rwr, dk]

response = Plotly.plot(data, ["layout" => layout, "filename" => "iref14_dk_ranking_compare", "fileopt" => "overwrite"])
plot_url = response["url"]
println(plot_url)
