using Plotly

rwr = Dict()
x = ["Monogenic", "Polygenic", "Cancer"]

#iref14
# y_r=[14.171086856745625,18.700078516254987,10.18305352055352] #rwr, r=0.35

#inthint2
y_r = [24.436484254940606, 23.950604256854252, 9.028113553113553] #rwr, r=0.35

rwr["y"] = y_r
rwr["x"] = x
rwr["type"] = "bar"
rwr["name"] = "RWR, r=0.35"

dk = Dict()

#iref14
# y_d=[11.947644830447002, 16.130514175367114,10.254161579161579] #hyperrwr, r=0.4

#inthint2
y_d = [20.774683003132942,19.546320346320346,8.96554487179487] #hyperrwr, r=0.35

dk["y"] = y_d
dk["x"] = x
dk["type"] = "bar"
dk["name"] = "HyperRWR, r=0.35"

r = Dict()

#iref14
# y = [14.68949909722354,18.96054133774722,12.310375966625964] #dk, t=0.1

#inthint2
y = [24.010643808480783,25.774242424242427,10.075755494505495] #dk, t=0.05

r["y"] = y
r["x"] = x
r["type"] = "bar"
r["name"] = "DK, t=0.1"

r2 = Dict()

#iref14
# y2 = [11.95101934706482,19.015496350055177,17.32510429385429] #t=0.01, hyperDK

#inthint2
y2 = [19.162533609237723,19.960254329004332,13.018658424908425] #t=0.05, hyperDK

r2["y"] = y2
r2["x"] = x
r2["type"] = "bar"
r2["name"] = "HyperDK, t=0.01"

r3 = Dict()

#iref14
y3 = [11.731917115574326,20.75800441388677,29.802352971102973] #t=0.1, hyperDK

#inthint2
y3 = [19.17687972810245,19.668434343434345,15.420856227106228] #t=0.1, hyperDK

r3["y"] = y3
r3["x"] = x
r3["type"] = "bar"
r3["name"] = "HyperDK, t=0.1"

################

layout = Dict()

layout["title"] = "Comparing avg ranks of DK w/ and w/o hyperedges, w/ IntHint2"

x_axis = Dict()
x_axis["title"] = "Disease Class"
layout["xaxis"] = x_axis

y_axis = Dict()
y_axis["title"] = "Ranking"
layout["yaxis"] = y_axis

layout["barmode"] = "group"

data = [r, r2, r3]

response = Plotly.plot(data, ["layout" => layout, "filename" => "inthint2_hyper_dk_ranking_compare", "fileopt" => "overwrite"])
plot_url = response["url"]
println(plot_url)
