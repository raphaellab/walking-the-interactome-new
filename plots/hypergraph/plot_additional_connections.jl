using Plotly

rwr = Dict()
x = ["Monogenic", "Polygenic", "Cancer"]

y_r=[15419.829258240372,16176.167038549449,2.201972327517043e4]
#i changed cancer's number because otherwise it was enormous

rwr["y"] = y_r
rwr["x"] = x
rwr["type"] = "bar"
rwr["name"] = "Genes that went up in rank"

dk = Dict()

y_d=[9425,9434,12207]

dk["y"] = y_d
dk["x"] = x
dk["type"] = "bar"
dk["name"] = "Genes whose rank stayed the same"

r = Dict()

y = [345.46177337305176,7475.981082469819,360.7746314181175]

r["y"] = y
r["x"] = x
r["type"] = "bar"
r["name"] = "Genes that went down in rank"

################

layout = Dict()

layout["title"] = "(Geometric) Average number of additional gene connections via hyperedges, w/ Iref14"

x_axis = Dict()
x_axis["title"] = "How rank of gene changed w/ hyperedges added"
layout["xaxis"] = x_axis

y_axis = Dict()
y_axis["title"] = "Ranking"
layout["yaxis"] = y_axis

layout["barmode"] = "group"

data = [rwr, dk, r]

#.... meh, this plot is super ugly
response = Plotly.plot(data, ["layout" => layout, "filename" => "iref14_hyper_additional_conn_compare", "fileopt" => "overwrite"])
plot_url = response["url"]
println(plot_url)
