monogenic={"Cataract, autosomal dominant"=>(
[1 3 2 1 3 2 90 1 100 55 2
 1 1 2 1 3 2 86 1 100 62 2],

[2,4,3,6,6,7,2,9,-1,4,8]),"Hirschsprung Disease"=>(
[1 16 1 1 1 1
 19 17 3 9 1 1],

[10,24,50,5,5,10]),"Stickler syndrome"=>(
[1 3 1 12
 1 2 1 36],

[33,3,3,5]),"Charcot Marie Tooth Disease"=>(
[1 59 20 10 38 6 42 10 34 2 10 1 1 2 100 33 8 22 16 1 28
 1 64 26 7 34 5 21 4 28 4 6 1 1 1 100 29 10 23 24 2 24],

[9,9,18,43,4,77,15,20,4,206,16,5,4,43,-1,12,89,16,32,361,12]),"Holoprosencephaly"=>(
[30 1 1 2 1 4
 52 1 1 2 1 3],

[8,10,9,19,7,27]),"Xeroderma pigmentosum"=>(
[2 1 1 1 18 1 1 1
 2 1 1 1 18 2 1 1],

[17,32,44,43,22,34,19,44]),"Peters anomaly"=>(
[1 2 1 55
 1 2 1 42],

[9,38,19,2]),"Nemaline myopathy"=>(
[3 1 14 4 4 1
 3 1 12 2 4 2],

[22,157,15,75,37,66]),"Fanconi anemia"=>(
[1 1 1 1 1 3 1 1 1 1 1 1
 1 1 1 1 1 1 1 1 1 1 1 1],

[34,57,14,10,16,13,75,50,60,26,44,36]),"Myoclonic dystonia"=>(
[21 30 50
 29 70 37],

[20,23,4]),"Epidermolysis bullosa"=>(
[5 2 1 1 1 1 1 1 1 1 1 1
 3 1 1 1 1 1 1 1 1 1 1 1],

[38,54,11,73,10,13,50,61,8,8,22,16]),"Bare lymphocyte syndrome type II"=>(
[1 1 1 1
 1 1 1 1],

[28,9,6,18]),"Juvenile myoclonic epilepsy"=>(
[33 33 10 5 21
 23 48 7 12 37],

[10,6,4,5,10]),"Cholestasis"=>(
[42 1 2
 43 1 3],

[3,5,3]),"Limb-Girdle Muscle Dystrophy"=>(
[5 4 85 1 2 1 1 2 1 1 1 100 1 1
 6 12 84 1 1 1 1 2 1 1 1 100 1 1],

[63,14,2,20,30,23,104,206,22,6,6,-1,10,49]),"Cornelia de Lange syndrome"=>(
[1 1 1
 1 1 1],

[84,92,22]),"Hypercholesterolemia, familial"=>(
[2 1 1
 1 1 2],

[26,21,34]),"Bare lymphocyte syndrome type I"=>(
[3 3 3
 3 3 3],

[25,15,11]),"Progressive external ophthalmoplegia"=>(
[2 1 100 1
 2 1 100 1],

[56,3,-1,3]),"Fundus albipunctatus"=>(
[79 1 1
 90 1 1],

[4,3,2]),"Elliptocytosis"=>(
[1 1 2 1
 1 1 2 1],

[142,15,16,28]),"Joubert syndrome"=>(
[45 1 1 22
 33 1 1 13],

[4,22,5,14]),"Nephronophthisis, hereditary"=>(
[1 1 1 1
 1 1 1 1],

[6,22,8,9]),"Familial hyperinsulinemic hypoglycemia"=>(
[1 14 2 19 23 2
 4 9 2 15 29 2],

[114,27,11,8,5,6]),"Arthrogryposis"=>(
[4 2 2 2
 2 4 2 2],

[22,12,9,14]),"Refsum disease"=>(
[1 1 1 2 1
 1 1 1 1 1],

[9,5,6,6,15]),"Primary open-angle glaucoma"=>(
[3 9 16
 6 14 18],

[99,44,19]),"Hypokalemic periodic paralysis"=>(
[19 37 26
 74 61 59],

[10,10,6]),"Spondylocostal dysostosis"=>(
[100 100 66
 100 100 66],

[-1,-1,3]),"Dilated cardiomyopathy"=>(
[20 41 3 5 37 11 1 19 25 3 2 1 2 18 61 5 8 2 1 1
 40 41 5 10 58 22 1 17 28 2 2 1 2 59 61 4 7 3 1 1],

[11,11,66,28,5,5,30,15,10,55,12,104,206,6,1,13,35,109,38,59]),"Chondrodysplasia punctata"=>(
[41 1 30 2 1 1 40 1 1 1 1 3 1 1
 52 1 35 2 1 1 32 1 1 1 1 5 1 1],

[4,5,8,9,3,80,6,6,9,6,8,2,12,10]),"Distal hereditary motor neuronopathy"=>(
[6 44 37 1 4 7 96
 5 33 25 1 1 4 95],

[16,10,15,361,20,73,1]),"Microphthalmia"=>(
[19 48 44 1 53 9 2 5 7
 29 39 54 22 47 6 5 20 43],

[25,1,4,3,2,30,335,1,9]),"Leigh Syndrome"=>(
[2 100 18 32 100 100 100 100 5 9 44 100 24 100 100 67 74 4 1 100 100 32 27 100
 2 100 8 24 100 100 100 100 3 4 28 100 15 100 100 60 63 2 1 100 100 16 18 100],

[21,-1,12,6,-1,-1,-1,-1,19,22,6,-1,5,-1,-1,3,2,26,76,-1,-1,4,5,-1]),"Noonan Syndrome, Costello syndrome, Cardiofaciocutaneous Syndrome"=>(
[2 1 3 1 2 3 1 1
 1 1 3 1 2 4 1 1],

[67,29,26,86,62,148,57,111]),"Retinitis pigmentosa"=>(
[31 39 1 69 40 67 1 1 10 4 32 1 100 17 47 100 1 71 3 43 100
 38 39 1 88 25 90 1 69 30 3 33 47 100 26 31 100 1 83 1 40 100],

[15,1,51,5,4,4,16,4,9,77,3,1,-1,4,3,-1,25,6,76,4,-1]),"Multiple epiphyseal dysplasia AD"=>(
[1 1 1 1 1
 1 1 1 1 1],

[7,6,3,8,3]),"Night-blindness, congenital stationary"=>(
[73 1 100 8 20 1 19
 66 1 100 54 67 1 27],

[1,1,-1,6,1,2,15]),"Bile-acid synthesis defect, congenital"=>(
[38 44 100 100
 44 30 100 100],

[12,1,-1,-1]),"Hemochromatosis"=>(
[100 1 1 1 1
 100 1 1 1 1],

[-1,5,3,6,6]),"Waardenburg syndrome"=>(
[2 1 1 1 1 17
 1 1 1 1 1 5],

[30,30,25,5,10,19]),"Hypertrophic cardiomyopathy"=>(
[1 18 9 1 34 1 1 69 100 1 1 100 11 18 16
 1 15 10 1 9 1 1 64 100 1 1 100 19 11 33],

[18,23,15,10,6,12,104,2,-1,41,59,-1,9,18,17]),"Spastic paraplegia"=>(
[4 19 73 12 3 4
 15 63 67 15 7 9],

[14,26,2,32,38,3]),"Ectodermal dysplasia"=>(
[1 1 1
 1 1 1],

[11,14,8]),"Kartagener syndrome"=>(
[100 80 33
 100 78 32],

[-1,1,1]),"Amyloidosis VI"=>(
[15 6 1
 13 2 1],

[10,8,2009]),"Maple-syrup urine disease"=>(
[16 1 1 6
 13 1 1 4],

[11,2,4,14]),"Hemophagocytic lymphohistiocytosis"=>(
[1 8 8 1
 1 17 11 1],

[32,60,7,4]),"Leukoencephalopathy with vanishing white matter"=>(
[1 1 1 2 1
 1 1 1 1 1],

[14,10,15,24,9]),"Hermansky-Pudlak syndrome"=>(
[2 100 2 2 1 1 19 1
 2 100 3 3 1 1 28 1],

[6,-1,3,10,2,61,18,3]),"Spinocerebellar Ataxia"=>(
[3 19 1 1 36 6 20 100 84 2 1 46
 2 16 3 6 24 10 24 100 48 1 1 53],

[167,30,263,95,1,65,53,-1,1,38,75,13]),"Aicardi-Goutieres syndrome"=>(
[75 36 79 29
 65 31 65 31],

[1,1,1,15]),"Bardet-Biedl Syndrome"=>(
[1 2 2 1 1 1 1 1 1 2 1 1 1
 1 2 4 1 1 1 1 1 1 2 1 1 1],

[8,13,63,9,9,17,33,9,25,24,26,6,20]),"Ehlers Danlos syndrome"=>(
[5 2 64 1 1 1 63 20 7
 9 2 75 2 1 2 73 13 7],

[2,15,3,44,40,12,2,25,6]),"Amyotrophic lateral sclerosis"=>(
[8 31 33 36 8 3 14 29
 13 17 27 32 23 4 25 43],

[45,6,9,15,28,73,11,6]),"Primary microcephaly"=>(
[15 9 28 12
 13 12 27 23],

[39,55,13,20]),"Nonsyndromic hearing loss"=>(
[1 14 86 100 77 100 60 100 100 2 40 100 4 2 100 42 23 38 100 2 1 1 100 1 3 7 4 1 50 1 21 100 100 2 68 57 57 47 36 100 20 29
 1 2 88 100 80 100 50 100 100 3 41 100 3 2 100 43 15 25 100 2 1 1 100 1 45 13 3 1 38 1 24 100 100 2 66 69 60 45 35 100 8 38],

[16,9,1,-1,1,-1,1,-1,-1,104,4,-1,140,8,-1,2,9,8,-1,2,5,13,-1,5,4,56,42,1,4,12,3,-1,-1,3,3,7,4,2,2,-1,17,5]),"Cutis laxa"=>(
[9 1 1 1
 6 1 1 1],

[7,15,54,25]),"Familial exudative vitreoretinopathy"=>(
[14 100 3
 64 100 18],

[11,-1,12]),"Long QT Syndrome"=>(
[2 22 2 2 1 6 3 19 1
 9 12 2 2 1 10 2 60 1],

[24,22,3,6,27,14,28,12,17]),"Combined oxidative phosphorylation deficiency"=>(
[11 16 3 25
 15 7 1 18],

[12,8,62,7]),"Congenital myasthenic syndromes"=>(
[1 1 2 2 1
 1 1 1 2 1],

[4,3,2,1,9]),"Achromatopsia"=>(
[51 100 68
 26 100 60],

[4,-1,1]),"Congenital central hypoventilation syndrome"=>(
[43 45 1 8 1 51
 33 62 2 30 9 79],

[2,9,50,17,5,5]),"Hyper-IgM syndrome"=>(
[1 27 1 10
 1 9 1 6],

[12,12,49,61]),"Cerebrooculofacioskeletal syndrome"=>(
[1 3 2 1
 1 4 2 1],

[43,20,34,19]),"Kallmann syndrome"=>(
[1 1 100 100
 1 1 100 100],

[3,72,-1,-1]),"Neuronal ceroid lipofuscinosis"=>(
[1 34 5 13 3 36 100
 1 31 13 9 3 9 100],

[33,6,30,28,88,5,-1]),"Polycystic kidney disease"=>(
[1 1 61
 1 1 61],

[17,31,2]),"Pseudohypoaldosteronism, type I, autosomal recessive"=>(
[2 1 2
 2 1 2],

[16,21,22]),"Multiple Acyl-CoA Dehydrogenase deficiency"=>(
[1 1 33
 1 1 24],

[8,23,2]),"Adrenoleukodystrophy"=>(
[1 1 1 1 1
 1 1 1 1 1],

[5,6,80,10,7]),"Pituitary dwarfism"=>(
[1 2 43 1
 1 2 82 1],

[13,14,8,6]),"Generalized epilepsy with febrile seizures plus"=>(
[62 24 46 24
 79 42 63 45],

[4,4,4,6]),"Brachydactyly"=>(
[1 12 1 23 24
 1 18 1 22 29],

[70,4,8,17,7]),"Arrhythmogenic right ventricular dysplasia (ARVD)"=>(
[3 40 1 3 1 21
 2 70 1 2 1 18],

[15,13,73,9,31,22]),"Mitochondrial complex I deficiency disorders"=>(
[24 1 100 100 5 100 49 1 21 100 22 100 100 28 100 26
 15 1 100 100 3 100 23 1 13 100 19 100 100 11 100 15],

[5,21,-1,-1,19,-1,6,18,5,-1,9,-1,-1,4,-1,5]),"Nonbullous congenital ichthyosiform erythroderma"=>(
[1 48 2 2
 1 47 2 2],

[10,4,3,4]),"Severe congenital neutropenia"=>(
[15 7 100
 10 5 100],

[20,63,-1]),"Keratosis palmoplantaris striata"=>(
[3 4 1
 4 3 1],

[27,57,73]),"Osteopetrosis"=>(
[57 1 14 69 1
 65 1 36 53 1],

[11,7,19,3,4]),"Pulmonary surfactant metabolism dysfunction"=>(
[47 78 59
 54 70 62],

[1,1,14]),"Atypical mycobacteriosis, familial"=>(
[1 1 1 2 1 1
 1 2 1 2 1 1],

[14,409,7,140,8,4])}

polygenic={"Alzheimer Disease"=>(
[1 1 1 1
 1 1 1 1],

[57,66,2009,109]),"Essential hypertension"=>(
[1 22 8 1 16 16 43 5 68 1 1 6
 1 24 23 1 16 15 21 12 86 1 1 7],

[23,16,22,10,30,30,4,7,4,54,25,114]),"Inflammatory Bowel Disease"=>(
[2 16 2 45 100 16 84 5
 9 13 7 56 100 16 73 3],

[3,37,4,5,-1,17,1,28]),"Pheochromocytoma"=>(
[1 100 23 4 21 59
 2 100 24 5 13 56],

[417,-1,26,50,17,4]),"Graves disease"=>(
[4 5 5 36 33
 5 5 8 38 61],

[98,13,9,3,14]),"Non-Insulin-Dependent Diabetes Mellitus"=>(
[60 11 38 42 100 92 100 4 15 2 42 11 1 36 3 3 47 2 6
 67 8 37 36 100 95 100 5 9 1 45 17 1 25 2 25 44 2 21],

[4,77,11,2,-1,2,-1,106,25,45,3,4,31,6,91,14,5,55,4]),"Systemic lupus erythematodes"=>(
[2 13 5 39 29 100 4
 1 8 57 58 19 100 6],

[23,15,3,5,4,-1,14]),"Mycobacterium tuberculosis, susceptibility to"=>(
[12 21 7 100 36 8
 16 47 25 100 76 10],

[10,21,98,-1,12,44]),"Obesity"=>(
[1 6 5 1 47 22 1 100 5 1 2 28 1
 1 15 18 1 51 34 2 100 3 2 1 18 1],

[10,57,88,10,2,6,239,-1,36,11,23,3,11]),"Age-Related Macular Degeneration"=>(
[53 38 3 9 4 59 3 2 100 98 2 12
 39 54 6 11 13 59 9 2 100 98 2 16],

[2,6,2,43,6,1,7,4,-1,1,19,15]),"Rheumatoid arthiritis"=>(
[75 22 1 2 19 12 17
 75 28 2 5 7 38 18],

[3,23,77,28,12,16,16]),"Maturity-Onset Diabetes of the Young (MODY)"=>(
[100 14 9 100 7 72 100 40 49
 100 10 10 100 6 90 100 51 68],

[-1,14,77,-1,16,3,-1,5,6])}

cancer={"Prostate cancer"=>(
[16 1 47 1 6 1 23 28 12 1 3 19 27
 11 2 56 1 9 1 17 21 11 2 4 21 22],

[22,144,9,107,46,57,16,5,17,33,91,42,10]),"Esophageal carcinoma"=>(
[1 100 59 8 57 3 8 37 1 1
 2 100 38 11 60 3 12 36 5 6],

[865,-1,2,56,3,170,69,5,186,240]),"Bladder Cancer"=>(
[1 1 10 2
 1 1 17 3],

[258,62,49,111]),"Lung cancer"=>(
[1 59 1 1 3
 1 59 8 3 6],

[865,2,62,863,57]),"Glioma of brain, familial"=>(
[38 2 1 3 19 5 3 2 2 4 38 13 2 51 2 93 4 24 68 2 2
 33 1 2 4 17 11 5 2 1 5 33 17 2 45 1 91 3 25 73 7 2],

[24,50,863,107,26,141,54,186,57,41,13,19,53,9,865,2,91,23,2,170,165]),"Medulloblastoma"=>(
[11 33 2 20 70 1
 9 25 3 21 81 1],

[57,9,170,31,5,362]),"Juvenile myelomonocytic leukemia"=>(
[27 1 1 13 2
 26 1 1 10 2],

[11,44,62,26,148]),"Breast cancer familial"=>(
[2 3 38 1 1 1 60 7 4 1 1 1 6 1 1
 7 10 30 1 1 1 52 3 3 1 1 1 12 1 1],

[70,75,10,34,530,57,2,110,13,865,253,86,24,91,326]),"Hepataocellular carcinoma"=>(
[1 2 1
 2 3 1],

[865,121,362]),"Pancreatic carcinoma"=>(
[1 2 5 2 100 1
 1 7 5 5 100 6],

[865,265,57,62,-1,186]),"Thyroid carcinoma, papillary"=>(
[11 2 6 11 7 4 22 7
 9 2 5 17 4 3 26 4],

[51,50,75,12,116,65,13,38]),"Hereditary nonpolyposis colorectal cancer"=>(
[2 2 2 1 2 1 1
 2 2 2 2 2 1 1],

[83,50,165,44,69,48,53])}

using PyPlot

function get_rank_deg(dic)
    rwr = Int[]
    dk = Int[]
    deg = Int[]
    for z in values(dic)
        dis_rwr = z[1][1,:][:]
        dis_dk = z[1][2,:][:]
        dis_deg = z[2]
        rwr = [rwr,dis_rwr]
        dk = [dk,dis_dk]
        deg = [deg,dis_deg]
    end
    return rwr, dk, deg
end

m_rwr, m_dk, m_deg = get_rank_deg(monogenic)
p_rwr, p_dk, p_deg = get_rank_deg(polygenic)
c_rwr, c_dk, c_deg = get_rank_deg(cancer)

rwr = [m_rwr,p_rwr,c_rwr]
dk = [m_dk,p_dk,c_dk]
deg = [m_deg,p_deg,c_deg]

#rwr
# plot(m_deg, m_rwr, linestyle="None", marker="o", c="Blue", label="Monogenic")
# plot(p_deg, p_rwr, linestyle="None", marker="o", c="Red", label="Polygenic")
# plot(c_deg, c_rwr, linestyle="None", marker="o", c="Yellow", label="Cancer")

# xscale("log")

# ylim([-3,103])
# xlim([10.0^-0.3, 10.0^4])

# suptitle("Degree vs. Ranking for RWR, r=0.35 over all Disease Classes w/ iRef14")
# xlabel("Degree")
# ylabel("Ranking")

# legend(loc=1, prop=["size" => 9])
# savefig("degree_rank_rwr_all_new.pdf")

#dk
plot(m_deg, m_dk, linestyle="None", marker="o", c="Blue", label="Monogenic")
plot(p_deg, p_dk, linestyle="None", marker="o", c="Red", label="Polygenic")
plot(c_deg, c_dk, linestyle="None", marker="o", c="Yellow", label="Cancer")

xscale("log")

ylim([-3,103])
xlim([10.0^-0.3, 10.0^4])

suptitle("Degree vs. Ranking for DK, beta=0.0015 over all Disease Classes w/ iRef14")
xlabel("Degree")
ylabel("Ranking")

legend(loc=1, prop=["size" => 9])
savefig("degree_rank_dk_all_new.pdf")





