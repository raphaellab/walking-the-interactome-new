monogenic = {"Cataract, autosomal dominant"=>(
[1 3 2 1 3 2 90 1 100 56 2
 1 1 2 1 3 2 85 1 100 52 2],

[2,4,3,6,6,7,2,9,-1,4,8]),"Hirschsprung Disease"=>(
[1 16 1 1 1 1
 15 19 3 45 1 1],

[10,24,50,5,5,10]),"Stickler syndrome"=>(
[1 3 1 12
 1 2 1 39],

[33,3,3,5]),"Charcot Marie Tooth Disease"=>(
[1 60 20 10 38 6 41 10 34 2 9 1 1 2 100 32 8 22 16 1 28
 1 74 25 7 31 5 21 4 26 4 5 1 3 1 100 29 9 19 23 2 23],

[9,9,18,43,4,77,15,20,4,206,16,5,4,43,-1,12,89,16,32,361,12]),"Holoprosencephaly"=>(
[30 1 1 2 1 4
 65 1 1 2 1 38],

[8,10,9,19,7,27]),"Xeroderma pigmentosum"=>(
[2 1 1 1 18 1 1 1
 2 1 1 1 22 2 1 1],

[17,32,44,43,22,34,19,44]),"Peters anomaly"=>(
[1 2 1 55
 1 3 1 32],

[9,38,19,2]),"Nemaline myopathy"=>(
[3 1 14 4 4 1
 3 1 12 2 4 2],

[22,157,15,75,37,66]),"Fanconi anemia"=>(
[1 1 1 1 1 3 1 1 1 1 1 1
 1 1 1 1 1 1 1 1 1 1 1 1],

[34,57,14,10,16,13,75,50,60,26,44,36]),"Myoclonic dystonia"=>(
[21 30 50
 18 69 19],

[20,23,4]),"Epidermolysis bullosa"=>(
[4 2 1 1 1 1 1 1 1 1 1 1
 3 1 1 1 1 1 2 1 1 1 1 1],

[38,54,11,73,10,13,50,61,8,8,22,16]),"Bare lymphocyte syndrome type II"=>(
[1 1 1 1
 1 1 1 1],

[28,9,6,18]),"Juvenile myoclonic epilepsy"=>(
[33 33 10 5 21
 10 59 7 36 33],

[10,6,4,5,10]),"Cholestasis"=>(
[42 1 2
 11 1 35],

[3,5,3]),"Limb-Girdle Muscle Dystrophy"=>(
[5 4 85 1 2 1 1 2 1 1 1 100 1 1
 7 17 61 1 1 1 1 2 1 1 1 100 1 1],

[63,14,2,20,30,23,104,206,22,6,6,-1,10,49]),"Cornelia de Lange syndrome"=>(
[1 1 1
 1 1 1],

[84,92,22]),"Hypercholesterolemia, familial"=>(
[2 1 1
 1 1 2],

[26,21,34]),"Bare lymphocyte syndrome type I"=>(
[3 3 3
 3 3 3],

[25,15,11]),"Progressive external ophthalmoplegia"=>(
[2 1 100 1
 3 1 100 1],

[56,3,-1,3]),"Fundus albipunctatus"=>(
[80 1 1
 67 1 1],

[4,3,2]),"Elliptocytosis"=>(
[1 1 2 1
 1 1 2 1],

[142,15,16,28]),"Joubert syndrome"=>(
[44 1 1 22
 54 1 1 9],

[4,22,5,14]),"Nephronophthisis, hereditary"=>(
[1 1 1 1
 1 1 1 1],

[6,22,8,9]),"Familial hyperinsulinemic hypoglycemia"=>(
[1 14 2 19 22 2
 5 9 2 17 51 2],

[114,27,11,8,5,6]),"Arthrogryposis"=>(
[4 2 2 2
 3 8 2 2],

[22,12,9,14]),"Refsum disease"=>(
[1 1 1 2 1
 1 1 1 39 1],

[9,5,6,6,15]),"Primary open-angle glaucoma"=>(
[3 9 17
 8 46 35],

[99,44,19]),"Hypokalemic periodic paralysis"=>(
[19 37 26
 4 10 12],

[10,10,6]),"Spondylocostal dysostosis"=>(
[100 100 66
 100 100 66],

[-1,-1,3]),"Dilated cardiomyopathy"=>(
[20 43 3 5 37 11 1 19 24 3 2 1 2 17 61 5 8 2 1 1
 56 43 5 11 59 46 1 18 54 2 2 1 2 60 58 4 8 3 1 1],

[11,11,66,28,5,5,30,15,10,55,12,104,206,6,1,13,35,109,38,59]),"Chondrodysplasia punctata"=>(
[41 1 30 2 1 1 39 1 1 1 1 3 1 1
 54 1 34 2 1 1 30 1 1 1 1 6 1 1],

[4,5,8,9,3,80,6,6,9,6,8,2,12,10]),"Distal hereditary motor neuronopathy"=>(
[6 44 37 1 4 7 96
 5 29 20 1 1 4 92],

[16,10,15,361,20,73,1]),"Microphthalmia"=>(
[19 48 44 1 53 9 2 5 7
 24 30 40 64 34 6 22 46 69],

[25,1,4,3,2,30,335,1,9]),"Leigh Syndrome"=>(
[2 100 18 31 100 100 100 100 5 9 43 100 23 100 100 66 74 4 1 100 100 32 27 100
 2 100 7 23 100 100 100 100 3 5 28 100 16 100 100 46 53 2 1 100 100 16 18 100],

[21,-1,12,6,-1,-1,-1,-1,19,22,6,-1,5,-1,-1,3,2,26,76,-1,-1,4,5,-1]),"Noonan Syndrome, Costello syndrome, Cardiofaciocutaneous Syndrome"=>(
[2 1 3 1 2 3 1 1
 1 1 3 1 2 4 1 1],

[67,29,26,86,62,148,57,111]),"Retinitis pigmentosa"=>(
[32 39 1 71 40 67 1 1 10 4 32 1 100 17 47 100 1 71 3 43 100
 48 41 1 93 44 88 1 70 49 3 30 50 100 32 22 100 1 73 1 34 100],

[15,1,51,5,4,4,16,4,9,77,3,1,-1,4,3,-1,25,6,76,4,-1]),"Multiple epiphyseal dysplasia AD"=>(
[1 1 1 1 1
 1 1 1 1 1],

[7,6,3,8,3]),"Night-blindness, congenital stationary"=>(
[73 1 100 8 20 1 19
 78 1 100 19 80 1 15],

[1,1,-1,6,1,2,15]),"Bile-acid synthesis defect, congenital"=>(
[38 44 100 100
 44 26 100 100],

[12,1,-1,-1]),"Hemochromatosis"=>(
[100 1 1 1 1
 100 1 1 1 1],

[-1,5,3,6,6]),"Waardenburg syndrome"=>(
[2 1 1 1 1 17
 1 1 1 1 1 5],

[30,30,25,5,10,19]),"Hypertrophic cardiomyopathy"=>(
[1 18 9 1 34 1 1 69 100 1 1 100 10 18 16
 1 49 11 1 11 1 1 40 100 1 1 100 66 19 38],

[18,23,15,10,6,12,104,2,-1,41,59,-1,9,18,17]),"Spastic paraplegia"=>(
[4 19 73 12 3 4
 56 79 43 18 6 9],

[14,26,2,32,38,3]),"Ectodermal dysplasia"=>(
[1 1 1
 1 1 1],

[11,14,8]),"Kartagener syndrome"=>(
[100 80 33
 100 3 2],

[-1,1,1]),"Amyloidosis VI"=>(
[15 6 1
 14 2 1],

[10,8,2009]),"Maple-syrup urine disease"=>(
[15 1 1 6
 12 1 1 5],

[11,2,4,14]),"Hemophagocytic lymphohistiocytosis"=>(
[1 8 8 1
 1 51 24 1],

[32,60,7,4]),"Leukoencephalopathy with vanishing white matter"=>(
[1 1 1 2 1
 1 1 1 2 1],

[14,10,15,24,9]),"Hermansky-Pudlak syndrome"=>(
[2 100 2 2 1 1 19 1
 2 100 3 3 1 1 42 1],

[6,-1,3,10,2,61,18,3]),"Spinocerebellar Ataxia"=>(
[3 19 1 1 36 6 20 100 84 2 1 46
 2 14 3 6 36 9 23 100 51 1 1 48],

[167,30,263,95,1,65,53,-1,1,38,75,13]),"Aicardi-Goutieres syndrome"=>(
[75 36 74 29
 68 8 14 36],

[1,1,1,15]),"Bardet-Biedl Syndrome"=>(
[1 2 2 1 1 1 1 1 1 2 1 1 1
 1 2 4 1 1 1 1 1 1 2 1 1 1],

[8,13,63,9,9,17,33,9,25,24,26,6,20]),"Ehlers Danlos syndrome"=>(
[5 2 64 1 1 1 63 20 7
 18 2 76 2 1 2 63 13 16],

[2,15,3,44,40,12,2,25,6]),"Amyotrophic lateral sclerosis"=>(
[8 31 33 36 8 3 14 30
 13 17 26 31 26 3 25 37],

[45,6,9,15,28,73,11,6]),"Primary microcephaly"=>(
[15 9 28 12
 12 43 25 25],

[39,55,13,20]),"Nonsyndromic hearing loss"=>(
[1 14 86 100 77 100 60 100 100 2 40 100 4 2 100 42 23 38 100 2 1 1 100 1 3 6 4 1 50 1 21 100 100 2 68 57 57 47 36 100 20 29
 1 3 83 100 79 100 44 100 100 3 41 100 3 2 100 42 17 23 100 2 1 1 100 1 41 14 3 1 37 1 32 100 100 2 65 67 61 46 35 100 8 37],

[16,9,1,-1,1,-1,1,-1,-1,104,4,-1,140,8,-1,2,9,8,-1,2,5,13,-1,5,4,56,42,1,4,12,3,-1,-1,3,3,7,4,2,2,-1,17,5]),"Cutis laxa"=>(
[9 1 1 1
 6 1 1 1],

[7,15,54,25]),"Familial exudative vitreoretinopathy"=>(
[14 100 3
 11 100 7],

[11,-1,12]),"Long QT Syndrome"=>(
[2 22 2 2 1 6 3 19 1
 8 9 2 2 1 22 2 75 1],

[24,22,3,6,27,14,28,12,17]),"Combined oxidative phosphorylation deficiency"=>(
[11 16 3 25
 16 7 1 18],

[12,8,62,7]),"Congenital myasthenic syndromes"=>(
[1 1 2 2 1
 1 1 1 2 1],

[4,3,2,1,9]),"Achromatopsia"=>(
[51 100 68
 59 100 66],

[4,-1,1]),"Congenital central hypoventilation syndrome"=>(
[43 45 1 8 1 51
 30 64 2 42 45 75],

[2,9,50,17,5,5]),"Hyper-IgM syndrome"=>(
[1 27 1 10
 1 9 1 7],

[12,12,49,61]),"Cerebrooculofacioskeletal syndrome"=>(
[1 3 2 1
 1 4 2 1],

[43,20,34,19]),"Kallmann syndrome"=>(
[1 1 100 100
 1 1 100 100],

[3,72,-1,-1]),"Neuronal ceroid lipofuscinosis"=>(
[1 34 5 13 3 35 100
 1 31 18 9 3 9 100],

[33,6,30,28,88,5,-1]),"Polycystic kidney disease"=>(
[1 1 61
 1 1 62],

[17,31,2]),"Pseudohypoaldosteronism, type I, autosomal recessive"=>(
[2 1 2
 2 1 2],

[16,21,22]),"Multiple Acyl-CoA Dehydrogenase deficiency"=>(
[1 1 33
 1 1 20],

[8,23,2]),"Adrenoleukodystrophy"=>(
[1 1 1 1 1
 1 1 1 1 1],

[5,6,80,10,7]),"Pituitary dwarfism"=>(
[1 2 43 1
 1 2 53 1],

[13,14,8,6]),"Generalized epilepsy with febrile seizures plus"=>(
[62 24 46 23
 83 5 47 37],

[4,4,4,6]),"Brachydactyly"=>(
[1 12 1 23 24
 1 53 1 15 30],

[70,4,8,17,7]),"Arrhythmogenic right ventricular dysplasia (ARVD)"=>(
[3 41 1 3 1 21
 2 84 1 2 1 17],

[15,13,73,9,31,22]),"Mitochondrial complex I deficiency disorders"=>(
[24 1 100 100 5 100 49 1 19 100 22 100 100 28 100 26
 13 1 100 100 3 100 23 1 13 100 31 100 100 12 100 16],

[5,21,-1,-1,19,-1,6,18,5,-1,9,-1,-1,4,-1,5]),"Nonbullous congenital ichthyosiform erythroderma"=>(
[1 47 2 2
 1 10 2 2],

[10,4,3,4]),"Severe congenital neutropenia"=>(
[15 7 100
 9 4 100],

[20,63,-1]),"Keratosis palmoplantaris striata"=>(
[3 4 1
 4 3 1],

[27,57,73]),"Osteopetrosis"=>(
[57 1 14 69 1
 34 1 31 33 1],

[11,7,19,3,4]),"Pulmonary surfactant metabolism dysfunction"=>(
[47 78 59
 39 50 62],

[1,1,14]),"Atypical mycobacteriosis, familial"=>(
[1 1 1 2 1 1
 1 2 1 2 1 1],

[14,409,7,140,8,4])}

polygenic = 
{"Alzheimer Disease"=>(
[1 1 1 1
 1 1 1 1],

[57,66,2009,109]),"Essential hypertension"=>(
[1 22 8 1 16 16 43 5 68 1 1 6
 1 50 21 1 22 12 61 51 89 1 1 7],

[23,16,22,10,30,30,4,7,4,54,25,114]),"Inflammatory Bowel Disease"=>(
[2 16 2 45 100 16 84 5
 50 17 8 48 100 16 79 3],

[3,37,4,5,-1,17,1,28]),"Pheochromocytoma"=>(
[1 100 23 4 22 59
 2 100 22 6 13 40],

[417,-1,26,50,17,4]),"Graves disease"=>(
[4 4 5 36 33
 9 7 9 38 79],

[98,13,9,3,14]),"Non-Insulin-Dependent Diabetes Mellitus"=>(
[60 11 38 42 100 92 100 4 15 2 42 11 1 36 3 3 47 2 6
 67 10 30 46 100 94 100 5 9 1 46 26 1 19 2 48 39 2 32],

[4,77,11,2,-1,2,-1,106,25,45,3,4,31,6,91,14,5,55,4]),"Systemic lupus erythematodes"=>(
[2 13 5 39 27 100 4
 1 4 77 53 67 100 10],

[23,15,3,5,4,-1,14]),"Mycobacterium tuberculosis, susceptibility to"=>(
[12 22 7 100 36 8
 32 49 25 100 98 12],

[10,21,98,-1,12,44]),"Obesity"=>(
[1 6 5 1 49 22 1 100 5 1 2 28 1
 1 14 14 1 57 40 2 100 3 2 1 50 1],

[10,57,88,10,2,6,239,-1,36,11,23,3,11]),"Age-Related Macular Degeneration"=>(
[53 39 3 9 4 59 3 2 100 98 2 12
 43 54 10 21 16 54 13 2 100 80 2 34],

[2,6,2,43,6,1,7,4,-1,1,19,15]),"Rheumatoid arthiritis"=>(
[75 22 1 2 19 12 16
 73 24 2 5 14 64 18],

[3,23,77,28,12,16,16]),"Maturity-Onset Diabetes of the Young (MODY)"=>(
[100 14 9 100 7 72 100 40 49
 100 13 13 100 6 76 100 63 58],

[-1,14,77,-1,16,3,-1,5,6])}

cancer = 
{"Prostate cancer"=>(
[17 1 47 1 6 1 23 28 11 1 3 19 27
 8 2 56 1 9 1 15 21 9 2 4 20 20],

[22,144,9,107,46,57,16,5,17,33,91,42,10]),"Esophageal carcinoma"=>(
[1 100 59 8 56 3 8 37 1 1
 2 100 55 11 67 3 12 34 6 6],

[865,-1,2,56,3,170,69,5,186,240]),"Bladder Cancer"=>(
[1 1 10 2
 1 1 20 3],

[258,62,49,111]),"Lung cancer"=>(
[1 59 1 1 3
 1 56 8 3 6],

[865,2,62,863,57]),"Glioma of brain, familial"=>(
[38 2 1 3 19 5 3 2 2 4 39 13 2 51 2 93 4 24 68 2 2
 32 3 2 6 17 11 5 2 1 5 31 16 2 37 1 92 3 26 76 7 2],

[24,50,863,107,26,141,54,186,57,41,13,19,53,9,865,2,91,23,2,170,165]),"Medulloblastoma"=>(
[11 33 2 20 70 1
 9 25 3 20 81 1],

[57,9,170,31,5,362]),"Juvenile myelomonocytic leukemia"=>(
[27 1 1 13 2
 24 1 1 9 2],

[11,44,62,26,148]),"Breast cancer familial"=>(
[2 3 38 1 1 1 60 7 4 1 1 1 6 1 1
 7 11 29 1 1 1 44 3 3 1 1 1 12 1 1],

[70,75,10,34,530,57,2,110,13,865,253,86,24,91,326]),"Hepataocellular carcinoma"=>(
[1 2 1
 2 3 1],

[865,121,362]),"Pancreatic carcinoma"=>(
[1 2 5 2 100 1
 1 7 5 6 100 6],

[865,265,57,62,-1,186]),"Thyroid carcinoma, papillary"=>(
[11 2 6 11 7 4 22 7
 9 2 6 14 4 3 26 4],

[51,50,75,12,116,65,13,38]),"Hereditary nonpolyposis colorectal cancer"=>(
[2 2 2 1 2 1 1
 2 2 2 3 2 1 1],

[83,50,165,44,69,48,53])}

using Plotly

function remove_1(tup)
    new_ranks = Array(Int, 2,0)
    new_deg = Int[]
    ranks = tup[1]
    deg = tup[2]
    for d in range(1, size(deg,1))
        if (deg[d] != -1) 
            push!(new_deg, deg[d])
            new_ranks = [new_ranks ranks[:,d]]
        end
    end
    return new_ranks, new_deg
end

function deg_ranks_from_dict(dict)
    ranks = Array(Int,2,0)
    deg = Int[]
    for disease in keys(dict)
        r = remove_1(dict[disease])
        ranks = [ranks r[1]]
        deg = [deg,r[2]]
    end
    return ranks, deg
end

m = deg_ranks_from_dict(monogenic)
p = deg_ranks_from_dict(polygenic)
c = deg_ranks_from_dict(cancer)

# plotly won't work if i loop over method also soo... just alternate method between 1 and 2

for class in [(m, "Monogenic Diseases", "monogenic"),(p, "Polygenic Diseases", "polygenic"),(c, "Oncogenic (Cancer) Diseases", "cancer")]
    method=2
    degrees = class[1][2]
    ranks = class[1][1]
    name = class[2]
    filename_method = ""
    r = ranks[method,:][:]
    # WTF = ["x" => degrees, "type" => "scatter", "mode" => "markers"]
    # WTF["y"] = r
    WTF = Dict()
    WTF["y"] = degrees
    WTF["x"] = r
    WTF["type"] = "scatter"
    WTF["mode"] = "markers"
    layout = ["xaxis" => ["title" => "Ranking"], "yaxis" => ["title" => "Degree"], "title" => "ok"]
    if method == 1
        z = "Degree versus Ranking for Random Walk with Restart over " * name
        layout["title"] = z
        filename_method = "rwr"
    else
        z = "Degree versus Ranking for Random Walk with Restart over " * name
        layout["title"] = z
        filename_method = "dk"
    end

    filename = "rank_degree_" * filename_method * "_" * class[3]

    response = Plotly.plot([WTF], ["layout" => layout, "filename" => filename, "fileopt" => "overwrite"])
    println(response["url"])
end






# for plotting all at once

# degrees = [m[2],p[2],c[2]]
# ranks = [m[1] p[1] c[1]]
# rwr = ranks[1,:][:]
# dk = ranks[2,:][:]

# trace = [
#     "x" => dk,
#     "y" => degrees,
#     "type" => "scatter",
#     "mode" => "markers"
# ]

# layout = [
#     "xaxis" => ["title" => "Ranking"],
#     "yaxis" => ["title" => "Degree"],
#     "title" => "Degree versus Ranking for Diffusion Kernel over all Disease Classes"
# ]

# response = Plotly.plot([trace], ["layout" => layout, "filename" => "rank_degree_dk_all", "fileopt" => "overwrite"])
# println(response["url"])





