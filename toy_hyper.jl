using Suppressor

using StatsBase
using PyPlot

@suppress_err begin

include("cluster.jl")
include("rwr_and_diffusion_kernel.jl")

# using PyPlot

# n=30
# #graph is two n cliques separated by 2n vertices
# m = 4*n #number of vertices
# r=0.3

# g_fam = zeros(m,2)
# g_fam[1:n,1] = 1
# g_fam[(m-n+1):m,2] = 1

# A=zeros(m, m)
# for i=n:(m-n)
#     A[i,i+1]=1
# end
# A=A+A'

# x1=construct_hyper_rwr_mat(g_fam, A, r)

# ####

# y=zeros(m, m)
# for i=1:(n-1)
#     y[i, (i+1):n]=1
# end

# for i=n:(m-n)
#     y[i, i+1]=1
# end

# for i=(m-n+1):(m-1)
#     y[i, (i+1):m]=1
# end

# y=y+y'

# y1 = make_rwr_mat(y,r)

# println("hyper")
# println(x1[1:5,1:5])
# println("reg")
# println(y1[1:5,1:5])

# pcolormesh(y1)
# colorbar()
# title("RWR Kernel for Toy Graph")
# savefig("rwr_reg_toy_1.png")

# figure(2)
# pcolormesh(y1)
# figure(2)
# colorbar()

############################

# random preferential attachment hypergraph? https://arxiv.org/pdf/1502.02401v1.pdf

#below we do (approximately) d-regular hypergraph
# output is matrix whose columns are hyperedges, rows are nodes
function create_rand_pref_d_reg_hypergraph(time, p, d)
    edge_mat = ones(1,1) #n by m
    n=1 #number of nodes
    m=1 #number of edges
    degs = [1]
    for t=1:time
        r = rand()

        # with probability p, add new vertex and make it connect to d-1 others
        if r < p
            #other edges it connects to
            #neighbors= rand(1:n, 1, min(d-1, n))
            # println(Array(1:length(degs)))
            # println(min(d-1, n))
            # println(degs)
            neighbors = sample(Array(1:length(degs)), WeightVec(degs), min(d-1, n))

            edge = zeros(n+1, 1)
            edge[neighbors] = 1
            edge[n+1] = 1

            edge_mat = [edge_mat zeros(n, 1)]
            edge_mat = [edge_mat; zeros(1, m+1)]
            edge_mat[:, m+1] = edge

            #update n, m
            n=n+1
            m=m+1

            #update degs
            for v in neighbors
                degs[v] = degs[v] + 1
            end

            #degs = [degs, min(d-1, n)]
            push!(degs, min(d-1, n))
        else
            #with probability 1-p, select d vertices and make a new edge
            neighbors = rand(1:n, 1, min(d, n))
            edge = zeros(n, 1)
            edge[neighbors] = 1

            edge_mat = [edge_mat edge]
            m=m+1

            #update degs
            for v in neighbors
                degs[v] = degs[v] + 1
            end
        end
    end
    return edge_mat
end

function create_hyper_pmf(edge_mat)
    n = size(edge_mat, 1)
    pmf = zeros(n, n)

    #get rid of duplicate edges
    edge_mat = unique(edge_mat, 2)

    #get rid of self-loops
    edge_mat = edge_mat[:, find(Int.(sum(edge_mat, 1) .> 1))]

    for i=1:size(edge_mat, 1)
        #get hyperedges using i
        relevant_edges = edge_mat[:, find(edge_mat[i, :])]

        #get rid of i in relevant edges
        for k=1:size(relevant_edges, 2)
            relevant_edges[i, k] = 0
        end

        #normalize each edge
        relevant_edges = relevant_edges ./ sum(relevant_edges, 1)

        relevant_edges = (1/size(relevant_edges, 2)) * sum(relevant_edges, 2)

        pmf[i, :] = relevant_edges'
    end

    return pmf
end

function create_simple_pmf(edge_mat)
    n = size(edge_mat, 1)
    pmf = zeros(n, n)

    #get rid of duplicate edges
    edge_mat = unique(edge_mat, 2)

    #get rid of self-loops
    edge_mat = edge_mat[:, find(Int.(sum(edge_mat, 1) .> 1))]

    for i=1:size(edge_mat, 1)
        #get hyperedges using i
        relevant_edges = edge_mat[:, find(edge_mat[i, :])]

        #find all vertices connected to i
        relevant_edges = Int.(sum(relevant_edges, 2) .> 0)
        relevant_edges[i] = 0

        pmf[i, :] = (relevant_edges ./ sum(relevant_edges))'
    end

    return pmf
end

end

r=0.4

edge_mat = create_rand_pref_d_reg_hypergraph(60, 1, 6)
hyper_pmf = create_hyper_pmf(edge_mat)
simple_pmf = create_simple_pmf(edge_mat)

hyper_kernel = make_rwr_mat_pmf(hyper_pmf, r)
simple_kernel = make_rwr_mat_pmf(simple_pmf, r)

pcolormesh(hyper_kernel - simple_kernel)
axis("tight")
colorbar()

# pcolormesh(hyper_kernel)
# axis("tight")
# colorbar()

# figure(2)
# pcolormesh(simple_kernel)
# figure(2)
# axis("tight")
# figure(2)
# colorbar()