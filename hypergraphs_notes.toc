\contentsline {section}{\numberline {1}Introduction}{4}{section.1}
\contentsline {subsection}{\numberline {1.1}Related Work}{5}{subsection.1.1}
\contentsline {section}{\numberline {2}Hypergraphs}{6}{section.2}
\contentsline {subsection}{\numberline {2.1}Directed Hypergraphs}{8}{subsection.2.1}
\contentsline {section}{\numberline {3}Random Walks}{10}{section.3}
\contentsline {section}{\numberline {4}Equivalent Random Walks}{13}{section.4}
\contentsline {subsection}{\numberline {4.1}Overview of Results}{14}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Counterexamples}{15}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Detailed Balance}{17}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Proofs of Main Theorems}{18}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Discussion}{20}{subsection.4.5}
\contentsline {subsubsection}{\numberline {4.5.1}Problem \ref {prob_1}}{20}{subsubsection.4.5.1}
\contentsline {subsubsection}{\numberline {4.5.2}Problem \ref {prob_2}}{21}{subsubsection.4.5.2}
\contentsline {section}{\numberline {5}Applications to Disease-Gene Prioritization}{21}{section.5}
\contentsline {subsection}{\numberline {5.1}Method}{21}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Personalized PageRank Kernel}{22}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Results}{23}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}Degree Distribution in Simple Graph}{25}{subsubsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.2}Simple Graphs versus Hypergraphs}{26}{subsubsection.5.3.2}
\contentsline {subsubsection}{\numberline {5.3.3}Different Types of Hypergraphs}{27}{subsubsection.5.3.3}
\contentsline {section}{\numberline {6}Conclusion}{29}{section.6}
\contentsline {subsection}{\numberline {6.1}Creating a Different Hypergraph}{29}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Alternative Versions of Problems \ref {prob_1} and \ref {prob_2}}{30}{subsection.6.2}
\contentsline {section}{\numberline {7}Acknowledgements}{30}{section.7}
