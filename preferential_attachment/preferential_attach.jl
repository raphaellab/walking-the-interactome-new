using StatsBase
using LightGraphs

function create_preferential_attachment(n,m)
    A=zeros(n,n)
    A[1:3,1:3] = [0 1 1; 1 0 1; 1 1 0]
    for i=4:n
        degs = Int.(sum(A[1:i,1:i],1))
        neighbors = sample(1:length(degs), WeightVec(vec(degs)), 3)
        A[i,neighbors]=1

        A = A + A'
        A = Int.(A .> 0)
    end
    return A
end

A=create_preferential_attachment(100,5)

g=Graph(A)
mc = maximal_cliques(g)
mc = filter(x -> length(x) > 1, mc)

C=zeros(size(A,1),length(mc))
for i=1:length(mc)
    C[mc[i],i]=1
end

W=create_weighted_hyper_transition_mat(A-A, C)
UW=create_unweighted_hyper_transition_mat(A-A, C)

W_alt=create_weighted_hyper_transition_mat(A, C)
UW_alt=create_unweighted_hyper_transition_mat(A, C)

r=0.4

#PPR = r * inv(I - (1-r)*P)
PPR_W = r * inv(eye(W) - (1-r)*W)
PPR_UW = r * inv(eye(UW) - (1-r)*UW)
PPR_W_alt = r * inv(eye(W_alt) - (1-r)*W_alt)
PPR_UW_alt = r * inv(eye(UW_alt) - (1-r)*UW_alt)